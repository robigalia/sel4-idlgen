use std::iter::Peekable;

use clang::source::{SourceLocation, SourceRange};
use clang::token::{Token, TokenKind};
use clang::Entity;
use log::{trace, warn};

use super::expression::{Expression, Identifier, Operator};
use super::{remove_randomness, StructFieldType};

const OPERATOR_PRECEDENCE: &'static [(Operator, u32, Associativity)] = &[
    (Operator::UnaryPlus, 12, Associativity::RightToLeft),
    (Operator::UnaryMinus, 12, Associativity::RightToLeft),
    (Operator::LogicalNot, 12, Associativity::RightToLeft),
    (Operator::BitwiseNot, 12, Associativity::RightToLeft),
    (Operator::Sizeof, 12, Associativity::RightToLeft),
    (Operator::Defined, 12, Associativity::RightToLeft),
    (Operator::Multiply, 11, Associativity::LeftToRight),
    (Operator::Divide, 11, Associativity::LeftToRight),
    (Operator::Remainder, 11, Associativity::LeftToRight),
    (Operator::Add, 10, Associativity::LeftToRight),
    (Operator::Subtract, 10, Associativity::LeftToRight),
    (Operator::LeftShift, 9, Associativity::LeftToRight),
    (Operator::RightShift, 9, Associativity::LeftToRight),
    (Operator::LessThan, 8, Associativity::LeftToRight),
    (Operator::LessThanEqual, 8, Associativity::LeftToRight),
    (Operator::GreaterThan, 8, Associativity::LeftToRight),
    (Operator::GreaterThanEqual, 8, Associativity::LeftToRight),
    (Operator::Equal, 7, Associativity::LeftToRight),
    (Operator::NotEqual, 7, Associativity::LeftToRight),
    (Operator::BitwiseAnd, 6, Associativity::LeftToRight),
    (Operator::BitwiseXor, 5, Associativity::LeftToRight),
    (Operator::BitwiseOr, 4, Associativity::LeftToRight),
    (Operator::LogicalAnd, 3, Associativity::LeftToRight),
    (Operator::LogicalOr, 2, Associativity::LeftToRight),
];

#[derive(Clone, Copy, Debug, PartialEq)]
enum Associativity {
    LeftToRight,
    RightToLeft,
}

pub fn parse_expression_from_entity(e: &Entity) -> Result<Expression, ()> {
    let vec = e
        .get_range()
        .expect("macro value has no source text")
        .tokenize();
    let mut tokens = vec
        .iter()
        .filter(|x| x.get_kind() != TokenKind::Comment)
        .peekable();

    parse_expression(&vec, &mut tokens)
}

// https://en.wikipedia.org/wiki/Operator-precedence_parser
pub fn parse_expression<'a, I>(
    vec: &'a Vec<Token>,
    tokens: &mut Peekable<I>,
) -> Result<Expression, ()>
where
    I: Iterator<Item = &'a Token<'a>>,
{
    parse_expression_1(parse_primary(vec, tokens)?, 0, vec, tokens)
}

fn parse_expression_1<'a, I>(
    mut lhs: Expression,
    min_precedence: u32,
    vec: &'a Vec<Token>,
    tokens: &mut Peekable<I>,
) -> Result<Expression, ()>
where
    I: Iterator<Item = &'a Token<'a>>,
{
    while let Some(op) = get_binary_operator_from_token(tokens.peek()) {
        let (op_precedence, _) = get_operator_info(op);

        if op_precedence < min_precedence {
            break;
        }

        tokens.next();

        let mut rhs = parse_primary(vec, tokens)?;

        while let Some(op_lookahead) = get_binary_operator_from_token(tokens.peek()) {
            let (lookahead_precedence, lookahead_associativity) = get_operator_info(op_lookahead);

            match lookahead_associativity {
                Associativity::LeftToRight => {
                    if lookahead_precedence <= op_precedence {
                        break;
                    }
                }
                Associativity::RightToLeft => {
                    if lookahead_precedence < op_precedence {
                        break;
                    }
                }
            }

            rhs = parse_expression_1(rhs, lookahead_precedence, vec, tokens)?;
        }

        lhs = Expression::BinaryOperator {
            op,
            arg1: Box::new(lhs),
            arg2: Box::new(rhs),
        };
    }

    Ok(lhs)
}

fn parse_primary<'a, I>(vec: &'a Vec<Token>, tokens: &mut Peekable<I>) -> Result<Expression, ()>
where
    I: Iterator<Item = &'a Token<'a>>,
{
    let token = get_next_token(vec, tokens)?;
    let spelling = token.get_spelling();

    match (token.get_kind(), spelling.as_ref()) {
        (TokenKind::Punctuation, "(") => {
            let e = parse_expression(vec, tokens)?;
            expect_token(")", TokenKind::Punctuation, vec, tokens)?;
            Ok(e)
        }
        // HACK: Treat the LIBSEL4_BIT function-like macro call as if it were a unary operator
        (TokenKind::Identifier, "LIBSEL4_BIT") => {
            let arg = parse_primary(vec, tokens)?;
            Ok(Expression::BinaryOperator {
                op: Operator::LeftShift,
                arg1: Box::new(Expression::NumericLiteral(1)),
                arg2: Box::new(arg),
            })
        }
        (TokenKind::Punctuation, "-")
        | (TokenKind::Punctuation, "+")
        | (TokenKind::Punctuation, "!")
        | (TokenKind::Punctuation, "~")
        | (TokenKind::Keyword, "sizeof")
        | (TokenKind::Identifier, "defined") => {
            let op = string_to_unary_operator(&spelling).expect("unary operator not found");
            let arg = parse_primary(vec, tokens)?;

            Ok(Expression::UnaryOperator {
                op,
                arg1: Box::new(arg),
            })
        }
        (TokenKind::Identifier, _) => Ok(Expression::Identifier(Identifier::Unresolved(
            remove_randomness(spelling),
        ))),
        (TokenKind::Literal, _) => {
            if spelling.starts_with("\"") && spelling.ends_with("\"") {
                if spelling.contains("\\") {
                    // TODO: Escape sequences
                    warn!(
                        "PARSER: escape sequence parsing in string literals not implemented: {:?} '{}' at {:?}",
                        token.get_kind(),
                        token.get_spelling(),
                        token.get_location()
                    );
                    Err(())
                } else {
                    Ok(Expression::StringLiteral(spelling))
                }
            } else if spelling.len() == 3 && spelling.starts_with("'") && spelling.ends_with("'") {
                // TODO: character literal
                warn!(
                    "PARSER: character literal parsing not implemented: {:?} '{}' at {:?}",
                    token.get_kind(),
                    token.get_spelling(),
                    token.get_location()
                );
                Err(())
            } else if spelling.starts_with("0x") || spelling.starts_with("0X") {
                let spelling = &spelling[2..]
                    .trim_end_matches(|c| c == 'l' || c == 'L' || c == 'u' || c == 'U')
                    .trim_end_matches(|c| c == 'l' || c == 'L' || c == 'u' || c == 'U')
                    .trim_end_matches(|c| c == 'l' || c == 'L' || c == 'u' || c == 'U')
                    .replace('\'', "");

                if let Ok(num) = u64::from_str_radix(spelling, 16) {
                    Ok(Expression::NumericLiteral(num))
                } else {
                    trace!(
                        "PARSER: Unable to parse token as hex numeric literal: {:?} '{}' at {:?}",
                        token.get_kind(),
                        token.get_spelling(),
                        token.get_location()
                    );

                    Err(())
                }
            } else if spelling.starts_with("0b") || spelling.starts_with("0B") {
                let spelling = &spelling[2..]
                    .trim_end_matches(|c| c == 'l' || c == 'L' || c == 'u' || c == 'U')
                    .trim_end_matches(|c| c == 'l' || c == 'L' || c == 'u' || c == 'U')
                    .trim_end_matches(|c| c == 'l' || c == 'L' || c == 'u' || c == 'U')
                    .replace('\'', "");

                if let Ok(num) = u64::from_str_radix(spelling, 2) {
                    Ok(Expression::NumericLiteral(num))
                } else {
                    trace!(
                        "PARSER: Unable to parse token as binary numeric literal: {:?} '{}' at {:?}",
                        token.get_kind(),
                        token.get_spelling(),
                        token.get_location()
                    );

                    Err(())
                }
            } else if spelling.starts_with("0") && spelling.len() > 1 {
                let spelling = &spelling[1..]
                    .trim_end_matches(|c| c == 'l' || c == 'L' || c == 'u' || c == 'U')
                    .trim_end_matches(|c| c == 'l' || c == 'L' || c == 'u' || c == 'U')
                    .trim_end_matches(|c| c == 'l' || c == 'L' || c == 'u' || c == 'U')
                    .replace('\'', "");

                if let Ok(num) = u64::from_str_radix(spelling, 8) {
                    Ok(Expression::NumericLiteral(num))
                } else {
                    trace!(
                        "PARSER: Unable to parse token as octal numeric literal: {:?} '{}' at {:?}",
                        token.get_kind(),
                        token.get_spelling(),
                        token.get_location()
                    );

                    Err(())
                }
            } else if spelling.chars().nth(0).unwrap().is_numeric() {
                let spelling = &spelling
                    .trim_end_matches(|c| c == 'l' || c == 'L' || c == 'u' || c == 'U')
                    .trim_end_matches(|c| c == 'l' || c == 'L' || c == 'u' || c == 'U')
                    .trim_end_matches(|c| c == 'l' || c == 'L' || c == 'u' || c == 'U')
                    .replace('\'', "");

                if let Ok(num) = u64::from_str_radix(spelling, 10) {
                    Ok(Expression::NumericLiteral(num))
                } else {
                    trace!(
                        "PARSER: Unable to parse token as decimal numeric literal: {:?} '{}' at {:?}",
                        token.get_kind(),
                        token.get_spelling(),
                        token.get_location()
                    );

                    Err(())
                }
            } else {
                trace!(
                    "PARSER: Don't know how to parse literal: {:?} '{}' at {:?}",
                    token.get_kind(),
                    token.get_spelling(),
                    token.get_location()
                );

                Err(())
            }
        }
        _ => {
            trace!(
                "PARSER: Unexpected token {:?} '{}' at {:?}",
                token.get_kind(),
                token.get_spelling(),
                token.get_location()
            );

            Err(())
        }
    }
}

fn get_binary_operator_from_token(opt_token: Option<&&Token>) -> Option<Operator> {
    if let Some(token) = opt_token {
        if token.get_kind() == TokenKind::Punctuation {
            return string_to_binary_operator(&token.get_spelling());
        }
    }

    None
}

fn string_to_binary_operator(s: &str) -> Option<Operator> {
    match s {
        "*" => Some(Operator::Multiply),
        "/" => Some(Operator::Divide),
        "%" => Some(Operator::Remainder),
        "+" => Some(Operator::Add),
        "-" => Some(Operator::Subtract),
        "<<" => Some(Operator::LeftShift),
        ">>" => Some(Operator::RightShift),
        "<" => Some(Operator::LessThan),
        "<=" => Some(Operator::LessThanEqual),
        ">" => Some(Operator::GreaterThan),
        ">=" => Some(Operator::GreaterThanEqual),
        "==" => Some(Operator::Equal),
        "!=" => Some(Operator::NotEqual),
        "&" => Some(Operator::BitwiseAnd),
        "^" => Some(Operator::BitwiseXor),
        "|" => Some(Operator::BitwiseOr),
        "&&" => Some(Operator::LogicalAnd),
        "||" => Some(Operator::LogicalOr),
        _ => None,
    }
}

fn get_operator_info(op: Operator) -> (u32, Associativity) {
    let (table_op, precedence, associativity) = OPERATOR_PRECEDENCE[op as usize];
    if table_op != op {
        panic!(
            "Operator precedence table has incorrect entry for operator {:?}",
            op
        );
    } else {
        (precedence, associativity)
    }
}

fn string_to_unary_operator(s: &str) -> Option<Operator> {
    match s {
        "+" => Some(Operator::UnaryPlus),
        "-" => Some(Operator::UnaryMinus),
        "!" => Some(Operator::LogicalNot),
        "~" => Some(Operator::BitwiseNot),
        "sizeof" => Some(Operator::Sizeof),
        "defined" => Some(Operator::Defined),
        _ => None,
    }
}

fn expect_eof<'a, I>(_vec: &'a Vec<Token>, tokens: &mut Peekable<I>) -> Result<(), ()>
where
    I: Iterator<Item = &'a Token<'a>>,
{
    if let Some(token) = tokens.next() {
        trace!(
            "PARSER: Expected eof, found {:?} '{}' at {:?}",
            token.get_kind(),
            token.get_spelling(),
            token.get_location()
        );
        Err(())
    } else {
        Ok(())
    }
}

pub fn parse_macro_value(e: &Entity) -> Result<Expression, ()> {
    trace!("Attempting to parse macro value");

    let name = remove_randomness(e.get_name().expect("enum constant has no name"));
    let vec = e
        .get_range()
        .expect("macro value has no source text")
        .tokenize();
    let mut tokens = vec
        .iter()
        .filter(|x| x.get_kind() != TokenKind::Comment)
        .peekable();

    expect_token(&name, TokenKind::Identifier, &vec, &mut tokens)?;
    let result = parse_expression(&vec, &mut tokens)?;
    expect_eof(&vec, &mut tokens)?;

    Ok(result)
}

pub fn get_enum_constant_source_text(e: &Entity) -> String {
    let mut result = String::new();
    let mut expected_offset_with_no_whitespace = 0;

    for token in get_range_from_entity_to_eof(e)
        .tokenize()
        .iter()
        .filter(|x| x.get_kind() != TokenKind::Comment)
    {
        if token.get_kind() == TokenKind::Punctuation
            && (token.get_spelling() == "," || token.get_spelling() == "}")
        {
            break;
        }
        let offset = token.get_location().get_spelling_location().offset;
        if expected_offset_with_no_whitespace > 0 && offset > expected_offset_with_no_whitespace {
            result.push_str(" ")
        }
        let spelling = remove_randomness(token.get_spelling());
        expected_offset_with_no_whitespace = offset + spelling.len() as u32;
        result.push_str(&spelling);
    }

    result
}

// HACK:
// If clang gets confused it doesn't include the confusing bits in an entity's source range.
// If it gets *really* confused, it doen't even calculate a source range for the entity.
// If it's possible for clang to get confused, use this fn instead of entity.get_range().
fn get_range_from_entity_to_eof<'a>(e: &'a Entity) -> SourceRange<'a> {
    let tu = e.get_translation_unit().get_entity();
    let e_location = {
        if let Some(r) = e.get_range() {
            // the range has a better start location if it's available
            r.get_start()
        } else {
            // HACK: When clang gets really confused the location it gives us might not actually be
            // at the start of the thing we want to parse. So, just assume that the thing we want
            // starts at the beginning of whatever line clang thinks the location is.
            let line = e
                .get_location()
                .expect("can't get entity location")
                .get_spelling_location()
                .line;
            get_location_of_first_token_on_line(line, e)
        }
    };
    let tu_range = tu.get_range().expect("can't get range from tu");

    SourceRange::new(e_location, tu_range.get_end())
}

fn get_location_of_first_token_on_line<'a>(line: u32, e: &'a Entity) -> SourceLocation<'a> {
    let tu = e.get_translation_unit().get_entity();
    tu.get_range()
        .unwrap()
        .tokenize()
        .iter()
        .filter(|x| x.get_location().get_spelling_location().line == line)
        .nth(0)
        .unwrap()
        .get_location()
}

pub fn parse_enum_constant(e: &Entity) -> Result<Option<Expression>, ()> {
    if let Ok(()) = parse_enum_constant_no_value(e) {
        return Ok(None);
    } else if let Ok(x) = parse_enum_constant_expression(e) {
        return Ok(Some(x));
    } else {
        Err(())
    }
}

fn parse_enum_constant_no_value(e: &Entity) -> Result<(), ()> {
    // Examples:
    //   my_enum_value ,
    //   my_enum_value }

    trace!("Attempting to parse enum constant with no value");

    let name = remove_randomness(e.get_name().expect("enum constant has no name"));
    let vec = get_range_from_entity_to_eof(&e).tokenize();
    let mut tokens = vec
        .iter()
        .filter(|x| x.get_kind() != TokenKind::Comment)
        .peekable();

    expect_token(&name, TokenKind::Identifier, &vec, &mut tokens)?;
    expect_one_of_tokens((",", "}"), TokenKind::Punctuation, &vec, &mut tokens)?;

    Ok(())
}

fn expect_token<'a, I>(
    value: &str,
    kind: TokenKind,
    vec: &'a Vec<Token>,
    tokens: &mut Peekable<I>,
) -> Result<(), ()>
where
    I: Iterator<Item = &'a Token<'a>>,
{
    let token = get_next_token(vec, tokens)?;
    let spelling = remove_randomness(token.get_spelling());

    if token.get_kind() == kind && spelling == value {
        Ok(())
    } else {
        trace!(
            "PARSER: Expected {:?} '{}', found {:?} '{}' at {:?}",
            kind,
            value,
            token.get_kind(),
            spelling,
            token.get_location()
        );
        Err(())
    }
}

fn get_next_token<'a, I>(
    _vec: &'a Vec<Token>,
    tokens: &mut Peekable<I>,
) -> Result<&'a Token<'a>, ()>
where
    I: Iterator<Item = &'a Token<'a>>,
{
    if let Some(token) = tokens.next() {
        Ok(token)
    } else {
        trace!("PARSER: Unexpected end of token stream");
        Err(())
    }
}

fn expect_one_of_tokens<'a, I>(
    value: (&str, &str),
    kind: TokenKind,
    vec: &'a Vec<Token>,
    tokens: &mut Peekable<I>,
) -> Result<(), ()>
where
    I: Iterator<Item = &'a Token<'a>>,
{
    let token = get_next_token(vec, tokens)?;
    let spelling = remove_randomness(token.get_spelling());

    if token.get_kind() == kind && (spelling == value.0 || spelling == value.1) {
        Ok(())
    } else {
        trace!(
            "PARSER: Expected {:?} '{}' or '{}', found {:?} '{}' at {:?}",
            kind,
            value.0,
            value.1,
            token.get_kind(),
            spelling,
            token.get_location()
        );
        Err(())
    }
}

pub fn parse_enum_constant_expression(e: &Entity) -> Result<Expression, ()> {
    // Examples:
    //   my_enum_value = 5 ,
    //   my_enum_value = -2 }

    trace!("Attempting to parse enum constant with numeric literal value");

    let name = remove_randomness(e.get_name().expect("enum constant has no name"));
    let vec = get_range_from_entity_to_eof(&e).tokenize();
    let mut tokens = vec
        .iter()
        .filter(|x| x.get_kind() != TokenKind::Comment)
        .peekable();

    expect_token(&name, TokenKind::Identifier, &vec, &mut tokens)?;
    expect_token("=", TokenKind::Punctuation, &vec, &mut tokens)?;
    let result = parse_expression(&vec, &mut tokens)?;
    expect_one_of_tokens((",", "}"), TokenKind::Punctuation, &vec, &mut tokens)?;

    Ok(result)
}

pub fn parse_typedef_type(e: &Entity) -> Result<String, ()> {
    // Examples:
    //   my_enum_value ,
    //   my_enum_value }

    trace!("Attempting to parse a typedef");

    let mut result = String::new();
    let name = remove_randomness(e.get_name().expect("enum constant has no name"));
    let vec = get_range_from_entity_to_eof(&e).tokenize();
    let mut tokens = vec
        .iter()
        .filter(|x| x.get_kind() != TokenKind::Comment)
        .peekable();

    expect_token("typedef", TokenKind::Keyword, &vec, &mut tokens)?;

    loop {
        let id = parse_keyword_or_identifier(&vec, &mut tokens)?;
        if id == name {
            break;
        } else {
            result.push_str(&id);
            result.push_str(" ");
        }
    }

    expect_token(";", TokenKind::Punctuation, &vec, &mut tokens)?;

    result = result.trim_end().to_string();
    Ok(result)
}

fn parse_keyword_or_identifier<'a, I>(
    vec: &'a Vec<Token>,
    tokens: &mut Peekable<I>,
) -> Result<String, ()>
where
    I: Iterator<Item = &'a Token<'a>>,
{
    let token = get_next_token(vec, tokens)?;
    let spelling = remove_randomness(token.get_spelling());

    if token.get_kind() == TokenKind::Identifier || token.get_kind() == TokenKind::Keyword {
        return Ok(spelling);
    }

    trace!(
        "PARSER: Expected keyword or identifier, found {:?} '{}' at {:?}",
        token.get_kind(),
        spelling,
        token.get_location()
    );

    Err(())
}

pub fn get_struct_field_source_text(e: &Entity) -> String {
    let mut result = String::new();
    let mut expected_offset_with_no_whitespace = 0;

    for token in get_range_from_entity_to_eof(e)
        .tokenize()
        .iter()
        .filter(|x| x.get_kind() != TokenKind::Comment)
    {
        if token.get_kind() == TokenKind::Punctuation && token.get_spelling() == ";" {
            break;
        }
        let offset = token.get_location().get_spelling_location().offset;
        if expected_offset_with_no_whitespace > 0 && offset > expected_offset_with_no_whitespace {
            result.push_str(" ")
        }
        let spelling = remove_randomness(token.get_spelling());
        expected_offset_with_no_whitespace = offset + spelling.len() as u32;
        result.push_str(&spelling);
    }

    result
}

pub fn parse_struct_field(e: &Entity) -> Result<StructFieldType, ()> {
    if let Ok(x) = parse_struct_field_simple_type(e) {
        return Ok(x);
    } else if let Ok(x) = parse_struct_field_simple_type_with_multiple_fields_on_same_line(e) {
        return Ok(x);
    } else if let Ok(x) = parse_struct_field_array_type(e) {
        return Ok(x);
    } else if let Ok(x) = parse_struct_field_pointer_type(e) {
        return Ok(x);
    } else {
        Err(())
    }
}

fn parse_struct_field_simple_type(e: &Entity) -> Result<StructFieldType, ()> {
    // Examples:
    //   int field ;

    trace!("Attempting to parse struct field with simple type");

    let name = remove_randomness(e.get_name().expect("struct field has no name"));
    let vec = get_range_from_entity_to_eof(&e).tokenize();
    let mut tokens = vec
        .iter()
        .filter(|x| x.get_kind() != TokenKind::Comment)
        .peekable();

    // TODO: this will fail for something like 'unsigned int'
    let type_ = replace_type(parse_keyword_or_identifier(&vec, &mut tokens)?);
    expect_token(&name, TokenKind::Identifier, &vec, &mut tokens)?;
    expect_token(";", TokenKind::Punctuation, &vec, &mut tokens)?;

    Ok(StructFieldType::Simple(type_))
}

// HACK: Replace the type if it has a replacement
fn replace_type(type_: String) -> String {
    for (matcher, replacer) in super::REPLACEMENT_TYPES {
        if &type_ == matcher {
            return replacer.to_string();
        }
    }

    type_
}

fn parse_struct_field_simple_type_with_multiple_fields_on_same_line(
    e: &Entity,
) -> Result<StructFieldType, ()> {
    // Examples:
    //   int other_filed, this_field, another_field ;

    trace!("Attempting to parse struct field with multiple fields declared on the same line");

    let name = remove_randomness(e.get_name().expect("struct field has no name"));
    let vec = get_range_from_entity_to_eof(&e).tokenize();
    let mut tokens = vec
        .iter()
        .filter(|x| x.get_kind() != TokenKind::Comment)
        .peekable();

    // TODO: this will fail for something like 'unsigned int'
    let type_ = replace_type(parse_keyword_or_identifier(&vec, &mut tokens)?);
    let mut found_our_name = false;

    loop {
        let id = parse_identifier(&vec, &mut tokens)?;

        if id == name {
            found_our_name = true;
        }

        let token = get_next_token(&vec, &mut tokens)?;

        match (token.get_kind(), token.get_spelling().as_ref()) {
            (TokenKind::Punctuation, ",") => {
                continue;
            }
            (TokenKind::Punctuation, ";") => {
                break;
            }
            (kind, value) => {
                trace!(
                    "PARSER: Expected Punctuation ',' or ';', found {:?} '{}' at {:?}",
                    kind,
                    value,
                    token.get_location()
                );
            }
        }
    }

    if found_our_name {
        Ok(StructFieldType::Simple(type_))
    } else {
        trace!(
            "PARSER: Expected '{}' in struct field list, not found",
            name
        );
        return Err(());
    }
}

fn parse_struct_field_array_type(e: &Entity) -> Result<StructFieldType, ()> {
    // Examples:
    //   int field [ 6 ] ;
    //   int field [ SOMETHING + 2 ] ;

    trace!("Attempting to parse struct field with array type");

    let name = remove_randomness(e.get_name().expect("struct field has no name"));
    let vec = get_range_from_entity_to_eof(&e).tokenize();
    let mut tokens = vec
        .iter()
        .filter(|x| x.get_kind() != TokenKind::Comment)
        .peekable();

    // TODO: this will fail for something like 'unsigned int'
    let type_ = replace_type(parse_keyword_or_identifier(&vec, &mut tokens)?);
    expect_token(&name, TokenKind::Identifier, &vec, &mut tokens)?;
    expect_token("[", TokenKind::Punctuation, &vec, &mut tokens)?;
    let array_size = parse_expression(&vec, &mut tokens)?;
    expect_token("]", TokenKind::Punctuation, &vec, &mut tokens)?;
    expect_token(";", TokenKind::Punctuation, &vec, &mut tokens)?;

    Ok(StructFieldType::Array(type_, array_size))
}

fn parse_struct_field_pointer_type(e: &Entity) -> Result<StructFieldType, ()> {
    // Examples:
    //   int field ;

    trace!("Attempting to parse struct field with simple type");

    let name = remove_randomness(e.get_name().expect("struct field has no name"));
    let vec = get_range_from_entity_to_eof(&e).tokenize();
    let mut tokens = vec
        .iter()
        .filter(|x| x.get_kind() != TokenKind::Comment)
        .peekable();

    // TODO: this will fail for something like 'unsigned int'
    let type_ = parse_keyword_or_identifier(&vec, &mut tokens)?;
    expect_token("*", TokenKind::Punctuation, &vec, &mut tokens)?;
    expect_token(&name, TokenKind::Identifier, &vec, &mut tokens)?;
    expect_token(";", TokenKind::Punctuation, &vec, &mut tokens)?;

    Ok(StructFieldType::Pointer(type_))
}

fn parse_identifier<'a, I>(vec: &'a Vec<Token>, tokens: &mut Peekable<I>) -> Result<String, ()>
where
    I: Iterator<Item = &'a Token<'a>>,
{
    let token = get_next_token(vec, tokens)?;
    let spelling = remove_randomness(token.get_spelling());

    if token.get_kind() == TokenKind::Identifier {
        return Ok(spelling);
    }

    trace!(
        "PARSER: Expected identifier, found {:?} '{}' at {:?}",
        token.get_kind(),
        spelling,
        token.get_location()
    );

    Err(())
}

pub fn is_struct_packed(e: &Entity) -> bool {
    e.get_range()
        .expect("SourceRange for struct not found")
        .tokenize()
        .iter()
        .filter(|x| {
            x.get_kind() == TokenKind::Identifier
                && (x.get_spelling() == "SEL4_PACKED" || x.get_spelling() == "PACKED")
        })
        .take(1)
        .count()
        > 0
}

pub fn get_source_text(e: &Entity) -> String {
    let mut result = String::new();
    let mut expected_offset_with_no_whitespace = 0;
    if let Some(range) = e.get_range() {
        for token in range.tokenize().iter() {
            let offset = token.get_location().get_spelling_location().offset;
            if expected_offset_with_no_whitespace > 0 && offset > expected_offset_with_no_whitespace
            {
                result.push_str(" ")
            }
            let spelling = token.get_spelling();
            expected_offset_with_no_whitespace = offset + spelling.len() as u32;
            result.push_str(&remove_randomness(spelling));
        }
    } else {
        trace!(
            "No source range available for {:?} location {:?}",
            e,
            e.get_location()
        );
    }
    result
}
