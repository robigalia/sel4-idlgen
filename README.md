# sel4-idlgen

Generates a complete XML IDL specification for seL4 that can be used to
procedurally generate an seL4 library. Requires the following inputs at
runtime:

- The seL4 kernel source code
- Manually created files describing the seL4 system calls and which cpu
  register each argument and return value occupies

## Requirements

libclang >= 3.9

On Debian: ```apt-get install libclang-dev```

## Usage

```
USAGE:
    sel4-idlgen [FLAGS] [OPTIONS] --machine-register-file <machine-register-file> --sel4-source-dir <sel4-source-dir> --syscall-file <syscall-file>

FLAGS:
    -h, --help       Prints help information
    -q               Silence all output
    -V, --version    Prints version information
    -v               Increase message verbosity, can be used multiple times

OPTIONS:
    -r, --machine-register-file <machine-register-file>    Path to machine register definition file
    -o, --output-file <output-file>                        File to write output to. If omitted will write to stdout.
    -k, --sel4-source-dir <sel4-source-dir>                Path to the sel4 kernel source code
    -s, --syscall-file <syscall-file>                      Path to the syscall definition file
```

## Example

```
git clone https://gitlab.com/robigalia/sel4-idlgen
cd sel4-idlgen
git clone --branch 12.0.0 https://github.com/sel4/sel4
cargo run -- --sel4-source-dir sel4 --machine-register-file inputs/machine-registers-sel4-12.0.0.csv --syscall-file inputs/syscalls-sel4-12.0.0.csv --output-file sel4-12.0.0.xml
mkdir build
cd build
cmake -DCROSS_COMPILER_PREFIX= -DCMAKE_TOOLCHAIN_FILE=../sel4/gcc.cmake -G Ninja -C ../sel4/configs/X64_verified.cmake ../sel4/
cd ..
cargo run --example eval -- --idl-file sel4-12.0.0.xml --sel4-build-dir build
```
 
### Message register file format

TODO

### System call file format

TODO

### Generated IDL file format

TODO

## Goals

- Glean as much information from the kernel source code as possible
- Expose rich, detailed, and complete information about seL4
- Minimize assumptions you need to make about seL4 and its build system to
  understand the IDL
- Generate a single IDL file containing information about all architectures,
  platforms, and kernel configurations

## Status

In development. The format of the generated IDL file is expected to change
significantly as we develop our seL4 library against it.
