use std::collections::HashMap;
use std::path::{Path, PathBuf};

use clap::{crate_version, App, Arg};

#[derive(Copy, Clone, Debug)]
enum Value {
    Uint(u64),
    Sint(i64),
    // Variable exists but has no value
    Null,
    // Variable does not exist
    Undefined,
}

enum StructFieldType<'a> {
    Simple(&'a str),
    Pointer(&'a str),
    Array(&'a str, Value),
}

fn main() {
    let m = App::new("sel4-idlgen eval sample application")
        .about("Evaluates an sel4-idlgen generated xml idl file against a generated sel4 kernel configuration and prints the result")
        .version(crate_version!())
        .arg(
            Arg::with_name("idl-file")
                .short("i")
                .long("idl-file")
                .help("Path to the sel4-idlgen generated xml idl file")
                .takes_value(true)
                .required(true)
        )
        .arg(
            Arg::with_name("sel4-build-dir")
                .short("b")
                .long("sel4-build-dir")
                .help("Path to the seL4 build directory that has already been configured with cmake")
                .takes_value(true)
                .required(true)
        )
        .get_matches();

    let idl_file_path = m.value_of("idl-file").unwrap();
    let sel4_build_dir = m.value_of("sel4-build-dir").unwrap();
    let mut state = load_config_values(sel4_build_dir);
    add_primitive_types_to_state(&mut state);

    let idl_text = std::fs::read_to_string(idl_file_path)
        .expect("Could not read idl file -- are you sure it exists?");
    let doc = roxmltree::Document::parse(&idl_text).expect("Could not parse idl file as xml");
    for elem in doc.root().descendants().filter(|e| e.is_element()) {
        match elem.tag_name().name() {
            "Constant" => {
                if let Some((name, value)) = evaluate_constant(&elem, &state) {
                    state.insert(name.to_string(), value);
                    println!("{} = {}", name, value);
                }
            }
            "Enum" => {
                if let Some(enum_name) = evaluate_enum(&elem, &state) {
                    state.insert(enum_name.to_string(), Value::Null);
                    let mut value = Value::Uint(0);
                    for elem in elem
                        .descendants()
                        .filter(|e| e.is_element() && e.tag_name().name() == "EnumConstant")
                    {
                        if let Some((name, opt_value)) = evaluate_enum_constant(&elem, &state) {
                            if let Some(v) = opt_value {
                                value = v;
                            }
                            state.insert(name.to_string(), value);
                            println!("{} = {}", name, value);
                            value = value.add(&Value::Uint(1));
                        }
                    }
                }
            }
            "Type" => {
                if let Some((name, value)) = evaluate_type(&elem, &state) {
                    state.insert(name.to_string(), value);
                    println!("sizeof({}) = {}", name, value);
                }
            }
            "Struct" => {
                if let Some((struct_name, is_packed)) = evaluate_struct(&elem, &state) {
                    state.insert(struct_name.to_string(), Value::Null);
                    println!(
                        "{}struct {}",
                        if is_packed { "packed " } else { "" },
                        struct_name
                    );
                    for elem in elem
                        .descendants()
                        .filter(|e| e.is_element() && e.tag_name().name() == "StructField")
                    {
                        if let Some((field_name, type_)) = evaluate_struct_field(&elem, &state) {
                            match type_ {
                                StructFieldType::Simple(type_name) => println!(
                                    "    {}: {}{}",
                                    field_name,
                                    type_name,
                                    get_type_warning(type_name, &state)
                                ),
                                StructFieldType::Pointer(type_name) => println!(
                                    "    {}: {}*{}",
                                    field_name,
                                    type_name,
                                    get_type_warning(type_name, &state)
                                ),
                                StructFieldType::Array(type_name, array_size) => println!(
                                    "    {}: [{}; {}]{}",
                                    field_name,
                                    type_name,
                                    array_size,
                                    get_type_warning(type_name, &state)
                                ),
                            }
                        }
                    }
                }
            }
            "BitfieldStruct" => {
                if let Some(struct_name) = evaluate_bitfield_struct(&elem, &state) {
                    state.insert(struct_name.to_string(), Value::Null);
                    println!("bitfield struct {}", struct_name);
                    for elem in elem
                        .descendants()
                        .filter(|e| e.is_element() && e.tag_name().name() == "BitfieldStructField")
                    {
                        if let Some((opt_name, bits)) =
                            evaluate_bitfield_struct_field(&elem, &state)
                        {
                            if let Some(name) = opt_name {
                                println!("    {}: {} bits", name, bits);
                            } else {
                                println!("    (padding): {} bits", bits);
                            }
                        }
                    }
                }
            }
            "TaggedUnion" => {
                if let Some((union_name, tag_name)) = evaluate_tagged_union(&elem, &state) {
                    state.insert(union_name.to_string(), Value::Null);
                    state.insert(tag_name.to_string(), Value::Null);
                    println!("tagged union {} with tag {}", union_name, tag_name);
                    for elem in elem
                        .descendants()
                        .filter(|e| e.is_element() && e.tag_name().name() == "TaggedUnionField")
                    {
                        if let Some((type_, tag_constant)) =
                            evaluate_tagged_union_field(&elem, &state)
                        {
                            println!(
                                "    {} = {}{}",
                                type_,
                                tag_constant,
                                get_type_warning(type_, &state)
                            );
                        }
                    }
                }
            }
            "Syscall" => {
                if let Some((syscall_name, syscall_number, does_send_ipcbuf, does_receive_ipcbuf)) =
                    evaluate_syscall(&elem, &state)
                {
                    println!(
                        "syscall {} (Number: {}, Sends ipcbuf: {}, Receives ipcbuf: {})",
                        syscall_name, syscall_number, does_send_ipcbuf, does_receive_ipcbuf
                    );
                    for elem in elem.descendants().filter(|e| e.is_element()) {
                        match elem.tag_name().name() {
                            "SyscallArgument" => {
                                if let Some((name, type_, register, special_value)) =
                                    evaluate_syscall_argument(&elem, &state)
                                {
                                    if let Some(sv) = special_value {
                                        if sv == "sysno" {
                                            println!(
                                                "    argument: '{}' => {}",
                                                syscall_number, register
                                            );
                                        } else {
                                            println!(
                                                "    argument '{}: {}' => {} **{}**{}",
                                                name,
                                                type_,
                                                register,
                                                sv,
                                                get_type_warning(type_, &state)
                                            );
                                        }
                                    } else {
                                        println!(
                                            "    argument '{}: {}' => {}{}",
                                            name,
                                            type_,
                                            register,
                                            get_type_warning(type_, &state)
                                        );
                                    }
                                }
                            }
                            "SyscallReturnValue" => {
                                if let Some((name, type_, register, special_value)) =
                                    evaluate_syscall_argument(&elem, &state)
                                {
                                    if let Some(sv) = special_value {
                                        println!(
                                            "    return value '{}: {}' <= {} **{}**{}",
                                            name,
                                            type_,
                                            register,
                                            sv,
                                            get_type_warning(type_, &state)
                                        );
                                    } else {
                                        println!(
                                            "    return value '{}: {}' <= {}{}",
                                            name,
                                            type_,
                                            register,
                                            get_type_warning(type_, &state)
                                        );
                                    }
                                }
                            }
                            _ => {}
                        }
                    }
                }
            }
            "Interface" => {
                if let Some(interface_name) = evaluate_interface(&elem, &state) {
                    println!(
                        "interface {}{}",
                        interface_name,
                        get_type_warning(interface_name, &state)
                    );
                    for elem in elem
                        .descendants()
                        .filter(|e| e.is_element() && e.tag_name().name() == "Method")
                    {
                        if let Some((method_name, invocation_label)) =
                            evaluate_method(&elem, &state)
                        {
                            println!(
                                "    method {}  (invocation label: {})",
                                method_name, invocation_label
                            );
                            for elem in elem.descendants().filter(|e| e.is_element()) {
                                match elem.tag_name().name() {
                                    "MethodArgument" => {
                                        if let Some((argument_name, type_, is_cap)) =
                                            evaluate_method_argument(&elem, &state)
                                        {
                                            if is_cap {
                                                println!(
                                                    "        argument cap: {}: {}{}",
                                                    argument_name,
                                                    type_,
                                                    get_type_warning(type_, &state)
                                                );
                                            } else {
                                                println!(
                                                    "        argument data: {}: {}{}",
                                                    argument_name,
                                                    type_,
                                                    get_type_warning(type_, &state)
                                                );
                                            }
                                        }
                                    }
                                    "MethodReturnValue" => {
                                        if let Some((argument_name, type_, is_cap)) =
                                            evaluate_method_argument(&elem, &state)
                                        {
                                            if is_cap {
                                                println!(
                                                    "        return value cap: {}: {}{}",
                                                    argument_name,
                                                    type_,
                                                    get_type_warning(type_, &state)
                                                );
                                            } else {
                                                println!(
                                                    "        return value data: {}: {}{}",
                                                    argument_name,
                                                    type_,
                                                    get_type_warning(type_, &state)
                                                );
                                            }
                                        }
                                    }
                                    _ => {}
                                }
                            }
                        }
                    }
                }
            }
            "Error" => {
                let name = elem.attribute("name").expect("Expected 'name' attribute");
                let type_ = elem.attribute("type").expect("Expected 'type' attribute");
                println!(
                    "**** IDL File does not contain an entry for {} '{}'",
                    type_, name
                );
            }
            _ => {}
        }
    }
}

fn load_config_values<P>(path: P) -> HashMap<String, Value>
where
    P: AsRef<Path>,
{
    let mut result = HashMap::new();
    let file_path = PathBuf::from(path.as_ref())
        .join("gen_config")
        .join("kernel")
        .join("gen_config.h");

    for line in std::fs::read_to_string(file_path)
        .expect("Could not read gen_config.h from seL4 build directory -- is directory correct and have you configured with cmake?")
        .lines()
        .map(|l| l.trim_start_matches("#define "))
    {
        let values = line.split(" ").collect::<Vec<&str>>();
        if values.len() == 2 {
            if let Ok(v) = values[1].parse::<u64>() {
                result.insert(values[0].to_string(), Value::Uint(v));
            } else if let Ok(v) = values[1].parse::<i64>() {
                result.insert(values[0].to_string(), Value::Sint(v));
            } else {
                result.insert(values[0].to_string(), Value::Null);
            }
        }
    }

    result
}

fn add_primitive_types_to_state(state: &mut HashMap<String, Value>) {
    // these are the sizeof() of the primitive types
    state.insert("char".to_string(), Value::Uint(1));
    state.insert("signed char".to_string(), Value::Uint(1));
    state.insert("unsigned char".to_string(), Value::Uint(1));
    state.insert("signed short".to_string(), Value::Uint(2));
    state.insert("unsigned short".to_string(), Value::Uint(2));
    state.insert("signed int".to_string(), Value::Uint(4));
    state.insert("unsigned int".to_string(), Value::Uint(4));
    if state["CONFIG_WORD_SIZE"].as_u64().unwrap() == 64 {
        state.insert("signed long".to_string(), Value::Uint(8));
        state.insert("unsigned long".to_string(), Value::Uint(8));
        state.insert("signed long long".to_string(), Value::Uint(8));
        state.insert("unsigned long long".to_string(), Value::Uint(8));
    } else {
        state.insert("signed long".to_string(), Value::Uint(4));
        state.insert("unsigned long".to_string(), Value::Uint(4));
        state.insert("signed long long".to_string(), Value::Uint(8));
        state.insert("unsigned long long".to_string(), Value::Uint(8));
    }
}

fn get_type_warning(name: &str, state: &HashMap<String, Value>) -> &'static str {
    if !state.contains_key(name) {
        " **** WARNING: Type is Undefined"
    } else {
        ""
    }
}

fn evaluate_constant<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<(&'a str, Value)> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        let value = {
            let value_elem = elem
                .children()
                .filter(|e| e.is_element() && e.tag_name().name() == "ConstantValue")
                .next()
                .expect("Expected 'ConstantValue' child element");
            evaluate_expression(&value_elem, &state)
        };
        Some((name, value))
    } else {
        None
    }
}

fn evaluate_enum<'a>(elem: &'a roxmltree::Node, state: &HashMap<String, Value>) -> Option<&'a str> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        Some(name)
    } else {
        None
    }
}

fn evaluate_enum_constant<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<(&'a str, Option<Value>)> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        let value = {
            let opt_value_elem = elem
                .children()
                .filter(|e| e.is_element() && e.tag_name().name() == "EnumConstantValue")
                .next();
            if let Some(value_elem) = opt_value_elem {
                Some(evaluate_expression(&value_elem, &state))
            } else {
                None
            }
        };
        Some((name, value))
    } else {
        None
    }
}

fn evaluate_type<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<(&'a str, Value)> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        let value = {
            let type_ = elem.attribute("type").expect("Expected 'type' attribute");
            state[type_]
        };
        Some((name, value))
    } else {
        None
    }
}

fn evaluate_struct<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<(&'a str, bool)> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        let is_packed = elem
            .attribute("is_packed")
            .expect("Expected 'is_packed' attribute")
            == "true";
        Some((name, is_packed))
    } else {
        None
    }
}

fn evaluate_struct_field<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<(&'a str, StructFieldType<'a>)> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        let type_elem = elem
            .children()
            .filter(|e| e.is_element() && e.tag_name().name() == "StructFieldType")
            .next()
            .expect("Expected 'StructFieldType' child element")
            .children()
            .filter(|e| e.is_element())
            .next()
            .expect("Expected child element in 'StructFieldType'");
        let type_name = type_elem
            .attribute("type")
            .expect("Expected 'type' attribute");
        let type_ = match type_elem.tag_name().name() {
            "Simple" => StructFieldType::Simple(type_name),
            "Pointer" => StructFieldType::Pointer(type_name),
            "Array" => {
                let len_elem = type_elem
                    .children()
                    .filter(|e| e.is_element() && e.tag_name().name() == "ArrayLength")
                    .next()
                    .expect("Expected 'ArrayLength' element");
                StructFieldType::Array(type_name, evaluate_expression(&len_elem, state))
            }
            x @ _ => panic!("Unexpected type '{}' for struct field '{}'", x, name),
        };
        Some((name, type_))
    } else {
        None
    }
}

fn evaluate_bitfield_struct<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<&'a str> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        Some(name)
    } else {
        None
    }
}

fn evaluate_bitfield_struct_field<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<(Option<&'a str>, u32)> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name");
        let bits = elem
            .attribute("bits")
            .expect("Expected 'bits' attribute")
            .parse::<u32>()
            .expect("Invalid value for 'bits' attribute");
        Some((name, bits))
    } else {
        None
    }
}

fn evaluate_tagged_union<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<(&'a str, &'a str)> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        let tag_name = elem
            .attribute("tag_name")
            .expect("Expected 'tag_name' attribute");
        Some((name, tag_name))
    } else {
        None
    }
}

fn evaluate_tagged_union_field<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<(&'a str, &'a str)> {
    if is_condition_true(&elem, &state) {
        let type_ = elem.attribute("type").expect("Expected 'type' attribute");
        let tag_constant = elem
            .attribute("tag_constant")
            .expect("Expected 'tag_constant' attribute");
        Some((type_, tag_constant))
    } else {
        None
    }
}

fn evaluate_syscall<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<(&'a str, Value, bool, bool)> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        let syscall_number = {
            let value_elem = elem
                .children()
                .filter(|e| e.is_element() && e.tag_name().name() == "SyscallNumber")
                .next()
                .expect("Expected 'SyscallNumber' child element");
            evaluate_expression(&value_elem, &state)
        };
        let does_send_ipcbuf = elem
            .attribute("does_send_ipcbuf")
            .expect("Expected 'does_send_ipcbuf' attribute")
            == "true";
        let does_receive_ipcbuf = elem
            .attribute("does_receive_ipcbuf")
            .expect("Expected 'does_receive_ipcbuf' attribute")
            == "true";
        Some((name, syscall_number, does_send_ipcbuf, does_receive_ipcbuf))
    } else {
        None
    }
}

fn evaluate_syscall_argument<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<(&'a str, &'a str, &'a str, Option<&'a str>)> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        let type_ = elem.attribute("type").expect("Expected 'type' attribute");
        let register = elem
            .attribute("register")
            .expect("Expected 'register' attribute");
        let special_value = elem.attribute("special_value");
        Some((name, type_, register, special_value))
    } else {
        None
    }
}

fn evaluate_interface<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<&'a str> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        Some(name)
    } else {
        None
    }
}

fn evaluate_method<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<(&'a str, Value)> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        let invocation_label = {
            let value_elem = elem
                .children()
                .filter(|e| e.is_element() && e.tag_name().name() == "InvocationLabel")
                .next()
                .expect("Expected 'InvocationLabel' child element");
            evaluate_expression(&value_elem, &state)
        };
        Some((name, invocation_label))
    } else {
        None
    }
}

fn evaluate_method_argument<'a>(
    elem: &'a roxmltree::Node,
    state: &HashMap<String, Value>,
) -> Option<(&'a str, &'a str, bool)> {
    if is_condition_true(&elem, &state) {
        let name = elem.attribute("name").expect("Expected 'name' attribute");
        let type_ = elem.attribute("type").expect("Expected 'type' attribute");
        let is_cap = elem
            .attribute("is_cap")
            .expect("Expected 'is_cap' attribute")
            == "true";
        Some((name, type_, is_cap))
    } else {
        None
    }
}

fn is_condition_true(elem: &roxmltree::Node, state: &HashMap<String, Value>) -> bool {
    let opt_full_condition_elem = elem
        .children()
        .filter(|e| e.is_element() && e.tag_name().name() == "FullCondition")
        .next();
    if let Some(full_condition_elem) = opt_full_condition_elem {
        evaluate_expression(&full_condition_elem, state).is_true()
    } else {
        true
    }
}

fn evaluate_expression(elem: &roxmltree::Node, state: &HashMap<String, Value>) -> Value {
    let child_elem = elem
        .children()
        .filter(|e| e.is_element())
        .next()
        .expect("Expected child element");

    match child_elem.tag_name().name() {
        "NumericLiteral" => Value::Uint(
            child_elem
                .text()
                .expect("Expected text inside xml element")
                .parse::<u64>()
                .expect("Expected a number"),
        ),
        "ConfigIdentifier" | "ConstantIdentifier" | "EnumConstantIdentifier" | "TypeIdentifier" => {
            let key = child_elem.text().expect("Expected text inside xml element");
            lookup_value(key, state)
        }
        "UnaryOperator" => evaluate_unary_operator(&child_elem, state),
        "BinaryOperator" => evaluate_binary_operator(&child_elem, state),
        _ => Value::Undefined,
    }
}

fn lookup_value(name: &str, state: &HashMap<String, Value>) -> Value {
    if state.contains_key(name) {
        state[name]
    } else {
        Value::Undefined
    }
}

fn evaluate_unary_operator(elem: &roxmltree::Node, state: &HashMap<String, Value>) -> Value {
    let op = elem
        .attribute("operator")
        .expect("Expected 'operator' attribute");

    let arg_elem = elem
        .children()
        .filter(|e| e.is_element() && e.tag_name().name() == "Arg1")
        .next()
        .expect("Expected 'Arg1' element");
    let arg_value = evaluate_expression(&arg_elem, state);

    match op {
        "UnaryPlus" => arg_value,
        "UnaryMinus" => arg_value.unary_minus(),
        "LogicalNot" => arg_value.logical_not(),
        "BitwiseNot" => arg_value.bitwise_not(),
        // A type's value is its size
        "Sizeof" => arg_value,
        "Defined" => arg_value.defined(),
        _ => Value::Undefined,
    }
}

fn evaluate_binary_operator(elem: &roxmltree::Node, state: &HashMap<String, Value>) -> Value {
    let op = elem
        .attribute("operator")
        .expect("Expected 'operator' attribute");

    let arg1_elem = elem
        .children()
        .filter(|e| e.is_element() && e.tag_name().name() == "Arg1")
        .next()
        .expect("Expected 'Arg1' element");
    let arg2_elem = elem
        .children()
        .filter(|e| e.is_element() && e.tag_name().name() == "Arg2")
        .next()
        .expect("Expected 'Arg1' element");
    let arg1_value = evaluate_expression(&arg1_elem, state);
    let arg2_value = evaluate_expression(&arg2_elem, state);

    match op {
        "Multiply" => arg1_value.multiply(&arg2_value),
        "Divide" => arg1_value.divide(&arg2_value),
        "Remainder" => arg1_value.remainder(&arg2_value),
        "Add" => arg1_value.add(&arg2_value),
        "Subtract" => arg1_value.subtract(&arg2_value),
        "LeftShift" => arg1_value.left_shift(&arg2_value),
        "RightShift" => arg1_value.right_shift(&arg2_value),
        "LessThan" => arg1_value.less_than(&arg2_value),
        "LessThanEqual" => arg1_value.less_than_equal(&arg2_value),
        "GreaterThan" => arg1_value.greater_than(&arg2_value),
        "GreaterThanEqual" => arg1_value.greater_than_equal(&arg2_value),
        "Equal" => arg1_value.equal(&arg2_value),
        "NotEqual" => arg1_value.not_equal(&arg2_value),
        "BitwiseAnd" => arg1_value.bitwise_and(&arg2_value),
        "BitwiseXor" => arg1_value.bitwise_xor(&arg2_value),
        "BitwiseOr" => arg1_value.bitwise_or(&arg2_value),
        "LogicalAnd" => arg1_value.logical_and(&arg2_value),
        "LogicalOr" => arg1_value.logical_or(&arg2_value),
        _ => Value::Undefined,
    }
}

impl Value {
    fn as_u64(&self) -> Option<u64> {
        match self {
            Self::Uint(x) => Some(*x),
            Self::Sint(x) => Some(*x as u64),
            Self::Null => None,
            Self::Undefined => None,
        }
    }

    fn is_true(&self) -> bool {
        match self {
            Self::Uint(x) => *x != 0,
            Self::Sint(x) => *x != 0,
            Self::Null => false,
            Self::Undefined => false,
        }
    }

    fn is_zero(&self) -> bool {
        match self {
            Self::Uint(x) => *x == 0,
            Self::Sint(x) => *x == 0,
            _ => false,
        }
    }

    fn unary_minus(&self) -> Value {
        match self {
            Self::Uint(x) => Self::Sint(-(*x as i64)),
            Self::Sint(x) => Self::Sint(-*x),
            Self::Null => Self::Undefined,
            Self::Undefined => Self::Undefined,
        }
    }

    fn logical_not(&self) -> Value {
        match self {
            Self::Uint(x) => Self::Uint(if *x != 0 { 0 } else { 1 }),
            Self::Sint(x) => Self::Uint(if *x != 0 { 0 } else { 1 }),
            Self::Null => Self::Undefined,
            Self::Undefined => Self::Undefined,
        }
    }

    fn bitwise_not(&self) -> Value {
        match self {
            Self::Uint(x) => Self::Uint(!*x),
            Self::Sint(x) => Self::Sint(!*x),
            Self::Null => Self::Undefined,
            Self::Undefined => Self::Undefined,
        }
    }

    fn defined(&self) -> Value {
        match self {
            Self::Uint(_) => Self::Uint(1),
            Self::Sint(_) => Self::Uint(1),
            Self::Null => Self::Uint(1),
            Self::Undefined => Self::Uint(0),
        }
    }

    fn multiply(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(*x * *y),
            (Self::Sint(x), Self::Sint(y)) => Self::Sint(*x * *y),
            (Self::Uint(x), Self::Sint(y)) => Self::Sint(*x as i64 * *y),
            (Self::Sint(x), Self::Uint(y)) => Self::Sint(*x * *y as i64),
            _ => Self::Undefined,
        }
    }

    fn divide(&self, other: &Value) -> Value {
        if other.is_zero() {
            Self::Undefined
        } else {
            match (self, other) {
                (Self::Uint(x), Self::Uint(y)) => Self::Uint(*x / *y),
                (Self::Sint(x), Self::Sint(y)) => Self::Sint(*x / *y),
                (Self::Uint(x), Self::Sint(y)) => Self::Sint(*x as i64 / *y),
                (Self::Sint(x), Self::Uint(y)) => Self::Sint(*x / *y as i64),
                _ => Self::Undefined,
            }
        }
    }

    fn remainder(&self, other: &Value) -> Value {
        if other.is_zero() {
            Self::Undefined
        } else {
            match (self, other) {
                (Self::Uint(x), Self::Uint(y)) => Self::Uint(*x % *y),
                (Self::Sint(x), Self::Sint(y)) => Self::Sint(*x % *y),
                (Self::Uint(x), Self::Sint(y)) => Self::Sint(*x as i64 % *y),
                (Self::Sint(x), Self::Uint(y)) => Self::Sint(*x % *y as i64),
                _ => Self::Undefined,
            }
        }
    }

    fn add(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(*x + *y),
            (Self::Sint(x), Self::Sint(y)) => Self::Sint(*x + *y),
            (Self::Uint(x), Self::Sint(y)) => Self::Sint(*x as i64 + *y),
            (Self::Sint(x), Self::Uint(y)) => Self::Sint(*x + *y as i64),
            _ => Self::Undefined,
        }
    }

    fn subtract(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(*x - *y),
            (Self::Sint(x), Self::Sint(y)) => Self::Sint(*x - *y),
            (Self::Uint(x), Self::Sint(y)) => Self::Sint(*x as i64 - *y),
            (Self::Sint(x), Self::Uint(y)) => Self::Sint(*x - *y as i64),
            _ => Self::Undefined,
        }
    }

    fn left_shift(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(*x << *y),
            (Self::Sint(x), Self::Sint(y)) => Self::Sint(*x << *y),
            (Self::Uint(x), Self::Sint(y)) => Self::Uint(*x << *y),
            (Self::Sint(x), Self::Uint(y)) => Self::Sint(*x << *y),
            _ => Self::Undefined,
        }
    }

    fn right_shift(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(*x >> *y),
            (Self::Sint(x), Self::Sint(y)) => Self::Sint(*x >> *y),
            (Self::Uint(x), Self::Sint(y)) => Self::Uint(*x >> *y),
            (Self::Sint(x), Self::Uint(y)) => Self::Sint(*x >> *y),
            _ => Self::Undefined,
        }
    }

    fn less_than(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(if *x < *y { 1 } else { 0 }),
            (Self::Sint(x), Self::Sint(y)) => Self::Uint(if *x < *y { 1 } else { 0 }),
            (Self::Uint(x), Self::Sint(y)) => Self::Uint(if (*x as i64) < *y { 1 } else { 0 }),
            (Self::Sint(x), Self::Uint(y)) => Self::Uint(if *x < (*y as i64) { 1 } else { 0 }),
            _ => Self::Undefined,
        }
    }

    fn less_than_equal(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(if *x <= *y { 1 } else { 0 }),
            (Self::Sint(x), Self::Sint(y)) => Self::Uint(if *x <= *y { 1 } else { 0 }),
            (Self::Uint(x), Self::Sint(y)) => Self::Uint(if (*x as i64) <= *y { 1 } else { 0 }),
            (Self::Sint(x), Self::Uint(y)) => Self::Uint(if *x <= (*y as i64) { 1 } else { 0 }),
            _ => Self::Undefined,
        }
    }

    fn greater_than(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(if *x > *y { 1 } else { 0 }),
            (Self::Sint(x), Self::Sint(y)) => Self::Uint(if *x > *y { 1 } else { 0 }),
            (Self::Uint(x), Self::Sint(y)) => Self::Uint(if (*x as i64) > *y { 1 } else { 0 }),
            (Self::Sint(x), Self::Uint(y)) => Self::Uint(if *x > (*y as i64) { 1 } else { 0 }),
            _ => Self::Undefined,
        }
    }

    fn greater_than_equal(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(if *x >= *y { 1 } else { 0 }),
            (Self::Sint(x), Self::Sint(y)) => Self::Uint(if *x >= *y { 1 } else { 0 }),
            (Self::Uint(x), Self::Sint(y)) => Self::Uint(if (*x as i64) >= *y { 1 } else { 0 }),
            (Self::Sint(x), Self::Uint(y)) => Self::Uint(if *x >= (*y as i64) { 1 } else { 0 }),
            _ => Self::Undefined,
        }
    }

    fn equal(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(if *x == *y { 1 } else { 0 }),
            (Self::Sint(x), Self::Sint(y)) => Self::Uint(if *x == *y { 1 } else { 0 }),
            (Self::Uint(x), Self::Sint(y)) => Self::Uint(if (*x as i64) == *y { 1 } else { 0 }),
            (Self::Sint(x), Self::Uint(y)) => Self::Uint(if *x == (*y as i64) { 1 } else { 0 }),
            _ => Self::Undefined,
        }
    }

    fn not_equal(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(if *x != *y { 1 } else { 0 }),
            (Self::Sint(x), Self::Sint(y)) => Self::Uint(if *x != *y { 1 } else { 0 }),
            (Self::Uint(x), Self::Sint(y)) => Self::Uint(if (*x as i64) != *y { 1 } else { 0 }),
            (Self::Sint(x), Self::Uint(y)) => Self::Uint(if *x != (*y as i64) { 1 } else { 0 }),
            _ => Self::Undefined,
        }
    }

    fn bitwise_and(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(*x & *y),
            (Self::Sint(x), Self::Sint(y)) => Self::Sint(*x & *y),
            (Self::Uint(x), Self::Sint(y)) => Self::Sint(*x as i64 & *y),
            (Self::Sint(x), Self::Uint(y)) => Self::Sint(*x & *y as i64),
            _ => Self::Undefined,
        }
    }

    fn bitwise_xor(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(*x ^ *y),
            (Self::Sint(x), Self::Sint(y)) => Self::Sint(*x ^ *y),
            (Self::Uint(x), Self::Sint(y)) => Self::Sint(*x as i64 ^ *y),
            (Self::Sint(x), Self::Uint(y)) => Self::Sint(*x ^ *y as i64),
            _ => Self::Undefined,
        }
    }

    fn bitwise_or(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(*x | *y),
            (Self::Sint(x), Self::Sint(y)) => Self::Sint(*x | *y),
            (Self::Uint(x), Self::Sint(y)) => Self::Sint(*x as i64 | *y),
            (Self::Sint(x), Self::Uint(y)) => Self::Sint(*x | *y as i64),
            _ => Self::Undefined,
        }
    }

    fn logical_and(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(if *x != 0 && *y != 0 { 1 } else { 0 }),
            (Self::Sint(x), Self::Sint(y)) => Self::Uint(if *x != 0 && *y != 0 { 1 } else { 0 }),
            (Self::Uint(x), Self::Sint(y)) => Self::Uint(if *x != 0 && *y != 0 { 1 } else { 0 }),
            (Self::Sint(x), Self::Uint(y)) => Self::Uint(if *x != 0 && *y != 0 { 1 } else { 0 }),
            _ => Self::Undefined,
        }
    }

    fn logical_or(&self, other: &Value) -> Value {
        match (self, other) {
            (Self::Uint(x), Self::Uint(y)) => Self::Uint(if *x != 0 || *y != 0 { 1 } else { 0 }),
            (Self::Sint(x), Self::Sint(y)) => Self::Uint(if *x != 0 || *y != 0 { 1 } else { 0 }),
            (Self::Uint(x), Self::Sint(y)) => Self::Uint(if *x != 0 || *y != 0 { 1 } else { 0 }),
            (Self::Sint(x), Self::Uint(y)) => Self::Uint(if *x != 0 || *y != 0 { 1 } else { 0 }),
            _ => Self::Undefined,
        }
    }
}

impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Uint(x) => write!(f, "{}", x),
            Self::Sint(x) => write!(f, "{}", x),
            Self::Null => write!(f, "Null"),
            Self::Undefined => write!(f, "Undefined"),
        }
    }
}
