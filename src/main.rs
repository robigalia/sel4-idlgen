// This utility translates the libsel4 C library into an XML IDL.
//
// While it tries to automate the process as much as possible, there are still manual steps.
//
// It was created because maintaining a libsel4 implementation in pure rust is difficult.
// The idea is to put some of the complex, hard-to-maintain code and domain-specific
// knowledge about seL4 into this utility so that downstream libraries can be generated from
// a clean IDL.
//
// This utility will have to be lovingly updated for each seL4 release to make sure
// it still works and generates a correct IDL.
//
// Hopefully in the future seL4 will publish an official, complete IDL so that this tool is no
// longer necessary.

mod cparser;
mod expression;

use std::collections::Bound::{Included, Unbounded};
use std::collections::{BTreeMap, BTreeSet};
use std::fs;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};
use std::path::{Path, PathBuf};

use clang::{Clang, EntityKind, EntityVisitResult, Index, TypeKind};
use clap::{crate_version, App, Arg};
use log::{debug, info, trace, warn};
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use tempfile::NamedTempFile;
use walkdir::WalkDir;

use cparser::*;
use expression::{Expression, Identifier, Operator};

const OUTPUT_FILE_VERSION: usize = 1;

// HACK: These definitions are declared with an empty value in all files so the parser doesn't get
// confused when it sees them
const REQUIRED_DEFINES: &'static [&'static str] = &[
    "CONST",
    "LIBSEL4_INLINE",
    "LIBSEL4_INLINE_FUNC",
    "PURE",
    "PACKED",
    "SEL4_PACKED",
];

// Put macros here that are used internally by libsel4 and not useful to export
const IGNORED_MACROS: &'static [&'static str] = &[
    "CONST",
    "GET_MRS",
    "LIBSEL4_BIT",
    "LIBSEL4_INLINE",
    "LIBSEL4_INLINE_FUNC",
    "LIBSEL4_MCS_REPLY",
    "LIBSEL4_UNUSED",
    "MAYBE_GET_MRS",
    "MCS_COND",
    "MCS_MAYBE_GET_MR1",
    "MCS_PARAM",
    "MCS_PARAM_DECL",
    "MCS_REPLY",
    "MCS_REPLY_DECL",
    "MR_ARGS",
    "PURE",
    "RECV_MRS",
    "REPLY",
    "SEL4_COMPILE_ASSERT",
    "SEL4_DEPRECATED",
    "SEL4_DEPRECATE_MACRO",
    "SEL4_FORCE_LONG_ENUM",
    "SEL4_OFFSETOF",
    "SEL4_PACKED",
    "SEL4_SIZE_SANITY",
    "seL4_Assert",
    "seL4_CompileTimeAssert",
    "seL4_DebugAssert",
    "seL4_Fail",
    "seL4_Null",
    "SEL4_GET_IPCBUF_SCALE",
    "SEL4_GET_IPCBUF",
    "__GCC_HAVE_DWARF2_CFI_ASM",
];

// HACK: Due to the hacky way we deal with the preprocessor, some lines will inevitibly get
// messed up. When our preprocessor sees the first line in each tuple, it will replace it with
// the second value.
const REPLACEMENT_LINES: &'static [(&'static str, &'static str)] = &[
    (
        "    seL4_NumInitialCaps         = 13",
        "    seL4_NumInitialCaps         = 13,",
    ),
    (
        "    seL4_NumInitialCaps         = 15",
        "    seL4_NumInitialCaps         = 15,",
    ),
];

// primitive types our consumers are expected to understand
const PRIMITIVE_TYPES: &'static [&'static str] = &[
    "char",
    "unsigned char",
    "signed char",
    "unsigned short",
    "signed short",
    "unsigned int",
    "signed int",
    "unsigned long",
    "signed long",
    "unsigned long long",
    "signed long long",
];

// HACK: Some non-primitive types are used without defining them, define them here
//    ("bad type", "replaced type")
pub const REPLACEMENT_TYPES: &'static [(&'static str, &'static str)] =
    &[("uint32_t", "seL4_Uint32"), ("uint64_t", "seL4_Uint64")];

const RANDOM_IDENTIFIER_MARKER: &'static str = "__IDLGEN_RANDOM_";
const RANDOM_IDENTIFIER_RANDOMNESS_LENGTH: usize = 10;

const GENERATED_SYSCALL_ENUM_NAME: &'static str = "seL4_IDLGEN_SyscallNumbers";
const GENERATED_INVOCATION_ENUM_NAME: &'static str = "seL4_IDLGEN_InvocationLabels";

const MAXIMUM_DEPENDENCY_GRAPH_DEPTH: u32 = 30;

#[derive(Clone, Copy, Debug, Eq, PartialOrd, Ord, PartialEq)]
enum Bits {
    Bits32,
    Bits64,
}

// Implied conditions based on the directory of the current file being processed
#[derive(Clone, Debug, Eq, PartialOrd, Ord, PartialEq)]
struct ImpliedConditions {
    arch: Option<String>,
    platform: Option<String>,
    bits: Option<Bits>,
}

#[derive(Clone, Debug)]
struct ImpliedConditionExpressions {
    arch: Option<Expression>,
    platform: Option<Expression>,
    bits: Option<Expression>,
}

#[derive(Debug)]
struct Constant {
    is_error: bool,
    name: String,
    condition: Option<String>,
    condition_expression: Option<Expression>,
    implied_conditions: ImpliedConditions,
    implied_condition_expressions: ImpliedConditionExpressions,
    value: Expression,
}

#[derive(Clone, Debug)]
struct Enum {
    is_error: bool,
    name: Option<String>,
    condition: Option<String>,
    condition_expression: Option<Expression>,
    implied_conditions: ImpliedConditions,
    implied_condition_expressions: ImpliedConditionExpressions,
    constants: Vec<EnumConstant>,
}

#[derive(Clone, Debug)]
struct EnumConstant {
    name: String,
    condition: Option<String>,
    condition_expression: Option<Expression>,
    value: Option<Expression>,
}

#[derive(Debug)]
struct Typedef {
    is_error: bool,
    name: String,
    condition: Option<String>,
    condition_expression: Option<Expression>,
    implied_conditions: ImpliedConditions,
    implied_condition_expressions: ImpliedConditionExpressions,
    type_: String,
}

#[derive(Debug)]
struct Struct {
    is_error: bool,
    name: Option<String>,
    condition: Option<String>,
    condition_expression: Option<Expression>,
    implied_conditions: ImpliedConditions,
    implied_condition_expressions: ImpliedConditionExpressions,
    fields: Vec<StructField>,
    is_packed: bool,
}

#[derive(Debug)]
struct StructField {
    name: String,
    condition: Option<String>,
    condition_expression: Option<Expression>,
    type_: StructFieldType,
}

#[derive(Debug)]
pub enum StructFieldType {
    Simple(String),
    Pointer(String),
    Array(String, Expression),
}

#[derive(Debug)]
struct Interface {
    name: String,
    implied_conditions: ImpliedConditions,
    implied_condition_expressions: ImpliedConditionExpressions,
    methods: Vec<Method>,
}

#[derive(Debug)]
struct Method {
    is_error: bool,
    name: String,
    condition: Option<String>,
    condition_expression: Option<Expression>,
    invocation_label: Option<Expression>,
    arguments: Vec<MethodArgument>,
    return_values: Vec<MethodArgument>,
}

#[derive(Debug)]
struct MethodArgument {
    name: String,
    type_: String,
    is_cap: Option<bool>,
}

#[derive(Debug)]
struct Syscall {
    is_error: bool,
    group: String,
    name: String,
    condition: Option<String>,
    condition_expression: Option<Expression>,
    syscall_number: Option<Expression>,
    does_send_ipcbuf: bool,
    does_receive_ipcbuf: bool,
    arguments: Vec<SyscallArgument>,
    return_values: Vec<SyscallArgument>,
}

#[derive(Debug)]
struct SyscallArgument {
    name: String,
    type_: String,
    symbolic_register: String,
    special_value: Option<String>,
}

#[derive(Debug)]
struct SyscallRegister {
    arch: String,
    arch_expression: Option<Expression>,
    symbolic_register: String,
    machine_register: String,
}

#[derive(Debug)]
struct TaggedUnion {
    is_error: bool,
    name: String,
    condition: Option<String>,
    condition_expression: Option<Expression>,
    implied_conditions: ImpliedConditions,
    implied_condition_expressions: ImpliedConditionExpressions,
    tag_name: String,
    fields: Vec<TaggedUnionField>,
}

#[derive(Debug)]
struct TaggedUnionField {
    condition: Option<String>,
    condition_expression: Option<Expression>,
    tag_constant: u32,
    type_: String,
}

#[derive(Debug)]
struct BitfieldStruct {
    is_error: bool,
    name: String,
    condition: Option<String>,
    condition_expression: Option<Expression>,
    implied_conditions: ImpliedConditions,
    implied_condition_expressions: ImpliedConditionExpressions,
    fields: Vec<BitfieldStructField>,
}

#[derive(Debug)]
enum BitfieldStructField {
    Field {
        condition: Option<String>,
        condition_expression: Option<Expression>,
        name: String,
        bits: u32,
    },
    Padding {
        condition: Option<String>,
        condition_expression: Option<Expression>,
        bits: u32,
    },
}

#[derive(Debug)]
enum NameLookup {
    Constant(usize),
    Enum(usize),
    EnumConstant {
        enum_index: usize,
        enum_name: String,
        enum_constant_index: usize,
    },
    Type(usize),
    Struct(usize),
    BitfieldStruct(usize),
    TaggedUnion(usize),
    InterfaceMethod {
        interface_root_index: usize,
        interface_index: usize,
        method_index: usize,
    },
    Syscall(usize),
}

#[derive(Debug)]
struct ImpliedConditionLookups {
    arch_to_condition: BTreeMap<String, Expression>,
    platform_to_condition: BTreeMap<String, Expression>,
    bits_to_condition: BTreeMap<Bits, Expression>,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum InterfaceType {
    Generic = 0,
    Arch,
    ArchType,
}

// This is poorly named, it is a collection of related information that gets loaded from parsing
// files that makes sense to treat as a unit
#[derive(Debug)]
struct AppState {
    constants: Vec<Constant>,
    enums: Vec<Enum>,
    // The array of three here represents the three different enums that will be generated for the
    // invocation labels: one array entry for each enum in InterfaceType
    interfaces: [Vec<Interface>; 3],
    types: Vec<Typedef>,
    structs: Vec<Struct>,
    bitfield_structs: Vec<BitfieldStruct>,
    tagged_unions: Vec<TaggedUnion>,
    syscalls: Vec<Syscall>,
    registers: Vec<SyscallRegister>,
}

fn main() -> io::Result<()> {
    let m = App::new("sel4-idlgen")
        .version(crate_version!())
        .arg(
            Arg::with_name("verbosity")
                .short("v")
                .multiple(true)
                .help("Increase message verbosity, can be used multiple times"),
        )
        .arg(
            Arg::with_name("quiet")
                .short("q")
                .help("Silence all output"),
        )
        .arg(
            Arg::with_name("sel4-source-dir")
                .short("k")
                .long("sel4-source-dir")
                .help("Path to the sel4 kernel source code")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("machine-register-file")
                .short("r")
                .long("machine-register-file")
                .help("Path to machine register definition file")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("syscall-file")
                .short("s")
                .long("syscall-file")
                .help("Path to the syscall definition file")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("output-file")
                .short("o")
                .long("output-file")
                .help("File to write output to. If omitted will write to stdout.")
                .takes_value(true),
        )
        .get_matches();

    let verbose = 1 + m.occurrences_of("verbosity") as usize;
    let quiet = m.is_present("quiet");
    let sel4_source_dir = m.value_of("sel4-source-dir").unwrap();
    let machine_register_file = m.value_of("machine-register-file").unwrap();
    let syscall_file = m.value_of("syscall-file").unwrap();
    let output_file = m.value_of("output-file");

    stderrlog::new()
        .verbosity(verbose)
        .quiet(quiet)
        .module(module_path!())
        .init()
        .unwrap();

    let mut appstate = AppState {
        constants: Vec::new(),
        enums: Vec::new(),
        types: Vec::new(),
        structs: Vec::new(),
        bitfield_structs: Vec::new(),
        tagged_unions: Vec::new(),
        registers: Vec::new(),
        syscalls: Vec::new(),
        interfaces: [Vec::new(), Vec::new(), Vec::new()],
    };

    // parse the interesting files in libsel4 and pull the constants, enums, types, structs,
    // unions, syscalls, and api interfaces into our state tables

    let path = PathBuf::from(sel4_source_dir).join("libsel4");
    for entry in WalkDir::new(path)
        .sort_by(|a, b| a.file_name().cmp(b.file_name()))
        .into_iter()
        .filter_map(|e| e.ok())
    {
        let fname = entry.file_name().to_string_lossy();

        // pull out implied conditions based on the directory we are in
        let implied_conditions = ImpliedConditions {
            arch: get_arch_from_path(entry.path()),
            platform: get_plat_from_path(entry.path()),
            bits: get_bits_from_path(entry.path()),
        };

        if fname.ends_with(".c") || fname.ends_with(".h") {
            // don't bother with the old stuff
            if fname != "deprecated.h" {
                info!("-- Processing {}", entry.path().to_string_lossy());

                // pull out conditions from C preprocessor statements and create a temporary file
                // without them that we will parse (because libclang doesn't expose them to us)
                let (conditions, temp_file) = preprocess_file(entry.path(), true)?;

                // then parse the temp file and add interesting stuff into our state
                parse_c_file(
                    temp_file.path(),
                    &mut appstate,
                    &implied_conditions,
                    &conditions,
                )?;
            }
        } else if fname.ends_with(".xml") {
            info!("-- Processing {}", entry.path().to_string_lossy());

            if fname == "syscall.xml" {
                parse_syscall_xml_file(entry.path(), &mut appstate)?;
            } else {
                parse_api_xml_file(entry.path(), &mut appstate, &implied_conditions)?;
            }
        } else if fname.ends_with(".bf") {
            info!("-- Processing {}", entry.path().to_string_lossy());

            let (conditions, temp_file) = preprocess_file(entry.path(), false)?;

            parse_bf_file(
                temp_file.path(),
                &mut appstate,
                &implied_conditions,
                &conditions,
            )?;
        }
    }

    // parse the interesting cmake files in the sel4 kernel

    let mut implied_condition_lookups = ImpliedConditionLookups {
        arch_to_condition: BTreeMap::new(),
        bits_to_condition: BTreeMap::new(),
        platform_to_condition: BTreeMap::new(),
    };

    for entry in WalkDir::new(sel4_source_dir)
        .sort_by(|a, b| a.file_name().cmp(b.file_name()))
        .into_iter()
        .filter_map(|e| e.ok())
    {
        let fname = entry.file_name().to_string_lossy();

        if fname.ends_with(".cmake") {
            parse_cmake_file(entry.path(), &mut implied_condition_lookups)?;
        }
    }

    // Pull in user-defined syscall data
    // TODO: Investigate a way to generate this information automatically

    info!("-- Processing {}", machine_register_file);
    parse_user_generated_machine_registers_file(machine_register_file, &mut appstate)?;

    info!("-- Processing {}", syscall_file);
    parse_user_generated_syscall_file(syscall_file, &mut appstate)?;

    // Process and validate the data

    generate_bits_to_condition(&mut implied_condition_lookups);
    generate_syscall_enum(&mut appstate);
    generate_invocation_enum(&mut appstate);
    generate_missing_arch_invocation_enums(&mut appstate, &implied_condition_lookups);
    give_unnamed_enums_a_name(&mut appstate);
    validate_everything_has_a_name(&mut appstate);
    validate_all_syscalls_have_arguments(&mut appstate);

    // N.B. Cannot change condition statements after this point

    generate_condition_expressions(&mut appstate, &implied_condition_lookups);

    // N.B. Cannot add or remove elements in appstate after this point

    let name_lookup = generate_name_lookup_table(&mut appstate);
    trace!("\n\nName Lookup Table:\n\n{:#?}", name_lookup);
    validate_num_entries_in_name_lookup(&appstate, &name_lookup);
    resolve_identifier_references_in_expressions(&mut appstate, &name_lookup);
    validate_all_references_exist(&mut appstate, &name_lookup);

    // TODO: Verify all architectures we pulled in from the seL4 source code have been given
    // register definitions in the user-generated file

    let (depmap, rev_depmap) = generate_dependency_map(&appstate, &name_lookup);
    trace!("\n\nDependency Map:\n\n{:#?}", depmap);
    trace!("\n\nReverse Dependency Map:\n\n{:#?}", rev_depmap);
    identify_caps_in_interface_methods(&mut appstate, &depmap);

    // N.B. Nothing else can be marked as an error after this point

    propagate_errors_to_dependencies(&mut appstate, &name_lookup, &rev_depmap);
    let sorted_names = topological_sort_dependency_map(&rev_depmap);
    validate_topological_sort_contains_all_names(&sorted_names, &name_lookup);

    trace!("\n\nSorted Names:\n\n{:#?}", sorted_names);

    trace!("\n\nappstate:\n\n{:#?}", appstate);

    // Write output

    if let Some(f) = output_file {
        generate_output(
            &appstate,
            &name_lookup,
            &sorted_names,
            std::fs::File::create(f).expect("Unable to create output file"),
        );
    } else {
        generate_output(&appstate, &name_lookup, &sorted_names, std::io::stdout());
    }

    Ok(())
}

fn get_arch_from_path(path: &Path) -> Option<String> {
    let mut prev_component = None;

    for parent in path.ancestors() {
        if let Some(fname) = parent.file_name() {
            let fnamestr = fname.to_string_lossy();
            if fnamestr == "arch_include" || fnamestr == "sel4_arch_include" {
                return prev_component;
            }
            prev_component = Some(fnamestr.into_owned());
        }
    }

    None
}

fn get_plat_from_path(path: &Path) -> Option<String> {
    let mut prev_component = None;

    for parent in path.ancestors() {
        if let Some(fname) = parent.file_name() {
            let fnamestr = fname.to_string_lossy();
            if fnamestr == "sel4_plat_include" {
                return prev_component;
            }
            prev_component = Some(fnamestr.into_owned());
        }
    }

    None
}

fn get_bits_from_path(path: &Path) -> Option<Bits> {
    let mut prev_component: Option<String> = None;

    for parent in path.ancestors() {
        if let Some(fname) = parent.file_name() {
            let fnamestr = fname.to_string_lossy();
            if fnamestr == "mode_include" {
                if let Some(c) = prev_component {
                    return match c.as_ref() {
                        "32" => Some(Bits::Bits32),
                        "64" => Some(Bits::Bits64),
                        _ => panic!("Unknown mode in directory structure {:?}", path),
                    };
                } else {
                    return None;
                }
            }
            prev_component = Some(fnamestr.into_owned());
        }
    }

    None
}

struct PreprocessorState {
    // the current conditional, populated from most recent #if, #ifdef, #ifndef, or #elif
    condition: String,
    // The current condition is negated and appended here when we hit an #elif or #else
    negated_conditions: String,
}

// Write all non-preprocessor lines to a new temporary file. Returns the temporary
// file and a BTreeMap that maps line numbers in the temporary file to all conditions that exist
// for that line number. If a line number is not present in the BTreeMap, the conditions for the
// highest previous line number that is present are valid for that line.
fn preprocess_file<F>(
    f: F,
    add_randomness: bool,
) -> io::Result<(BTreeMap<u32, String>, NamedTempFile)>
where
    F: AsRef<Path>,
{
    // TODO: If this hacky code becomes a maintenance burden write a proper C preprocessor

    let infile = File::open(f)?;
    let reader = BufReader::new(infile);
    let mut outfile = NamedTempFile::new()?;

    debug!("Writing preprocessed file to {}", outfile.path().display());

    let mut multiline = false;
    let mut row = 1;
    let mut condition = String::new();
    let mut pp_stack = Vec::new();
    let mut condition_by_source_line = BTreeMap::new();

    trace!("PREPROCESSOR: Writing REQUIRED_DEFINES to start of file");

    // add empty definitions for some definitions so that the parser doesn't get confused
    for def in REQUIRED_DEFINES {
        let mut new_line = String::new();
        new_line.push_str("#define ");
        new_line.push_str(def);
        trace!("{}: {}", row, new_line);
        outfile.write_all(new_line.as_bytes())?;
        outfile.write_all("\n".as_bytes())?;
        row += 1;
    }

    for line in reader.lines() {
        let orig_line = line?;
        let line = orig_line.trim();
        let mut preprocessor = false;

        if multiline {
            // We're in a multiline preprocessor statement
            trace!(
                "PREPROCESSOR: Identified multiline preprocessor line: <{}>",
                orig_line
            );
            condition.push_str(line);
            multiline = false;
            preprocessor = true;
        } else {
            if line.starts_with("#if") || line.starts_with("#el") || line.starts_with("#endif") {
                trace!(
                    "PREPROCESSOR: Identified preprocessor line: <{}>",
                    orig_line
                );
                // Preprocessor line that we care about
                condition.push_str(line);
                preprocessor = true;
            }
        }

        if preprocessor {
            // This is a preprocessor line that we care about
            if condition.ends_with('\\') {
                trace!("PREPROCESSOR: Preprocessor statement continued on next line");
                // preprocessor statement is continued on the next line
                condition.pop();
                condition = condition.trim().to_owned();
                multiline = true;
                continue;
            } else {
                // we have the complete preprocessor statement; decide what to do with it
                trace!(
                    "PREPROCESSOR: Handling preprocessor statement: <{}>",
                    condition
                );
                if condition.starts_with("#ifdef") {
                    let mut new_condition = "defined(".to_owned();
                    new_condition.push_str(condition.trim_start_matches("#ifdef").trim());
                    new_condition.push_str(")");
                    trace!("PREPROCESSOR: New condition: {}", new_condition);
                    if is_ignored_condition(&new_condition) {
                        trace!("PREPROCESSOR: ^ Condition is IGNORED, clearing it but still pushing it into stack");
                        new_condition.clear();
                    }
                    trace!(
                        "PREPROCESSOR: PUSH onto preprocessor stack with existing depth of ({})",
                        pp_stack.len()
                    );
                    pp_stack.push(PreprocessorState {
                        condition: new_condition,
                        negated_conditions: String::new(),
                    });
                    condition_by_source_line.insert(row, get_full_condition(&pp_stack));
                    trace!(
                        "PREPROCESSOR: Line {} full condition: {}",
                        row,
                        get_full_condition(&pp_stack)
                    );
                } else if condition.starts_with("#ifndef") {
                    let mut new_condition = "!defined(".to_owned();
                    new_condition.push_str(condition.trim_start_matches("#ifndef").trim());
                    new_condition.push_str(")");
                    trace!("PREPROCESSOR: New condition: {}", new_condition);
                    if is_ignored_condition(&new_condition) {
                        trace!("PREPROCESSOR: ^ Condition is IGNORED, clearing it but still pushing it into stack");
                        new_condition.clear();
                    }
                    trace!(
                        "PREPROCESSOR: PUSH onto preprocessor stack with existing depth of ({})",
                        pp_stack.len()
                    );
                    pp_stack.push(PreprocessorState {
                        condition: new_condition,
                        negated_conditions: String::new(),
                    });
                    condition_by_source_line.insert(row, get_full_condition(&pp_stack));
                    trace!(
                        "PREPROCESSOR: Line {} full condition: {}",
                        row,
                        get_full_condition(&pp_stack)
                    );
                } else if condition.starts_with("#if") {
                    let mut new_condition = condition.trim_start_matches("#if").trim().to_owned();
                    trace!("PREPROCESSOR: New condition: {}", new_condition);
                    if is_ignored_condition(&new_condition) {
                        trace!("PREPROCESSOR: ^ Condition is IGNORED, clearing it but still pushing it into stack");
                        new_condition.clear();
                    }
                    trace!(
                        "PREPROCESSOR: PUSH onto preprocessor stack with existing depth of ({})",
                        pp_stack.len()
                    );
                    pp_stack.push(PreprocessorState {
                        condition: new_condition,
                        negated_conditions: String::new(),
                    });
                    condition_by_source_line.insert(row, get_full_condition(&pp_stack));
                    trace!(
                        "PREPROCESSOR: Line {} full condition: {}",
                        row,
                        get_full_condition(&pp_stack)
                    );
                } else if condition.starts_with("#elif") {
                    let mut new_condition = condition.trim_start_matches("#elif").trim().to_owned();
                    trace!("PREPROCESSOR: New condition: {}", new_condition);
                    let mut state = pp_stack.pop().expect("Unmatched #elif");
                    if !state.condition.is_empty() {
                        if !state.negated_conditions.is_empty() {
                            state.negated_conditions.push_str(" && ");
                        }
                        state.negated_conditions.push_str("!(");
                        state.negated_conditions.push_str(&state.condition);
                        state.negated_conditions.push_str(")");
                    }
                    if is_ignored_condition(&new_condition) {
                        trace!("PREPROCESSOR: ^ Condition is IGNORED, clearing it");
                        new_condition.clear();
                    }
                    state.condition = new_condition;
                    trace!(
                        "PREPROCESSOR: Condition on top of stack replaced with: {} && {}",
                        state.negated_conditions,
                        state.condition
                    );
                    pp_stack.push(state);
                    condition_by_source_line.insert(row, get_full_condition(&pp_stack));
                    trace!(
                        "PREPROCESSOR: Line {} full condition: {}",
                        row,
                        get_full_condition(&pp_stack)
                    );
                } else if condition.starts_with("#else") {
                    let mut state = pp_stack.pop().expect("Unmatched #else");
                    if !state.condition.is_empty() {
                        if !state.negated_conditions.is_empty() {
                            state.negated_conditions.push_str(" && ");
                        }
                        state.negated_conditions.push_str("!(");
                        state.negated_conditions.push_str(&state.condition);
                        state.negated_conditions.push_str(")");
                        state.condition.clear();
                    }
                    trace!(
                        "PREPROCESSOR: Condition on top of stack replaced with: {} && {}",
                        state.negated_conditions,
                        state.condition
                    );
                    pp_stack.push(state);
                    condition_by_source_line.insert(row, get_full_condition(&pp_stack));
                    trace!(
                        "PREPROCESSOR: Line {} full condition: #if {}",
                        row,
                        get_full_condition(&pp_stack)
                    );
                } else if condition.starts_with("#endif") {
                    trace!(
                        "PREPROCESSOR: POP from preprocessor stack with existing depth of ({})",
                        pp_stack.len()
                    );
                    pp_stack.pop().expect("Unmatched #endif");
                    condition_by_source_line.insert(row, get_full_condition(&pp_stack));
                    trace!(
                        "PREPROCESSOR: Line {} full condition: {}",
                        row,
                        get_full_condition(&pp_stack)
                    );
                }
                condition.clear();
            }
        } else {
            // Not an interesting C preprocessor statement; if this isn't an include, put it
            // in the temporary file to get parsed by clang
            if line.starts_with("#include") {
                trace!("IGNORING LINE: {}", orig_line);
            } else {
                // HACK: Apply a hard-coded patch to this line if required
                let line = if let Some((_, replacement)) = REPLACEMENT_LINES
                    .iter()
                    .filter(|(l, _)| l == &orig_line)
                    .take(1)
                    .nth(0)
                {
                    debug!("PREPROCESSOR: Replacing original line '{}' with REPLACEMENT_LINES entry '{}'", orig_line, replacement);
                    replacement
                } else {
                    &orig_line[..]
                };

                // HACK: Sometimes clang will get confused after we remove preprocessor statements
                // when there's the same name used multiple times. libsel4 usually has "seL4" in
                // all their identifiers, so prepend "seL4" with a random value on each line to
                // make clang happy. We'll have to remove these as we pull them out of clang.
                let line = if add_randomness {
                    add_randomness_to_identifiers(line)
                } else {
                    line.to_string()
                };

                trace!("{}: {}", row, line);
                outfile.write_all(line.as_bytes())?;
                outfile.write_all("\n".as_bytes())?;
                row += 1
            }
        }
    }

    outfile.flush()?;
    Ok((condition_by_source_line, outfile))
}

fn is_ignored_condition(condition: &str) -> bool {
    // Some conditions aren't relevent to seL4 config, ignore them
    condition.contains("__")
        || condition.contains("HAVE_AUTOCONF")
        || condition.contains("_TYPES_H")
}

fn get_full_condition(stack: &Vec<PreprocessorState>) -> String {
    // the full condition is a logical and of all the conditions that are in scope
    let mut result = String::new();

    if stack.is_empty() {
        return result;
    }

    for state in stack {
        if !state.condition.is_empty() || !state.negated_conditions.is_empty() {
            if !result.is_empty() {
                result.push_str(" && ");
            }

            match (
                state.negated_conditions.is_empty(),
                state.condition.is_empty(),
            ) {
                (false, false) => {
                    result.push_str("(");
                    result.push_str(&state.negated_conditions);
                    result.push_str(" && (");
                    result.push_str(&state.condition);
                    result.push_str("))");
                }
                (true, false) => {
                    result.push_str("(");
                    result.push_str(&state.condition);
                    result.push_str(")");
                }
                (false, true) => result.push_str(&state.negated_conditions),
                (true, true) => panic!("unreachable?!"),
            }
        }
    }

    result
}

// HACK: To prevent clang from silently ignoring stuff that has the same name on different lines
fn add_randomness_to_identifiers(line: &str) -> String {
    let random_marker = {
        let mut s = String::new();
        s.push_str(RANDOM_IDENTIFIER_MARKER);
        s.push_str(
            thread_rng()
                .sample_iter(&Alphanumeric)
                .take(RANDOM_IDENTIFIER_RANDOMNESS_LENGTH)
                .collect::<String>()
                .as_ref(),
        );
        s.push_str("__seL4_");
        s
    };

    line.replace("seL4_", &random_marker)
}

fn parse_c_file<F>(
    f: F,
    appstate: &mut AppState,
    implied_conditions: &ImpliedConditions,
    conditions: &BTreeMap<u32, String>,
) -> io::Result<()>
where
    F: Into<PathBuf>,
{
    // We've removed all the #ifdef's from the C file we're processing and we have a table
    // showing us what conditional statements are active for each line in the C file. Now,
    // use clang to parse the C file for the things we're interested in, tying them to their
    // relevent conditions from the conditions map, and adding them to the AppState.

    trace!("Parsing C file");
    trace!("Creating clang object");
    let clang = Clang::new().unwrap();

    trace!("Creating index");
    let index = Index::new(
        &clang, true,  // exclude declarations from precompiled headers?
        false, // print diagnostics?
    );
    trace!("    {:?}", index);

    let mut parser = index.parser(f);
    trace!("Creating parser");
    parser
        .arguments(&["-xc", "-std=gnu11", "-nostdinc"])
        .detailed_preprocessing_record(true)
        .incomplete(true)
        .keep_going(true)
        .skip_function_bodies(true);
    trace!("    {:?}", parser);

    trace!("Creating translation unit");
    let tu = parser.parse().expect("Unable to parse file");
    trace!("    {:?}", tu);

    trace!("Grabbing root entity from translation unit");
    let root_entity = tu.get_entity();
    trace!("    {:?}", root_entity);

    trace!("Visiting each AST node");

    root_entity.visit_children(|cur, parent| {
        if !cur.is_in_system_header() {
            let opt_cur_name = opt_remove_randomness(cur.get_name());
            let opt_parent_name = opt_remove_randomness(parent.get_name());

            if cur.get_kind() as u32 == 441 {
                // This value isn't in the EntityKind enum so we crash if we try to print its name
                trace!("AST node: Unknown kind 441 with name {:?} at {:?}", opt_cur_name, cur.get_location());
            } else {
                trace!("AST node: {:?}", cur);
            }
            trace!("  Parent: {:?}", parent);
            match (cur.get_kind(), parent.get_kind()) {
                // CONSTANTS
                (EntityKind::MacroDefinition, EntityKind::TranslationUnit) => {
                    let name = remove_randomness(opt_cur_name.unwrap());
                    let line = cur.get_location().unwrap().get_file_location().line;

                    // skip the initial definitions we added ourselves
                    if line > REQUIRED_DEFINES.len() as u32 {
                        if is_ignored_constant(&name) {
                            trace!("IGNORING macro definition for {}", name);
                        } else if cur.is_function_like_macro() {
                            warn!("MACRO: Definition of {} is a function-like macro that I don't know how to evaluate, ignoring it", name);
                        } else {
                            let (value, is_error) = {
                                trace!("Parsing constant: '{}'", get_source_text(&cur));
                                if let Ok(v) = parse_macro_value(&cur) {
                                    (v, false)
                                } else {
                                    warn!("MACRO: Unable to parse macro value '{}', marking constant as error", get_source_text(&cur));
                                    // TODO: Make value in Constant struct an Option
                                    (Expression::Identifier(Identifier::Unresolved("Parse error".to_string())), true)
                                }
                            };
                            let condition = get_condition_at_line(line, conditions);

                            info!("Adding constant {} = {:?}   {:?} Condition {:?}", name, value, implied_conditions, condition);

                            appstate.constants.push(Constant {
                                name,
                                value,
                                condition,
                                implied_conditions: implied_conditions.clone(),
                                condition_expression: None,
                                implied_condition_expressions: ImpliedConditionExpressions {
                                    arch: None,
                                    bits: None,
                                    platform: None,
                                },
                                is_error,
                            });
                        }
                    } else {
                        trace!("Ignoring MacroDefinition because it's one of the ones we added from REQUIRED_DEFINEDS");
                    }

                    // TODO: Change to Continue, just recursing here out of curiosity
                    EntityVisitResult::Recurse
                },
                // ENUMS
                // for some reason enums are listed multiple times if they have a
                // typedef -- first with a parent of TranslationUnit, second
                // with a parent of a typedef or vardecl.
                // Populate our enum table the first time around, then the second
                // time around pull in the typedef name.
                (EntityKind::EnumDecl, EntityKind::TranslationUnit) => {
                    let name = opt_cur_name;
                    let line = cur.get_location().unwrap().get_file_location().line;
                    let condition = get_condition_at_line(line, conditions);

                    info!("Adding enum {:?} {:?} Condition {:?}", name, implied_conditions, condition);

                    appstate.enums.push(Enum {
                        name,
                        constants: Vec::new(),
                        condition,
                        implied_conditions: implied_conditions.clone(),
                        condition_expression: None,
                        implied_condition_expressions: ImpliedConditionExpressions {
                            arch: None,
                            bits: None,
                            platform: None,
                        },
                        is_error: false,
                    });

                    EntityVisitResult::Recurse
                }
                (EntityKind::EnumDecl, EntityKind::TypedefDecl) | (EntityKind::EnumDecl, EntityKind::VarDecl) => {
                    let mut e = appstate.enums.pop().expect("enumeration table entry should already exist");
                    let new_name = opt_parent_name;

                    info!("Renaming enum {:?} to {:?}", e.name, new_name);
                    e.name = new_name;
                    appstate.enums.push(e);

                    trace!("Continuing traversal without recursing into children");

                    EntityVisitResult::Continue
                },
                (EntityKind::EnumConstantDecl, EntityKind::EnumDecl) => {
                    let name = opt_cur_name.expect("enum constant has no name");
                    let line = cur.get_location().unwrap().get_file_location().line;

                    if name == "SEL4_FORCE_LONG_ENUM" {
                        // HACK: the parser misidentifies this macro call as the enum name since we didn't
                        // declare this macro, which works out good for us in this case
                        trace!("IGNORING enum value");

                        EntityVisitResult::Recurse
                    } else {
                        let mut e = appstate.enums.pop().expect("enum in table does not exist");
                        let condition = get_unique_condition(&e.condition, &get_condition_at_line(line, conditions));

                        let value = {
                            trace!("Parsing enum constant: '{}'", get_enum_constant_source_text(&cur));
                            match parse_enum_constant(&cur) {
                                Ok(x) => x,
                                Err(()) => {
                                    warn!("ENUM: Unable to parse enum value '{}' in enum {:?}, marking enum as error", get_enum_constant_source_text(&cur), e.name);
                                    e.is_error = true;
                                    None
                                },
                            }
                        };

                        debug!("Adding enum value {} = {:?} to enum {:?} Condition {:?}", name, value, e.name, condition);

                        e.constants.push(EnumConstant {
                            name,
                            value,
                            condition,
                            condition_expression: None,
                        });

                        appstate.enums.push(e);

                        EntityVisitResult::Recurse
                    }
                },
                // TYPEDEFS
                (EntityKind::TypedefDecl, EntityKind::TranslationUnit) => {
                    let name = opt_cur_name.expect("typedef has no name");
                    let line = cur.get_location().unwrap().get_file_location().line;
                    let type_ = cur.get_type().expect("typedef has no type").get_canonical_type();

                    match type_.get_kind() {
                        TypeKind::Enum | TypeKind::Record => {
                            trace!("Typedef is an Enum or a Record, recursing");

                            EntityVisitResult::Recurse
                        },
                        _ => {
                            let (type_, is_error) = {
                                trace!("Parsing typedef: '{}'", get_source_text(&cur));
                                match parse_typedef_type(&cur) {
                                    Ok(x) => (x, false),
                                    Err(()) => {
                                        warn!("TYPEDEF: Unable to parse typedef '{}', marking typedef as error", get_source_text(&cur));
                                        ("".to_string(), true)
                                    },
                                }
                            };
                            let condition = get_condition_at_line(line, conditions);

                            info!("Adding typedef {} = {}  {:?} Condition {:?}", name, type_, implied_conditions, condition);

                            appstate.types.push(Typedef {
                                name: name,
                                type_,
                                condition,
                                implied_conditions: implied_conditions.clone(),
                                condition_expression: None,
                                implied_condition_expressions: ImpliedConditionExpressions {
                                    arch: None,
                                    bits: None,
                                    platform: None,
                                },
                                is_error,
                            });

                            trace!("Continuing traversal without recursing into children");

                            EntityVisitResult::Continue
                        },
                    }
                },
                // STRUCTS
                // just like enums, structs are listed multiple times if they have a
                // typedef -- first with a parent of TranslationUnit, second
                // with a parent of a typedef.
                // Populate our struct table the first time around, then the second
                // time around pull in the typedef name.
                (EntityKind::StructDecl, EntityKind::TranslationUnit) => {
                    let name = opt_cur_name;
                    let line = cur.get_location().unwrap().get_file_location().line;
                    let condition = get_condition_at_line(line, conditions);

                    info!("Adding struct {:?} {:?} Condition {:?}", name, implied_conditions, condition);

                    appstate.structs.push(Struct {
                        name,
                        fields: Vec::new(),
                        is_packed: is_struct_packed(&cur),
                        condition,
                        implied_conditions: implied_conditions.clone(),
                        condition_expression: None,
                        implied_condition_expressions: ImpliedConditionExpressions {
                            arch: None,
                            bits: None,
                            platform: None,
                        },
                        is_error: false,
                    });

                    EntityVisitResult::Recurse
                }
                (EntityKind::StructDecl, EntityKind::TypedefDecl) => {
                    let mut e = appstate.structs.pop().expect("struct table entry should already exist");
                    let new_name = opt_parent_name;
                    // SEL4_PACKED can be set in the typdef too (and usually is)
                    let is_packed = is_struct_packed(&parent);

                    info!("Renaming struct {:?} to {:?}", e.name, new_name);
                    e.name = new_name;
                    if is_packed {
                        e.is_packed = true;
                    }
                    appstate.structs.push(e);

                    trace!("Continuing traversal without recursing into children");

                    EntityVisitResult::Continue
                },
                (EntityKind::FieldDecl, EntityKind::StructDecl) => {
                    let name = opt_cur_name.expect("field has no name");
                    let line = cur.get_location().unwrap().get_file_location().line;
                    let mut s = appstate.structs.pop().expect("struct in table does not exist");

                    trace!("Parsing struct field: '{}'", get_struct_field_source_text(&cur));

                    let type_ = match parse_struct_field(&cur) {
                        Ok(x) => x,
                        Err(_) => {
                            warn!("STRUCT: Unable to parse struct field '{}' in struct {:?}, marking struct as error", get_struct_field_source_text(&cur), s.name);
                            s.is_error = true;
                            // TODO: Make it an Option type
                            StructFieldType::Simple("BAD TYPE".to_string())
                        }
                    };

                    let condition = get_unique_condition(&s.condition, &get_condition_at_line(line, conditions));

                    debug!("Adding field {} type {:?} to struct {:?} Condition {:?}", name, type_, s.name, condition);

                    s.fields.push(StructField {
                        name: name,
                        type_,
                        condition,
                        condition_expression: None,
                    });

                    appstate.structs.push(s);

                    trace!("Continuing traversal without recursing into children");

                    EntityVisitResult::Continue
                },
                (_, EntityKind::StructDecl) => {
                    // We don't support things nested inside of structs yet
                    let mut s = appstate.structs.pop().expect("struct in table does not exist");

                    warn!("STRUCT: Nested {:?} inside struct {:?} not implemented, marking struct as error", cur.get_kind(), s.name);
                    s.is_error = true;
                    appstate.structs.push(s);

                    EntityVisitResult::Continue
                }
                // Unions (not supported right now)
                (EntityKind::UnionDecl, EntityKind::TranslationUnit) => {
                    panic!("Found a union named {:?}, but I don't know how to handle unions", opt_cur_name);
                },
                _ => {
                    trace!("Unhandled node type");

                    EntityVisitResult::Recurse
                }
            }
        } else {
            EntityVisitResult::Continue
        }
    });

    Ok(())
}

// HACK: Since we added random strings to identifiers in the preprocessor, we have to pull them out
// when we read stuff from clang
fn opt_remove_randomness<S>(opt_s: Option<S>) -> Option<String>
where
    S: ToString,
{
    if let Some(s) = opt_s {
        Some(remove_randomness(s))
    } else {
        None
    }
}

// HACK: Since we added random strings to identifiers in the preprocessor, we have to pull them out
// when we read stuff from clang
pub fn remove_randomness<S>(s: S) -> String
where
    S: ToString,
{
    let mut result = s.to_string();

    while let Some(pos) = result.find(RANDOM_IDENTIFIER_MARKER) {
        result.replace_range(
            pos..RANDOM_IDENTIFIER_MARKER.len() + RANDOM_IDENTIFIER_RANDOMNESS_LENGTH + 2,
            "",
        );
    }

    result
}

fn is_ignored_constant(constant: &str) -> bool {
    // some constants we just don't care about
    IGNORED_MACROS.contains(&constant)
        || constant.starts_with("__LIBSEL4")
        || (constant.starts_with("__") && constant.ends_with("_H"))
        || constant.ends_with("_TYPES_H")
}

fn get_condition_at_line(line: u32, conditions: &BTreeMap<u32, String>) -> Option<String> {
    let mut iter = conditions.range((Unbounded, Included(line)));
    match iter.next_back() {
        Some((_, s)) => {
            if s.is_empty() {
                None
            } else {
                Some(s.clone())
            }
        }
        _ => None,
    }
}

fn generate_condition_expressions(appstate: &mut AppState, lookups: &ImpliedConditionLookups) {
    for x in &mut appstate.constants {
        trace!("Parsing expression in '{:?}': '{:?}'", x.name, x.condition);
        x.implied_condition_expressions =
            get_expressions_from_implied_conditions(&x.implied_conditions, &lookups);
        x.condition_expression = get_expression_from_condition(&x.condition);
    }
    for x in &mut appstate.enums {
        trace!("Parsing expression in '{:?}': '{:?}'", x.name, x.condition);
        x.implied_condition_expressions =
            get_expressions_from_implied_conditions(&x.implied_conditions, &lookups);
        x.condition_expression = get_expression_from_condition(&x.condition);
        for x in &mut x.constants {
            trace!(
                "........Parsing expression in '{:?}': '{:?}'",
                x.name,
                x.condition
            );
            x.condition_expression = get_expression_from_condition(&x.condition);
        }
    }
    for x in &mut appstate.types {
        trace!("Parsing expression in '{:?}': '{:?}'", x.name, x.condition);
        x.implied_condition_expressions =
            get_expressions_from_implied_conditions(&x.implied_conditions, &lookups);
        x.condition_expression = get_expression_from_condition(&x.condition);
    }
    for x in &mut appstate.structs {
        trace!("Parsing expression in '{:?}': '{:?}'", x.name, x.condition);
        x.implied_condition_expressions =
            get_expressions_from_implied_conditions(&x.implied_conditions, &lookups);
        x.condition_expression = get_expression_from_condition(&x.condition);
        for x in &mut x.fields {
            trace!(
                "........Parsing expression in '{:?}': '{:?}'",
                x.name,
                x.condition
            );
            x.condition_expression = get_expression_from_condition(&x.condition);
        }
    }
    for x in &mut appstate.interfaces {
        for x in x {
            trace!("Parsing expressions in '{:?}'", x.name);
            x.implied_condition_expressions =
                get_expressions_from_implied_conditions(&x.implied_conditions, &lookups);
            for x in &mut x.methods {
                trace!(
                    "........Parsing expression in '{:?}': '{:?}'",
                    x.name,
                    x.condition
                );
                x.condition_expression = get_expression_from_condition(&x.condition);
            }
        }
    }
    for x in &mut appstate.syscalls {
        trace!("Parsing expression in '{:?}': '{:?}'", x.name, x.condition);
        x.condition_expression = get_expression_from_condition(&x.condition);
    }
    for x in &mut appstate.registers {
        let lookup = x
            .arch
            .trim_end_matches("_syscall")
            .trim_end_matches("_sysenter");
        trace!(
            "Looking up expression for arch '{}' in symbolic register '{}'",
            lookup,
            x.symbolic_register
        );
        let mut e = lookups.arch_to_condition[lookup].clone();
        if x.arch.ends_with("_syscall") {
            e = Expression::BinaryOperator {
                op: Operator::LogicalAnd,
                arg1: Box::new(e),
                arg2: Box::new(Expression::UnaryOperator {
                    op: Operator::Defined,
                    arg1: Box::new(Expression::Identifier(Identifier::Config(
                        "CONFIG_SYSCALL".to_string(),
                    ))),
                }),
            };
        } else if x.arch.ends_with("_sysenter") {
            e = Expression::BinaryOperator {
                op: Operator::LogicalAnd,
                arg1: Box::new(e),
                arg2: Box::new(Expression::UnaryOperator {
                    op: Operator::Defined,
                    arg1: Box::new(Expression::Identifier(Identifier::Config(
                        "CONFIG_SYSENTER".to_string(),
                    ))),
                }),
            };
        }
        x.arch_expression = Some(e);
    }
    for x in &mut appstate.tagged_unions {
        trace!("Parsing expression in '{:?}': '{:?}'", x.name, x.condition);
        x.implied_condition_expressions =
            get_expressions_from_implied_conditions(&x.implied_conditions, &lookups);
        x.condition_expression = get_expression_from_condition(&x.condition);
        for x in &mut x.fields {
            trace!(
                "........Parsing expression in '{:?}': '{:?}'",
                x.type_,
                x.condition
            );
            x.condition_expression = get_expression_from_condition(&x.condition);
        }
    }
    for x in &mut appstate.bitfield_structs {
        trace!("Parsing expression in '{:?}': '{:?}'", x.name, x.condition);
        x.implied_condition_expressions =
            get_expressions_from_implied_conditions(&x.implied_conditions, &lookups);
        x.condition_expression = get_expression_from_condition(&x.condition);
        for x in &mut x.fields {
            match x {
                BitfieldStructField::Field {
                    name,
                    condition,
                    bits,
                    ..
                } => {
                    trace!(
                        "........Parsing expression in '{:?}': '{:?}'",
                        name,
                        condition
                    );
                    *x = BitfieldStructField::Field {
                        name: name.clone(),
                        bits: *bits,
                        condition: condition.clone(),
                        condition_expression: get_expression_from_condition(&condition),
                    };
                }
                BitfieldStructField::Padding {
                    condition, bits, ..
                } => {
                    trace!("........Parsing expression in 'Padding': '{:?}'", condition);
                    *x = BitfieldStructField::Padding {
                        bits: *bits,
                        condition: condition.clone(),
                        condition_expression: get_expression_from_condition(&condition),
                    };
                }
            }
        }
    }
}

fn get_expressions_from_implied_conditions(
    ic: &ImpliedConditions,
    lookups: &ImpliedConditionLookups,
) -> ImpliedConditionExpressions {
    let arch = if let Some(x) = &ic.arch {
        Some(lookups.arch_to_condition[x].clone())
    } else {
        None
    };
    let bits = if let Some(x) = &ic.bits {
        Some(lookups.bits_to_condition[x].clone())
    } else {
        None
    };
    let platform = if let Some(x) = &ic.platform {
        Some(lookups.platform_to_condition[x].clone())
    } else {
        None
    };
    ImpliedConditionExpressions {
        arch,
        bits,
        platform,
    }
}

fn get_expression_from_condition(opt_c: &Option<String>) -> Option<Expression> {
    if let Some(c) = opt_c {
        let mut e = string_to_expression(c).expect("Unable to parse condition");
        e.resolve_identifiers(|i| {
            if i.starts_with("CONFIG_") {
                Identifier::Config(i.to_string())
            } else {
                panic!(
                    "Attempted to resolve unexpected identifier '{}' in condition expression '{}'",
                    i, c
                );
            }
        });
        Some(e)
    } else {
        None
    }
}

// TODO: This operation is very slow and adds a significant amount of runtime. We should cache the
// results so the same strings don't get evaluated multiple times.
fn string_to_expression(s: &str) -> io::Result<Expression> {
    // It seems the only way to get libclang to tokenize for us is to generate a file :(
    let mut f = NamedTempFile::new()?;
    f.write_all(s.as_bytes())?;
    f.flush()?;

    let clang = Clang::new().unwrap();

    let index = Index::new(
        &clang, true,  // exclude declarations from precompiled headers?
        false, // print diagnostics?
    );

    let mut parser = index.parser(f.path());
    parser
        .arguments(&["-xc", "-std=gnu11", "-nostdinc"])
        .incomplete(true)
        .keep_going(true)
        .skip_function_bodies(true);
    let tu = parser
        .parse()
        .expect("Unable to parse condition expression");
    let root_entity = tu.get_entity();

    Ok(parse_expression_from_entity(&root_entity).unwrap())
}

fn get_unique_condition(parent: &Option<String>, child: &Option<String>) -> Option<String> {
    // if child starts with parent, only return the unique part of child
    if let (Some(parent), Some(child)) = (parent, child) {
        if child.starts_with(parent) {
            let result = child.trim_start_matches(parent).trim_start_matches(" && ");
            if !result.is_empty() {
                Some(result.to_owned())
            } else {
                None
            }
        } else {
            None
        }
    } else {
        child.clone()
    }
}

// TODO: We should pull in and re-export documentation
fn parse_api_xml_file<F>(
    f: F,
    appstate: &mut AppState,
    implied_conditions: &ImpliedConditions,
) -> io::Result<()>
where
    F: AsRef<Path>,
{
    trace!("Parsing API XML file");

    trace!("Reading file into memory");
    let text = std::fs::read_to_string(f)?;

    trace!("Parsing xml file");
    let doc = roxmltree::Document::parse(&text).expect("Can't parse api xml file");

    let mut cur_type = InterfaceType::Generic;

    trace!("Visiting each XML node");

    for node in doc.root().descendants() {
        trace!("XML Node: {:?}", node);
        let tag_name = node.tag_name().name();
        match tag_name {
            "api" => {} // root node
            "interface" => {
                let name = node
                    .attribute("name")
                    .expect("no attribute 'name' in 'interface'");

                if let Some(_) = node.attribute("condition") {
                    panic!("XML API: Unexpected condition found on interface {}", name);
                }

                debug!("Adding API interface {} {:?}", name, implied_conditions);

                cur_type = get_interface_type_from_implied_conditions(&implied_conditions);

                appstate.interfaces[cur_type as usize].push(Interface {
                    name: name.to_string(),
                    implied_conditions: implied_conditions.clone(),
                    implied_condition_expressions: ImpliedConditionExpressions {
                        arch: None,
                        bits: None,
                        platform: None,
                    },
                    methods: Vec::new(),
                });
            }
            "method" => {
                let name = node
                    .attribute("name")
                    .expect("no attribute 'name' in 'method'");
                let condition = node.attribute("condition");
                let mut i = appstate.interfaces[cur_type as usize]
                    .pop()
                    .expect("interface table entry should already exist");

                info!(
                    "Adding API method {} on interface {} Condition {:?}",
                    name, i.name, condition
                );

                i.methods.push(Method {
                    is_error: false,
                    name: name.to_string(),
                    condition: if let Some(c) = condition {
                        Some(c.to_string())
                    } else {
                        None
                    },
                    condition_expression: None,
                    invocation_label: None,
                    arguments: Vec::new(),
                    return_values: Vec::new(),
                });

                appstate.interfaces[cur_type as usize].push(i);
            }
            "param" => {
                let name = node
                    .attribute("name")
                    .expect("no attribute 'name' in 'param'");
                let dir = node
                    .attribute("dir")
                    .expect("no attribute 'dir' in 'param'");
                let type_ = node
                    .attribute("type")
                    .expect("no attribute 'type' in 'param'");

                let mut i = appstate.interfaces[cur_type as usize]
                    .pop()
                    .expect("interface table entry should already exist");
                let mut m = i
                    .methods
                    .pop()
                    .expect("method table entry should already exist");

                if let Some(_) = node.attribute("condition") {
                    panic!("XML API: Unexpected condition found on param");
                }

                match dir {
                    "in" => {
                        debug!(
                            "Adding API argument {} to method {} on interface {}",
                            name, m.name, i.name
                        );

                        m.arguments.push(MethodArgument {
                            name: name.to_string(),
                            type_: type_.to_string(),
                            is_cap: None,
                        });
                    }
                    "out" => {
                        debug!(
                            "Adding API return value {} to method {} on interface {}",
                            name, m.name, i.name
                        );

                        m.return_values.push(MethodArgument {
                            name: name.to_string(),
                            type_: type_.to_string(),
                            is_cap: None,
                        });
                    }
                    _ => {
                        panic!(
                            "Unexpected direction '{}' on param {} in method {} of interface {}",
                            dir, name, m.name, i.name
                        );
                    }
                }

                i.methods.push(m);
                appstate.interfaces[cur_type as usize].push(i);
            }
            // TODO: Implement struct parsing from here.
            // Currently, these are just duplicates with stuff that's already in the C files, and
            // they don't even contain type information, so we just ignore them.
            "struct" | "member" => {}
            // TODO: Pull in documentation
            "brief" | "description" | "texttt" | "docref" | "autoref" | "return" | "cap_param"
            | "shortref" | "obj" | "errorenumdesc" => {}
            _ => {
                if tag_name.is_empty() {
                    trace!("Unhandled node");
                } else {
                    panic!("Unexpected XML Node '{}' found in api xml", tag_name);
                }
            }
        }
    }

    Ok(())
}

fn get_interface_type_from_implied_conditions(ic: &ImpliedConditions) -> InterfaceType {
    if ic.arch == None {
        InterfaceType::Generic
    } else {
        match ic.arch.as_ref().unwrap().as_ref() {
            "x86" | "riscv" | "arm" => InterfaceType::ArchType,
            _ => InterfaceType::Arch,
        }
    }
}

// TODO: We should pull in and re-export documentation
fn parse_syscall_xml_file<F>(f: F, appstate: &mut AppState) -> io::Result<()>
where
    F: AsRef<Path>,
{
    trace!("Parsing Syscall XML file");

    trace!("Reading file into memory");
    let text = std::fs::read_to_string(f)?;

    trace!("Parsing xml file");
    let doc = roxmltree::Document::parse(&text).expect("Can't parse syscall xml file");

    let mut group = "";
    let mut condition = None;

    trace!("Visiting each XML node");

    for node in doc.root().descendants() {
        trace!("XML Node: {:?}", node);
        let tag_name = node.tag_name().name();
        match tag_name {
            "syscalls" => {} // root node
            "api" => {
                trace!("Processing '{}'", tag_name);
                group = tag_name;
                condition = None;
                trace!("Configuration condition set to: {:?}", condition);
            }
            "api-master" => {
                trace!("Processing '{}'", tag_name);
                group = tag_name;
                condition = Some("!defined(CONFIG_KERNEL_MCS)".to_string());
                trace!("Configuration condition set to: {:?}", condition);
            }
            "api-mcs" => {
                trace!("Processing '{}'", tag_name);
                group = tag_name;
                condition = Some("defined(CONFIG_KERNEL_MCS)".to_string());
                trace!("Configuration condition set to: {:?}", condition);
            }
            "debug" => {
                trace!("Processing '{}'", tag_name);
                group = tag_name;
                condition = None;
                trace!("Configuration condition set to: {:?}", condition);
            }
            "config" => {
                if let Some(c) = node.attribute("condition") {
                    trace!("Configuration condition set to: {}", c);
                    condition = Some(c.to_string());
                }
            }
            "syscall" => {
                let name = node
                    .attribute("name")
                    .expect("no attribute 'name' in 'syscall'");

                info!(
                    "Adding syscall {} condition {:?} to group {}",
                    name, condition, group
                );

                appstate.syscalls.push(Syscall {
                    is_error: false,
                    group: group.to_string(),
                    name: name.to_string(),
                    condition: condition.clone(),
                    condition_expression: None,
                    syscall_number: None,
                    does_send_ipcbuf: false,
                    does_receive_ipcbuf: false,
                    arguments: Vec::new(),
                    return_values: Vec::new(),
                });
            }
            _ => {
                if tag_name.is_empty() {
                    trace!("Unhandled node");
                } else {
                    panic!("Unexpected XML Node '{}' found in syscall.xml", tag_name);
                }
            }
        }
    }

    Ok(())
}

fn parse_bf_file<F>(
    f: F,
    appstate: &mut AppState,
    implied_conditions: &ImpliedConditions,
    conditions: &BTreeMap<u32, String>,
) -> io::Result<()>
where
    F: AsRef<Path>,
{
    // A custom file format :(
    // Let's just wing it and hope for the best

    trace!("Opening bf file");
    let infile = File::open(f)?;

    trace!("Creating reader for bf file");
    let reader = BufReader::new(infile);

    trace!("Processing each line in bf file");

    for (i, line) in reader.lines().enumerate() {
        let i = i + 1;
        let orig_line = line?;
        trace!("Line: {}", orig_line);
        let line = orig_line.trim();

        // We don't need the required defines
        if REQUIRED_DEFINES.len() >= i {
            trace!("Manually added line, ignoring");
            continue;
        }

        if line.is_empty() {
            trace!("Empty line, ignoring");
            continue;
        }

        if line.starts_with("--") {
            trace!("Comment line, ignoring");
            continue;
        }

        if line.starts_with("}") {
            trace!("ending brace, ignoring");
            continue;
        }

        let split: Vec<&str> = line.split_whitespace().collect();

        if split.len() < 1 {
            panic!("unexpected empty line in bf file");
        }

        match split[0] {
            "tagged_union" => {
                if split.len() != 4 || split[3] != "{" {
                    panic!("Unexpected format of '{}' line in bf file", split[0]);
                }

                let name = split[1];
                let tag_name = split[2];
                let condition = get_condition_at_line(i as u32, conditions);

                info!(
                    "Adding tagged union '{}' {:?} Condition {:?}",
                    name, implied_conditions, condition
                );

                appstate.tagged_unions.push(TaggedUnion {
                    is_error: false,
                    name: name.to_string(),
                    condition,
                    implied_conditions: implied_conditions.clone(),
                    condition_expression: None,
                    implied_condition_expressions: ImpliedConditionExpressions {
                        arch: None,
                        bits: None,
                        platform: None,
                    },
                    tag_name: tag_name.to_string(),
                    fields: Vec::new(),
                });
            }
            "tag" => {
                if split.len() != 3 {
                    panic!("Unexpected format of '{}' line in bf file", split[0]);
                }

                let type_ = split[1];
                let constant = split[2]
                    .parse::<u32>()
                    .expect("Cannot convert tag constant to number in bf file");

                let mut tu = appstate
                    .tagged_unions
                    .pop()
                    .expect("Expected existing entry in tagged unions table");

                let condition = get_unique_condition(
                    &tu.condition,
                    &get_condition_at_line(i as u32, conditions),
                );

                debug!(
                    "Adding tag '{}' = '{}' to tagged union '{}' Condition {:?}",
                    type_, constant, tu.name, condition
                );

                tu.fields.push(TaggedUnionField {
                    condition,
                    condition_expression: None,
                    tag_constant: constant,
                    type_: type_.to_string(),
                });

                appstate.tagged_unions.push(tu);
            }
            "block" => {
                if split.len() != 3 || split[2] != "{" {
                    panic!("Unexpected format of '{}' line in bf file", split[0]);
                }

                let mut name = split[1].to_string();
                if name.starts_with("seL4_") {
                    name.push_str("_t");
                }
                let condition = get_condition_at_line(i as u32, conditions);

                info!(
                    "Adding bitfield struct '{}' {:?} Condition {:?}",
                    name, implied_conditions, condition
                );

                appstate.bitfield_structs.push(BitfieldStruct {
                    is_error: false,
                    name: name,
                    condition,
                    implied_conditions: implied_conditions.clone(),
                    condition_expression: None,
                    implied_condition_expressions: ImpliedConditionExpressions {
                        arch: None,
                        bits: None,
                        platform: None,
                    },
                    fields: Vec::new(),
                });
            }
            "field" => {
                if split.len() != 3 {
                    panic!("Unexpected format of '{}' line in bf file", split[0]);
                }

                let name = split[1];
                let bits = split[2]
                    .parse::<u32>()
                    .expect("Cannot convert bitfield field size to number in bf file");
                let mut bs = appstate
                    .bitfield_structs
                    .pop()
                    .expect("Expected existing entry in bitfield structs table");
                let condition = get_unique_condition(
                    &bs.condition,
                    &get_condition_at_line(i as u32, conditions),
                );

                debug!(
                    "Adding bitfield '{}' size '{}' to bitfield struct '{}' Condition {:?}",
                    name, bits, bs.name, condition
                );

                bs.fields.push(BitfieldStructField::Field {
                    name: name.to_string(),
                    condition,
                    condition_expression: None,
                    bits,
                });

                appstate.bitfield_structs.push(bs);
            }
            "padding" => {
                if split.len() != 2 {
                    panic!("Unexpected format of '{}' line in bf file", split[0]);
                }

                let bits = split[1]
                    .parse::<u32>()
                    .expect("Cannot convert bitfield padding size to number in bf file");
                let mut bs = appstate
                    .bitfield_structs
                    .pop()
                    .expect("Expected existing entry in bitfield structs table");
                let condition = get_unique_condition(
                    &bs.condition,
                    &get_condition_at_line(i as u32, conditions),
                );

                debug!(
                    "Adding bitfield padding size '{}' to bitfield struct '{}' Condition {:?}",
                    bits, bs.name, condition
                );

                bs.fields.push(BitfieldStructField::Padding {
                    condition,
                    condition_expression: None,
                    bits,
                });

                appstate.bitfield_structs.push(bs);
            }
            "base" => {} // seems like the base is always the arch word size
            _ => {
                panic!("Unknown directive '{}' in bf file", split[0]);
            }
        }
    }

    Ok(())
}

fn parse_cmake_file<F>(f: F, lookups: &mut ImpliedConditionLookups) -> io::Result<()>
where
    F: AsRef<Path>,
{
    trace!("Processing each line in cmake file");

    for line in fs::read_to_string(f)?.lines().map(|x| x.trim()) {
        trace!("Line: {}", line);

        if line.starts_with("declare_platform") {
            trace!("Processing declare_platform line");

            let split: Vec<&str> = line
                .trim_start_matches("declare_platform(")
                .trim_end_matches(")")
                .split_whitespace()
                .collect();

            trace!("line split: {:?}", split);

            if split.len() < 4
                || !split[1].starts_with("KernelPlat")
                || !split[2].starts_with("PLAT_")
            {
                panic!("unexpected format of declare_platform line in cmake file");
            }

            let platform = split[0];

            let mut config = "defined(CONFIG_".to_string();
            config.push_str(split[2]);
            config.push(')');

            debug!(
                "Setting configuration constant for platform '{}' = '{}'",
                platform, config
            );

            lookups.platform_to_condition.insert(
                platform.to_string(),
                get_expression_from_condition(&Some(config)).unwrap(),
            );
        } else if line.contains('\"') && line.contains(';') {
            let split: Vec<&str> = line.trim_matches('"').split(';').collect();

            trace!("line split: {:?}", split);

            if split.len() >= 3
                && split[1].contains("Arch")
                && !split[1].contains("MicroArch")
                && split[2].starts_with("ARCH_")
            {
                let arch = split[0];
                let mut config = "defined(CONFIG_".to_string();
                config.push_str(split[2]);
                config.push(')');

                debug!(
                    "Setting configuration constant for arch '{}' = '{}'",
                    arch, config
                );

                lookups.arch_to_condition.insert(
                    arch.to_string(),
                    get_expression_from_condition(&Some(config)).unwrap(),
                );
            } else if split.len() >= 4 && split[1].contains("Plat") && split[2].starts_with("PLAT_")
            {
                let platform = split[0];

                let mut config = "defined(CONFIG_".to_string();
                config.push_str(split[2]);
                config.push(')');

                debug!(
                    "Setting configuration constant for platform '{}' = '{}'",
                    platform, config
                );

                lookups.platform_to_condition.insert(
                    platform.to_string(),
                    get_expression_from_condition(&Some(config)).unwrap(),
                );
            }
        }
    }

    Ok(())
}

fn parse_user_generated_machine_registers_file<F>(f: F, appstate: &mut AppState) -> io::Result<()>
where
    F: AsRef<Path>,
{
    trace!("Parsing user-generated machine registers CSV file");

    trace!("Reading file into memory");
    let mut reader = csv::Reader::from_path(f)?;

    trace!("Reading header row");
    let header = reader
        .headers()
        .expect("No header row in machine register csv")
        .clone();

    if header.len() < 2 {
        panic!("Not enough fields in header row of machine register csv");
    }

    trace!("Headers: {:?}", header);

    trace!("Reading item rows");

    for (line, record) in reader.records().enumerate() {
        let line = line + 2; // move to 1-based and skip header
        let record = record.expect("Bad record in machine register csv");

        trace!("Row {}: {:?}", line, record);

        if record.len() != header.len() {
            panic!("Number of fields '{}' on line {} does not match header row '{}' in machine register csv", line, record.len(), header.len());
        }

        let symbolic_name = record.get(0).unwrap();

        if symbolic_name.is_empty() {
            panic!(
                "field at row {} col 1 in machine register csv cannot be empty",
                line
            );
        }

        info!(
            "Adding machine register definition for symbolic register '{}'",
            symbolic_name
        );

        for (col, machine_register) in record.iter().skip(1).enumerate() {
            let col = col + 1; // move to 1-based
            let arch = header.get(col).unwrap();

            if machine_register.is_empty() {
                panic!(
                    "field at row {} col {} is machine register csv cannot be empty",
                    line, col
                );
            }

            debug!(
                "Setting machine register for '{}' on arch '{}' to register '{}'",
                symbolic_name, arch, machine_register
            );

            appstate.registers.push(SyscallRegister {
                symbolic_register: symbolic_name.to_string(),
                machine_register: machine_register.to_string(),
                arch: arch.to_string(),
                arch_expression: None,
            });
        }
    }

    Ok(())
}

fn parse_user_generated_syscall_file<F>(f: F, appstate: &mut AppState) -> io::Result<()>
where
    F: AsRef<Path>,
{
    trace!("Parsing user-generated syscalls CSV file");

    trace!("Reading file into memory");
    let mut reader = csv::Reader::from_path(f)?;

    trace!("Reading item rows");

    for (line, record) in reader.records().enumerate() {
        let line = line + 2; // move to 1-based and skip header
        let record = record.expect("Bad record in syscall csv");

        trace!("Row {}: {:?}", line, record);

        let expected_len = 7;
        if record.len() != expected_len {
            panic!(
                "Number of fields '{}' on line {} does not match expected '{}' in syscall csv",
                line,
                record.len(),
                expected_len
            );
        }

        let group = read_syscall_field(&record, line, 0, false);
        let syscall_name = read_syscall_field(&record, line, 1, false);
        let direction = read_syscall_field(&record, line, 2, false);
        let argument_name = read_syscall_field(&record, line, 3, false);
        let argument_type = read_syscall_field(&record, line, 4, false);
        let symbolic_register = read_syscall_field(&record, line, 5, false);
        let special_value = read_syscall_field(&record, line, 6, true);

        let entry_index = {
            if let Some((i, _)) = appstate
                .syscalls
                .iter()
                .enumerate()
                .filter(|x| x.1.group == group && x.1.name == syscall_name)
                .nth(0)
            {
                i
            } else {
                panic!(
                    "System call name '{}' group '{}' in row {} of user-defined syscall csv file does not exist in seL4 syscall xml file",
                    syscall_name,
                    group,
                    line
                );
            }
        };

        match direction {
            "in" => {
                debug!(
                    "Adding syscall '{}' argument '{}' type '{}' symbolic register '{}' special_value '{}'",
                    appstate.syscalls[entry_index].name,
                    argument_name,
                    argument_type,
                    symbolic_register,
                    special_value
                );

                if special_value == "ipcbuf" {
                    trace!("....... setting does_send_ipcbuf = true");
                    appstate.syscalls[entry_index].does_send_ipcbuf = true;
                } else {
                    appstate.syscalls[entry_index]
                        .arguments
                        .push(SyscallArgument {
                            name: argument_name.to_string(),
                            type_: argument_type.to_string(),
                            symbolic_register: symbolic_register.to_string(),
                            special_value: if special_value.is_empty() {
                                None
                            } else {
                                Some(special_value.to_string())
                            },
                        });
                }
            }
            "out" => {
                if !special_value.is_empty() && special_value != "ipcbuf" {
                    panic!("special_value cannot be set on an entry with direction 'out' on row {} of syscall csv file", line);
                }

                debug!(
                    "Adding syscall '{}' return value '{}' type '{}' symbolic register '{}'",
                    appstate.syscalls[entry_index].name,
                    argument_name,
                    argument_type,
                    symbolic_register
                );

                if special_value == "ipcbuf" {
                    trace!("....... setting does_receive_ipcbuf = true");
                    appstate.syscalls[entry_index].does_receive_ipcbuf = true;
                } else {
                    appstate.syscalls[entry_index]
                        .return_values
                        .push(SyscallArgument {
                            name: argument_name.to_string(),
                            type_: argument_type.to_string(),
                            symbolic_register: symbolic_register.to_string(),
                            special_value: None,
                        });
                }
            }
            _ => panic!(
                "Unknown direction '{}' on line {} in syscall csv",
                direction, line
            ),
        }
    }

    Ok(())
}

fn read_syscall_field(
    record: &csv::StringRecord,
    row: usize,
    col: usize,
    allow_empty: bool,
) -> &str {
    let field = record.get(col).unwrap();
    if !allow_empty && field.is_empty() {
        panic!(
            "field at row {} col {} in syscall csv cannot be empty",
            row,
            col + 1
        );
    }
    field
}

fn generate_bits_to_condition(lookups: &mut ImpliedConditionLookups) {
    lookups.bits_to_condition.insert(
        Bits::Bits32,
        get_expression_from_condition(&Some("CONFIG_WORD_SIZE == 32".to_string())).unwrap(),
    );
    lookups.bits_to_condition.insert(
        Bits::Bits64,
        get_expression_from_condition(&Some("CONFIG_WORD_SIZE == 64".to_string())).unwrap(),
    );
}

fn give_unnamed_enums_a_name(appstate: &mut AppState) {
    let mut num = 1;
    for e in &mut appstate.enums {
        if e.name == None {
            e.name = Some(format!("seL4_IDLGEN_GeneratedEnumName{}", num));
            num += 1;
        }
    }
}

fn generate_name_lookup_table(appstate: &AppState) -> BTreeMap<String, Vec<NameLookup>> {
    let mut result = BTreeMap::new();

    // Enum constants should be added before constants
    for (i, x) in appstate.enums.iter().filter(|x| x.name != None).enumerate() {
        let name = x.name.clone().unwrap();
        create_lookup_table_entry(&name, NameLookup::Enum(i), &mut result);

        for (ci, x) in x.constants.iter().enumerate() {
            create_lookup_table_entry(
                &x.name,
                NameLookup::EnumConstant {
                    enum_index: i,
                    enum_name: name.clone(),
                    enum_constant_index: ci,
                },
                &mut result,
            );
        }
    }
    for (i, x) in appstate.constants.iter().enumerate() {
        create_lookup_table_entry(&x.name, NameLookup::Constant(i), &mut result);
    }
    for (i, x) in appstate.types.iter().enumerate() {
        create_lookup_table_entry(&x.name, NameLookup::Type(i), &mut result);
    }
    for (i, x) in appstate
        .structs
        .iter()
        .filter(|x| x.name != None)
        .enumerate()
    {
        let name = x.name.clone().unwrap();
        create_lookup_table_entry(&name, NameLookup::Struct(i), &mut result);
    }
    for (i, x) in appstate.bitfield_structs.iter().enumerate() {
        create_lookup_table_entry(&x.name, NameLookup::BitfieldStruct(i), &mut result);
    }
    for (i, x) in appstate.tagged_unions.iter().enumerate() {
        create_lookup_table_entry(&x.name, NameLookup::TaggedUnion(i), &mut result);
    }
    for (interface_root_index, x) in appstate.interfaces.iter().enumerate() {
        for (interface_index, x) in x.iter().enumerate() {
            for (method_index, x) in x.methods.iter().enumerate() {
                // TODO: The name should include the interface name and also some prefix to give
                // interface methods their own namespace
                create_lookup_table_entry(
                    &x.name,
                    NameLookup::InterfaceMethod {
                        interface_root_index,
                        interface_index,
                        method_index,
                    },
                    &mut result,
                );
            }
        }
    }
    for (i, x) in appstate.syscalls.iter().enumerate() {
        // TODO: The lookup key should include some prefix to give syscalls their own namespace
        create_lookup_table_entry(&x.name, NameLookup::Syscall(i), &mut result);
    }

    result
}

fn create_lookup_table_entry(
    key: &String,
    value: NameLookup,
    name_lookup: &mut BTreeMap<String, Vec<NameLookup>>,
) {
    if !name_lookup.contains_key(key) {
        name_lookup.insert(key.clone(), Vec::new());
    }
    let vec = name_lookup.get_mut(key).unwrap();
    vec.push(value);
}

fn validate_num_entries_in_name_lookup(
    appstate: &AppState,
    name_lookup: &BTreeMap<String, Vec<NameLookup>>,
) {
    let num_entries_appstate = {
        let mut x = 0;
        x += appstate.constants.len();
        x += appstate.enums.len();
        for ec in &appstate.enums {
            x += ec.constants.len();
        }
        x += appstate.types.len();
        x += appstate.structs.len();
        x += appstate.bitfield_structs.len();
        x += appstate.tagged_unions.len();
        for ifr in &appstate.interfaces {
            for iface in ifr.iter() {
                x += iface.methods.len();
            }
        }
        x += appstate.syscalls.len();
        x
    };

    let num_entries_name_lookup = {
        let mut x = 0;
        for (_, v) in name_lookup {
            x += v.len();
        }
        x
    };

    if num_entries_appstate != num_entries_name_lookup {
        panic!("Number of entries in appstate ({}) does not match number of entries in name_lookup ({})", num_entries_appstate, num_entries_name_lookup);
    }
}

// TODO: BUG: Some references can be resolved to both an enum constant and a constant,
// and we don't handle that correctly here, not sure what the best way to deal with this is.
fn resolve_identifier_references_in_expressions(
    appstate: &mut AppState,
    name_lookup: &BTreeMap<String, Vec<NameLookup>>,
) {
    for x in &mut appstate.constants {
        let name = &x.name;
        x.value.resolve_identifiers(|s| {
            resolve_identifier("constant", name, s.to_string(), name_lookup)
        });
        if x.value.has_unresolved_references() {
            x.is_error = true;
        }
    }

    for e in &mut appstate.enums {
        for c in &mut e.constants {
            let mut name = if let Some(name) = e.name.as_mut() {
                let mut new = name.clone();
                new.push_str("::");
                new
            } else {
                String::new()
            };
            name.push_str(&c.name);
            if let Some(value) = c.value.as_mut() {
                value.resolve_identifiers(|s| {
                    resolve_identifier("enum", &name, s.to_string(), name_lookup)
                });
                if value.has_unresolved_references() {
                    e.is_error = true;
                }
            }
        }
    }

    for s in &mut appstate.structs {
        for f in &mut s.fields {
            if let StructFieldType::Array(_, value) = &mut f.type_ {
                let mut name = if let Some(name) = s.name.as_mut() {
                    let mut new = name.clone();
                    new.push_str("::");
                    new
                } else {
                    String::new()
                };
                name.push_str(&f.name);
                value.resolve_identifiers(|s| {
                    resolve_identifier("struct", &name, s.to_string(), name_lookup)
                });
                if value.has_unresolved_references() {
                    s.is_error = true;
                }
            }
        }
    }
}

fn resolve_identifier(
    container_type: &str,
    container_name: &str,
    identifier: String,
    name_lookup: &BTreeMap<String, Vec<NameLookup>>,
) -> Identifier {
    trace!(
        "Resolving identifier '{}' contained in {} '{}'",
        identifier,
        container_type,
        container_name
    );

    if identifier.starts_with("CONFIG_") {
        Identifier::Config(identifier)
    } else {
        if let Some(vec) = name_lookup.get(&identifier) {
            let result = match &vec[0] {
                NameLookup::Constant(_) => Identifier::Constant(identifier),
                NameLookup::Type(_) => Identifier::Type(identifier),
                NameLookup::EnumConstant { enum_name, .. } => Identifier::EnumConstant {
                    constant_name: identifier,
                    enum_name: enum_name.clone(),
                },
                x @ _ => {
                    warn!(
                        "Reference to '{}' in {} '{}' is of unsupported type {:?}, {} will be marked as error",
                        identifier,
                        container_type,
                        container_name,
                        x,
                        container_type,
                    );
                    Identifier::Unresolved(identifier)
                }
            };

            trace!("^  Resolved to: {:?}", result);
            result
        } else {
            warn!(
                "Unknown reference to '{}' in {} '{}', {} will be marked as error",
                identifier, container_type, container_name, container_type
            );
            Identifier::Unresolved(identifier)
        }
    }
}

fn generate_syscall_enum(appstate: &mut AppState) {
    // if the kernel supports a configuration option to enable mcs, there is a group
    // "api-master" for non-mcs syscalls and "api-mcs" for mcs-syscalls. Unfortunately these
    // are at the beginning of the number range for syscalls so the constants for later
    // syscalls depend on whether mcs is enabled or not. To deal with this we generate two
    // enums with the same name that will get used depending on the mcs configuration.
    // if the kernel doesn't support a configuration option to enable mcs, there is only one
    // group "api" and we can shove everything in the same enum.

    let mut e_master = Enum {
        is_error: false,
        name: Some(GENERATED_SYSCALL_ENUM_NAME.to_string()),
        condition: Some("!defined(CONFIG_KERNEL_MCS)".to_string()),
        condition_expression: None, // will be resolved later
        implied_conditions: ImpliedConditions {
            arch: None,
            platform: None,
            bits: None,
        },
        implied_condition_expressions: ImpliedConditionExpressions {
            arch: None,
            platform: None,
            bits: None,
        },
        constants: Vec::new(),
    };

    let mut e_mcs = Enum {
        is_error: false,
        name: Some(GENERATED_SYSCALL_ENUM_NAME.to_string()),
        condition: Some("defined(CONFIG_KERNEL_MCS)".to_string()),
        condition_expression: None, // will be resolved later
        implied_conditions: ImpliedConditions {
            arch: None,
            platform: None,
            bits: None,
        },
        implied_condition_expressions: ImpliedConditionExpressions {
            arch: None,
            platform: None,
            bits: None,
        },
        constants: Vec::new(),
    };

    let mut sysno_master = 1;
    let mut sysno_mcs = 1;

    for x in &mut appstate.syscalls {
        let mut ec_name = x.name.clone();
        ec_name.push_str("_SyscallNumber");

        match x.group.as_ref() {
            "api" | "api-master" => {
                e_master.constants.push(EnumConstant {
                    name: ec_name.clone(),
                    condition: x.condition.clone(),
                    condition_expression: None, // will be resolved later
                    value: Some(Expression::UnaryOperator {
                        op: Operator::UnaryMinus,
                        arg1: Box::new(Expression::NumericLiteral(sysno_master)),
                    }),
                });
                sysno_master = sysno_master + 1;
            }
            "api-mcs" => {
                e_mcs.constants.push(EnumConstant {
                    name: ec_name.clone(),
                    condition: x.condition.clone(),
                    condition_expression: None, // will be resolved later
                    value: Some(Expression::UnaryOperator {
                        op: Operator::UnaryMinus,
                        arg1: Box::new(Expression::NumericLiteral(sysno_mcs)),
                    }),
                });
                sysno_mcs = sysno_mcs + 1;
            }
            _ => {
                e_master.constants.push(EnumConstant {
                    name: ec_name.clone(),
                    condition: x.condition.clone(),
                    condition_expression: None, // will be resolved later
                    value: Some(Expression::UnaryOperator {
                        op: Operator::UnaryMinus,
                        arg1: Box::new(Expression::NumericLiteral(sysno_master)),
                    }),
                });
                sysno_master = sysno_master + 1;
                if !e_mcs.constants.is_empty() {
                    e_mcs.constants.push(EnumConstant {
                        name: ec_name.clone(),
                        condition: x.condition.clone(),
                        condition_expression: None, // will be resolved later
                        value: Some(Expression::UnaryOperator {
                            op: Operator::UnaryMinus,
                            arg1: Box::new(Expression::NumericLiteral(sysno_mcs)),
                        }),
                    });
                    sysno_mcs = sysno_mcs + 1;
                }
            }
        };

        x.syscall_number = Some(Expression::Identifier(Identifier::EnumConstant {
            enum_name: GENERATED_SYSCALL_ENUM_NAME.to_string(),
            constant_name: ec_name.clone(),
        }));
    }

    if e_mcs.constants.is_empty() {
        // if kernel doesn't have a config option to enable mcs then we don't need a condition on
        // our generated enum
        e_master.condition = None;
        appstate.enums.push(e_master);
    } else {
        appstate.enums.push(e_master);
        appstate.enums.push(e_mcs);
    }
}

fn generate_invocation_enum(appstate: &mut AppState) {
    let e_template = Enum {
        is_error: false,
        name: None,
        condition: None,
        condition_expression: None,
        implied_conditions: ImpliedConditions {
            arch: None,
            platform: None,
            bits: None,
        },
        implied_condition_expressions: ImpliedConditionExpressions {
            arch: None,
            platform: None,
            bits: None,
        },
        constants: Vec::new(),
    };

    let mut enums = BTreeMap::new();
    let mut i = 0;
    let numentries_name = "NumEntries";

    // We'll end up generating three enums, where the second and third enums will have multiple
    // definitions conditioned on the architecture:
    // 1) A generic enum that contains invocations that are valid for all arches
    // 2) An architecture-specific enum (e.g. x86_64, aarch64, riscv64) whose numbering starts at
    //    the end of the generic enum
    // 3) An architecture-type-specific enum (e.g. x86, arm, risc-v) whose numbering starts at the
    //    end of the arch-specific enum

    for x in &mut appstate.interfaces {
        for iface in x {
            let (enum_name, parent_name) = {
                match i {
                    // N.B. These must match InterfaceType enum
                    0 => {
                        let mut enum_name = GENERATED_INVOCATION_ENUM_NAME.to_string();
                        enum_name.push_str("_Generic");
                        (enum_name, None)
                    }
                    1 => {
                        let mut enum_name = GENERATED_INVOCATION_ENUM_NAME.to_string();
                        let mut parent_name = enum_name.clone();
                        enum_name.push_str("_Arch");
                        parent_name.push_str("_Generic");
                        (enum_name, Some(parent_name))
                    }
                    2 => {
                        let mut enum_name = GENERATED_INVOCATION_ENUM_NAME.to_string();
                        let mut parent_name = enum_name.clone();
                        enum_name.push_str("_ArchType");
                        parent_name.push_str("_Arch");
                        (enum_name, Some(parent_name))
                    }
                    x @ _ => {
                        panic!(
                            "Unexpected interface group found in interface array at index {}",
                            x
                        );
                    }
                }
            };

            let key = (enum_name.clone(), iface.implied_conditions.clone());

            let e = if enums.contains_key(&key) {
                enums.get_mut(&key).unwrap()
            } else {
                let mut e = e_template.clone();
                e.name = Some(enum_name.clone());
                e.implied_conditions = iface.implied_conditions.clone();
                if parent_name == None {
                    // The first enum constant in the root enum is an invalid entry
                    e.constants.push(EnumConstant {
                        name: "seL4_IDLGEN_InvalidInvocationLabel".to_string(),
                        condition: None,
                        condition_expression: None, // will be resolved later
                        value: None,
                    });
                }
                enums.insert(key.clone(), e);
                enums.get_mut(&key).unwrap()
            };

            for x in &mut iface.methods {
                let mut name = iface.name.clone();
                name.push('_');
                name.push_str(&x.name);
                name.push_str("_InvocationLabel");

                e.constants.push(EnumConstant {
                    name: name.clone(),
                    condition: x.condition.clone(),
                    condition_expression: None, // will be resolved later
                    value: if e.constants.is_empty() {
                        if let Some(pn) = parent_name.as_ref() {
                            // the first entry' value starts at the end of the higher-level enum
                            let mut cn = pn.clone();
                            cn.push('_');
                            cn.push_str(numentries_name);
                            Some(Expression::Identifier(Identifier::EnumConstant {
                                constant_name: cn,
                                enum_name: pn.clone(),
                            }))
                        } else {
                            None
                        }
                    } else {
                        None
                    },
                });

                x.invocation_label = Some(Expression::Identifier(Identifier::EnumConstant {
                    enum_name: enum_name.clone(),
                    constant_name: name.clone(),
                }));
            }
        }
        i = i + 1
    }

    // Add an enum constant for the last entry in each enum

    for (_, mut e) in enums {
        let mut cn = e.name.as_ref().unwrap().clone();
        cn.push('_');
        cn.push_str(numentries_name);
        e.constants.push(EnumConstant {
            name: cn,
            condition: None,
            condition_expression: None, // will be resolved later
            value: None,
        });
        appstate.enums.push(e);
    }
}

fn generate_missing_arch_invocation_enums(appstate: &mut AppState, icl: &ImpliedConditionLookups) {
    // There must be an arch-specific invocation enum for all arches, but not all arches have one
    // defined. Generate the missing enums.

    let mut e_name = GENERATED_INVOCATION_ENUM_NAME.to_string();
    e_name.push('_');
    e_name.push_str("Arch");

    let mut parent_e_name = GENERATED_INVOCATION_ENUM_NAME.to_string();
    parent_e_name.push('_');
    parent_e_name.push_str("Generic");

    let mut parent_ec_name = GENERATED_INVOCATION_ENUM_NAME.to_string();
    parent_ec_name.push('_');
    parent_ec_name.push_str("Generic");
    parent_ec_name.push('_');
    parent_ec_name.push_str("NumEntries");

    let mut ec_name = GENERATED_INVOCATION_ENUM_NAME.to_string();
    ec_name.push('_');
    ec_name.push_str("Arch");
    ec_name.push('_');
    ec_name.push_str("NumEntries");

    let e_template = Enum {
        is_error: false,
        name: Some(e_name.clone()),
        condition: None,
        condition_expression: None,
        implied_conditions: ImpliedConditions {
            arch: None,
            platform: None,
            bits: None,
        },
        implied_condition_expressions: ImpliedConditionExpressions {
            arch: None,
            platform: None,
            bits: None,
        },
        constants: Vec::new(),
    };

    for (arch, _) in icl
        .arch_to_condition
        .iter()
        .filter(|(x, _)| x != &"arm_hyp")
        .filter(|(x, _)| {
            get_interface_type_from_implied_conditions(&ImpliedConditions {
                arch: Some(x.to_string()),
                platform: None,
                bits: None,
            }) == InterfaceType::Arch
        })
    {
        if appstate
            .enums
            .iter()
            .filter(|x| x.name == Some(e_name.clone()))
            .filter(|x| x.implied_conditions.arch == Some(arch.to_string()))
            .take(1)
            .count()
            == 0
        {
            trace!("Creating empty invocation enum for arch '{}'", arch);

            let mut new_e = e_template.clone();
            new_e.implied_conditions.arch = Some(arch.to_string());
            new_e.constants.push(EnumConstant {
                name: ec_name.clone(),
                condition: None,
                condition_expression: None, // will be resolved later
                value: Some(Expression::Identifier(Identifier::EnumConstant {
                    constant_name: parent_ec_name.clone(),
                    enum_name: parent_e_name.clone(),
                })),
            });

            appstate.enums.push(new_e);
        }
    }
}

fn validate_everything_has_a_name(appstate: &mut AppState) {
    for x in &mut appstate.enums {
        if x.name == None {
            warn!("Setting an enum with no name as an error");
            x.is_error = true;
        }
    }
    for x in &mut appstate.structs {
        if x.name == None {
            warn!("Setting a struct with no name as an error");
            x.is_error = true;
        }
    }
}

fn validate_all_syscalls_have_arguments(appstate: &mut AppState) {
    for x in &mut appstate.syscalls {
        if x.arguments.is_empty() && x.return_values.is_empty() {
            panic!("Syscall '{}' in group '{}' has no entry in the user-defined syscalls CSV file, please add it.", x.name, x.group);
        }
    }
}

fn validate_all_references_exist(
    appstate: &mut AppState,
    name_lookup: &BTreeMap<String, Vec<NameLookup>>,
) {
    // constants are validated when resolving expressions
    // enum constants are validated when resolved expressions
    for x in &mut appstate.types {
        if !name_lookup.contains_key(&x.type_) && !is_primitive_type(&x.type_) {
            warn!(
                "Type '{}' contains unknown reference to '{}', marking type as error",
                x.name, x.type_
            );
            x.is_error = true;
        }
    }
    for s in &mut appstate.structs {
        for f in &s.fields {
            let t = match &f.type_ {
                StructFieldType::Simple(t) => t,
                StructFieldType::Pointer(t) => t,
                StructFieldType::Array(t, _) => t,
            };
            if !name_lookup.contains_key(t) && !is_primitive_type(t) {
                warn!("Field '{}' in struct '{:?}' contains unknown reference to '{}', marking struct as error", f.name, s.name, t);
                s.is_error = true;
            }
        }
    }
    for x in &mut appstate.interfaces {
        for iface in x {
            for m in &mut iface.methods {
                for x in &mut m.arguments {
                    if !name_lookup.contains_key(&x.type_) && !is_primitive_type(&x.type_) {
                        warn!("Argument '{}' in interface method '{}::{}' contains unknown reference to '{}', marking method as error", x.name, iface.name, m.name, x.type_);
                        m.is_error = true;
                    }
                }
                for x in &mut m.return_values {
                    if !name_lookup.contains_key(&x.type_) && !is_primitive_type(&x.type_) {
                        warn!("Return value '{}' in interface method '{}::{}' contains unknown reference to '{}', marking method as error", x.name, iface.name, m.name, x.type_);
                        m.is_error = true;
                    }
                }
            }
        }
    }
    for s in &mut appstate.syscalls {
        for x in &mut s.arguments {
            if !name_lookup.contains_key(&x.type_) && !is_primitive_type(&x.type_) {
                warn!("Argument '{}' in syscall '{}' contains unknown reference to '{}', marking syscall as error", x.name, s.name, x.type_);
                s.is_error = true;
            }
        }
        for x in &mut s.return_values {
            if !name_lookup.contains_key(&x.type_) && !is_primitive_type(&x.type_) {
                warn!("Return value '{}' in syscall '{}' contains unknown reference to '{}', marking syscall as error", x.name, s.name, x.type_);
                s.is_error = true;
            }
        }
    }
    for tu in &mut appstate.tagged_unions {
        for x in &mut tu.fields {
            if !name_lookup.contains_key(&x.type_) {
                warn!("Tag '{}' in tagged union '{}' contains unknown reference to '{}', marking tagged union as error", x.tag_constant, tu.name, x.type_);
                tu.is_error = true;
            }
        }
    }
}

fn is_primitive_type(t: &str) -> bool {
    PRIMITIVE_TYPES.contains(&t)
}

fn generate_dependency_map(
    appstate: &AppState,
    name_lookup: &BTreeMap<String, Vec<NameLookup>>,
) -> (BTreeMap<String, Vec<String>>, BTreeMap<String, Vec<String>>) {
    // N.B. Dependencies are mapped between things that have an "is_error" field. Conditions are
    // not considered in the dependency map.

    let mut map = BTreeMap::new();
    let mut revmap = BTreeMap::new();

    for x in &appstate.constants {
        ensure_key_exists(&x.name, &mut map);
        ensure_key_exists(&x.name, &mut revmap);

        x.value.visit_identifiers(|i| {
            add_identifier_value_to_dependency_map(&x.name, i, &mut map, name_lookup);
            add_identifier_key_to_dependency_map(i, &x.name, &mut revmap, name_lookup);
        });
    }
    for e in &appstate.enums {
        if let Some(e_name) = e.name.as_ref() {
            ensure_key_exists(e_name, &mut map);
            ensure_key_exists(e_name, &mut revmap);

            for ec in &e.constants {
                ensure_key_exists(&ec.name, &mut map);
                ensure_key_exists(&ec.name, &mut revmap);

                if let Some(value) = ec.value.as_ref() {
                    value.visit_identifiers(|i| {
                        add_identifier_value_to_dependency_map(e_name, i, &mut map, name_lookup);
                        add_identifier_key_to_dependency_map(i, e_name, &mut revmap, name_lookup);
                    });
                }
            }
        }
    }
    for x in &appstate.types {
        ensure_key_exists(&x.name, &mut map);
        ensure_key_exists(&x.name, &mut revmap);

        add_to_dependency_map(&x.name, &x.type_, &mut map);
        add_to_dependency_map(&x.type_, &x.name, &mut revmap);
    }
    for s in &appstate.structs {
        if let Some(s_name) = s.name.as_ref() {
            ensure_key_exists(s_name, &mut map);
            ensure_key_exists(s_name, &mut revmap);

            for f in &s.fields {
                match &f.type_ {
                    StructFieldType::Simple(t) => {
                        add_to_dependency_map(s_name, t, &mut map);
                        add_to_dependency_map(t, s_name, &mut revmap);
                    }
                    StructFieldType::Pointer(t) => {
                        add_to_dependency_map(s_name, t, &mut map);
                        add_to_dependency_map(t, s_name, &mut revmap);
                    }
                    StructFieldType::Array(t, e) => {
                        add_to_dependency_map(s_name, t, &mut map);
                        add_to_dependency_map(t, s_name, &mut revmap);

                        e.visit_identifiers(|i| {
                            add_identifier_value_to_dependency_map(
                                s_name,
                                i,
                                &mut map,
                                name_lookup,
                            );
                            add_identifier_key_to_dependency_map(
                                i,
                                s_name,
                                &mut revmap,
                                name_lookup,
                            );
                        });
                    }
                }
            }
        }
    }
    for x in &appstate.interfaces {
        for iface in x {
            for m in &iface.methods {
                ensure_key_exists(&m.name, &mut map);
                ensure_key_exists(&m.name, &mut revmap);

                // the iface name is also its type, methods on the interface depend on the
                // interface type
                add_to_dependency_map(&m.name, &iface.name, &mut map);
                add_to_dependency_map(&iface.name, &m.name, &mut revmap);

                for x in &m.arguments {
                    add_to_dependency_map(&m.name, &x.type_, &mut map);
                    add_to_dependency_map(&x.type_, &m.name, &mut revmap);
                }
                for x in &m.return_values {
                    add_to_dependency_map(&m.name, &x.type_, &mut map);
                    add_to_dependency_map(&x.type_, &m.name, &mut revmap);
                }
            }
        }
    }
    for s in &appstate.syscalls {
        ensure_key_exists(&s.name, &mut map);
        ensure_key_exists(&s.name, &mut revmap);

        if let Some(value) = s.syscall_number.as_ref() {
            value.visit_identifiers(|i| {
                add_identifier_value_to_dependency_map(&s.name, i, &mut map, name_lookup);
                add_identifier_key_to_dependency_map(i, &s.name, &mut revmap, name_lookup);
            });
        }

        for x in &s.arguments {
            add_to_dependency_map(&s.name, &x.type_, &mut map);
            add_to_dependency_map(&x.type_, &s.name, &mut revmap);
        }
        for x in &s.return_values {
            add_to_dependency_map(&s.name, &x.type_, &mut map);
            add_to_dependency_map(&x.type_, &s.name, &mut revmap);
        }
    }
    for x in &appstate.bitfield_structs {
        ensure_key_exists(&x.name, &mut map);
        ensure_key_exists(&x.name, &mut revmap);

        // these have no dependencies but other things can depend on them
    }
    for tu in &appstate.tagged_unions {
        ensure_key_exists(&tu.name, &mut map);
        ensure_key_exists(&tu.name, &mut revmap);

        for x in &tu.fields {
            add_to_dependency_map(&tu.name, &x.type_, &mut map);
            add_to_dependency_map(&x.type_, &tu.name, &mut revmap);
        }
    }

    (map, revmap)
}

fn ensure_key_exists(key: &str, map: &mut BTreeMap<String, Vec<String>>) {
    let key = key.to_string();
    if !map.contains_key(&key) {
        map.insert(key, Vec::new());
    }
}

fn add_identifier_value_to_dependency_map<'a, 'b>(
    key: &'a str,
    id: &'b Identifier,
    map: &mut BTreeMap<String, Vec<String>>,
    name_lookup: &BTreeMap<String, Vec<NameLookup>>,
) {
    match id {
        Identifier::Unresolved(x) => add_to_dependency_map(key, x, map),
        Identifier::Config(_) => {}
        Identifier::Constant(x) => add_to_dependency_map(key, x, map),
        Identifier::EnumConstant {
            enum_name,
            constant_name,
            ..
        } => {
            add_to_dependency_map(key, enum_name, map);
            add_to_dependency_map(key, constant_name, map);
            // BUG: this constant name could live in multiple enums, but was only resolved into
            // one. As a workaround, redo the lookup again and add dependencies for all possible
            // enums.
            for entry in &name_lookup[constant_name] {
                if let NameLookup::EnumConstant { enum_name, .. } = entry {
                    add_to_dependency_map(key, enum_name, map);
                }
            }
        }
        Identifier::Type(x) => add_to_dependency_map(key, x, map),
    }
}

fn add_identifier_key_to_dependency_map<'a, 'b>(
    id: &'b Identifier,
    value: &'a str,
    map: &mut BTreeMap<String, Vec<String>>,
    name_lookup: &BTreeMap<String, Vec<NameLookup>>,
) {
    match id {
        Identifier::Unresolved(x) => add_to_dependency_map(x, value, map),
        Identifier::Config(_) => {}
        Identifier::Constant(x) => add_to_dependency_map(x, value, map),
        Identifier::EnumConstant {
            enum_name,
            constant_name,
            ..
        } => {
            add_to_dependency_map(enum_name, value, map);
            add_to_dependency_map(constant_name, value, map);
            // BUG: this constant name could live in multiple enums, but was only resolved into
            // one. As a workaround, redo the lookup again and add dependencies for all possible
            // enums.
            for entry in &name_lookup[constant_name] {
                if let NameLookup::EnumConstant { enum_name, .. } = entry {
                    add_to_dependency_map(enum_name, value, map);
                }
            }
        }
        Identifier::Type(x) => add_to_dependency_map(x, value, map),
    }
}

fn add_to_dependency_map<'a, 'b>(
    key: &'a str,
    value: &'b str,
    map: &mut BTreeMap<String, Vec<String>>,
) {
    // don't care about if something depends on itself
    if key != value {
        let key = key.to_string();
        if map.contains_key(&key) {
            let vec = map.get_mut(&key).unwrap();
            let v = value.to_string();
            if !vec.contains(&v) {
                vec.push(v);
            }
        } else {
            let mut vec = Vec::new();
            vec.push(value.to_string());
            map.insert(key, vec);
        }
    }
}

fn identify_caps_in_interface_methods(
    appstate: &mut AppState,
    depmap: &BTreeMap<String, Vec<String>>,
) {
    for x in &mut appstate.interfaces {
        for x in x {
            for method in &mut x.methods {
                for arg in &mut method.arguments {
                    arg.is_cap = Some(has_dependency(&arg.type_, "seL4_CPtr", depmap));
                }
                for arg in &mut method.return_values {
                    arg.is_cap = Some(has_dependency(&arg.type_, "seL4_CPtr", depmap));
                }
            }
        }
    }
}

fn has_dependency(name: &str, dep: &str, depmap: &BTreeMap<String, Vec<String>>) -> bool {
    if name == dep {
        return true;
    }

    if depmap.contains_key(name) {
        for entry in &depmap[name] {
            if entry == dep {
                return true;
            } else {
                if has_dependency(entry, dep, depmap) {
                    return true;
                }
            }
        }
    }

    false
}

fn propagate_errors_to_dependencies(
    appstate: &mut AppState,
    name_lookup: &BTreeMap<String, Vec<NameLookup>>,
    rev_depmap: &BTreeMap<String, Vec<String>>,
) {
    // we consider the first dependency to be ourself, in case there are multiple conditionals on
    // our name: if one conditional is in error then we consider all conditionals of our name to be
    // in error

    for (name, vec) in name_lookup.iter() {
        if are_any_in_error(vec, appstate) {
            propagate_error_to_dependencies(name, appstate, name_lookup, rev_depmap, 0);
        }
    }
}

fn are_any_in_error(vec: &Vec<NameLookup>, appstate: &AppState) -> bool {
    for nl in vec {
        if nl.is_error(appstate) {
            return true;
        }
    }

    false
}

fn propagate_error_to_dependencies(
    name: &str,
    appstate: &mut AppState,
    name_lookup: &BTreeMap<String, Vec<NameLookup>>,
    rev_depmap: &BTreeMap<String, Vec<String>>,
    depth: u32,
) {
    if depth > MAXIMUM_DEPENDENCY_GRAPH_DEPTH {
        panic!("Cycle detected in reverse dependency graph");
    }

    if name_lookup.contains_key(name) {
        for nl in name_lookup[name].iter() {
            nl.set_error(appstate);
        }
    }

    if rev_depmap.contains_key(name) {
        for depname in rev_depmap[name].iter() {
            warn!(
                "Marking '{}' as error because it depends on '{}' which is in error",
                depname, name
            );
            propagate_error_to_dependencies(depname, appstate, name_lookup, rev_depmap, depth + 1);
        }
    }
}

impl NameLookup {
    // TODO: Might make sense to use a macro to generate these

    fn resolve_constant<'a>(&self, appstate: &'a AppState) -> &'a Constant {
        if let NameLookup::Constant(x) = *self {
            &appstate.constants[x]
        } else {
            panic!("Tried to resolve {:?} into a Constant", self);
        }
    }

    fn resolve_constant_mut<'a>(&self, appstate: &'a mut AppState) -> &'a mut Constant {
        if let NameLookup::Constant(x) = *self {
            &mut appstate.constants[x]
        } else {
            panic!("Tried to resolve {:?} into a Constant", self);
        }
    }

    fn resolve_enum<'a>(&self, appstate: &'a AppState) -> &'a Enum {
        if let NameLookup::Enum(x) = *self {
            &appstate.enums[x]
        } else {
            panic!("Tried to resolve {:?} into an Enum", self);
        }
    }

    fn resolve_enum_mut<'a>(&self, appstate: &'a mut AppState) -> &'a mut Enum {
        if let NameLookup::Enum(x) = *self {
            &mut appstate.enums[x]
        } else {
            panic!("Tried to resolve {:?} into an Enum", self);
        }
    }

    fn resolve_type<'a>(&self, appstate: &'a AppState) -> &'a Typedef {
        if let NameLookup::Type(x) = *self {
            &appstate.types[x]
        } else {
            panic!("Tried to resolve {:?} into a Typedef", self);
        }
    }

    fn resolve_type_mut<'a>(&self, appstate: &'a mut AppState) -> &'a mut Typedef {
        if let NameLookup::Type(x) = *self {
            &mut appstate.types[x]
        } else {
            panic!("Tried to resolve {:?} into a Typedef", self);
        }
    }

    fn resolve_struct<'a>(&self, appstate: &'a AppState) -> &'a Struct {
        if let NameLookup::Struct(x) = *self {
            &appstate.structs[x]
        } else {
            panic!("Tried to resolve {:?} into a Struct", self);
        }
    }

    fn resolve_struct_mut<'a>(&self, appstate: &'a mut AppState) -> &'a mut Struct {
        if let NameLookup::Struct(x) = *self {
            &mut appstate.structs[x]
        } else {
            panic!("Tried to resolve {:?} into a Struct", self);
        }
    }

    fn resolve_bitfield_struct<'a>(&self, appstate: &'a AppState) -> &'a BitfieldStruct {
        if let NameLookup::BitfieldStruct(x) = *self {
            &appstate.bitfield_structs[x]
        } else {
            panic!("Tried to resolve {:?} into a BitfieldStruct", self);
        }
    }

    fn resolve_bitfield_struct_mut<'a>(
        &self,
        appstate: &'a mut AppState,
    ) -> &'a mut BitfieldStruct {
        if let NameLookup::BitfieldStruct(x) = *self {
            &mut appstate.bitfield_structs[x]
        } else {
            panic!("Tried to resolve {:?} into a BitfieldStruct", self);
        }
    }

    fn resolve_tagged_union<'a>(&self, appstate: &'a AppState) -> &'a TaggedUnion {
        if let NameLookup::TaggedUnion(x) = *self {
            &appstate.tagged_unions[x]
        } else {
            panic!("Tried to resolve {:?} into a TaggedUnion", self);
        }
    }

    fn resolve_tagged_union_mut<'a>(&self, appstate: &'a mut AppState) -> &'a mut TaggedUnion {
        if let NameLookup::TaggedUnion(x) = *self {
            &mut appstate.tagged_unions[x]
        } else {
            panic!("Tried to resolve {:?} into a TaggedUnion", self);
        }
    }

    fn resolve_interface_method<'a>(&self, appstate: &'a AppState) -> &'a Method {
        if let NameLookup::InterfaceMethod {
            interface_root_index,
            interface_index,
            method_index,
        } = *self
        {
            &appstate.interfaces[interface_root_index][interface_index].methods[method_index]
        } else {
            panic!("Tried to resolve {:?} into an InterfaceMethod", self);
        }
    }

    fn resolve_interface_method_mut<'a>(&self, appstate: &'a mut AppState) -> &'a mut Method {
        if let NameLookup::InterfaceMethod {
            interface_root_index,
            interface_index,
            method_index,
        } = *self
        {
            &mut appstate.interfaces[interface_root_index][interface_index].methods[method_index]
        } else {
            panic!("Tried to resolve {:?} into an InterfaceMethod", self);
        }
    }

    fn resolve_syscall<'a>(&self, appstate: &'a AppState) -> &'a Syscall {
        if let NameLookup::Syscall(x) = *self {
            &appstate.syscalls[x]
        } else {
            panic!("Tried to resolve {:?} into a Syscall", self);
        }
    }

    fn resolve_syscall_mut<'a>(&self, appstate: &'a mut AppState) -> &'a mut Syscall {
        if let NameLookup::Syscall(x) = *self {
            &mut appstate.syscalls[x]
        } else {
            panic!("Tried to resolve {:?} into a Syscall", self);
        }
    }

    fn is_error(&self, appstate: &AppState) -> bool {
        match *self {
            NameLookup::Constant(_) => self.resolve_constant(appstate).is_error,
            NameLookup::Enum(_) => self.resolve_enum(appstate).is_error,
            NameLookup::Type(_) => self.resolve_type(appstate).is_error,
            NameLookup::Struct(_) => self.resolve_struct(appstate).is_error,
            NameLookup::BitfieldStruct(_) => self.resolve_bitfield_struct(appstate).is_error,
            NameLookup::TaggedUnion(_) => self.resolve_tagged_union(appstate).is_error,
            NameLookup::InterfaceMethod { .. } => self.resolve_interface_method(appstate).is_error,
            NameLookup::Syscall(_) => self.resolve_syscall(appstate).is_error,
            // These don't have is_error
            NameLookup::EnumConstant { .. } => false,
        }
    }

    fn set_error(&self, appstate: &mut AppState) {
        match *self {
            NameLookup::Constant(_) => self.resolve_constant_mut(appstate).is_error = true,
            NameLookup::Enum(_) => self.resolve_enum_mut(appstate).is_error = true,
            NameLookup::Type(_) => self.resolve_type_mut(appstate).is_error = true,
            NameLookup::Struct(_) => self.resolve_struct_mut(appstate).is_error = true,
            NameLookup::BitfieldStruct(_) => {
                self.resolve_bitfield_struct_mut(appstate).is_error = true
            }
            NameLookup::TaggedUnion(_) => self.resolve_tagged_union_mut(appstate).is_error = true,
            NameLookup::InterfaceMethod { .. } => {
                self.resolve_interface_method_mut(appstate).is_error = true
            }
            NameLookup::Syscall(_) => self.resolve_syscall_mut(appstate).is_error = true,
            // These don't have is_error
            NameLookup::EnumConstant { .. } => {}
        }
    }
}

// https://en.wikipedia.org/wiki/Topological_sorting
fn topological_sort_dependency_map(depmap: &BTreeMap<String, Vec<String>>) -> Vec<&str> {
    let mut sorted = Vec::new();
    let mut unsorted = depmap.keys().collect::<BTreeSet<&String>>();
    let mut tempmarks = BTreeSet::new();

    while unsorted.len() > 0 {
        topological_sort_visit_node(
            unsorted.iter().nth(0).unwrap(),
            depmap,
            &mut unsorted,
            &mut tempmarks,
            &mut sorted,
        );
    }

    sorted
}

fn topological_sort_visit_node<'a>(
    name: &'a String,
    depmap: &'a BTreeMap<String, Vec<String>>,
    unsorted: &mut BTreeSet<&'a String>,
    tempmarks: &mut BTreeSet<&'a str>,
    sorted: &mut Vec<&'a str>,
) {
    trace!("topological sort {}", name);

    if !unsorted.contains(&name) {
        return;
    }

    if tempmarks.contains(&name.as_ref()) {
        panic!("Cycle detected in dependency graph");
    }

    tempmarks.insert(&name);

    if depmap.contains_key(name) {
        for child in &depmap[name] {
            topological_sort_visit_node(child, depmap, unsorted, tempmarks, sorted);
        }
    }

    tempmarks.remove(&name.as_ref());
    unsorted.remove(&name);
    sorted.insert(0, &name);
}

fn validate_topological_sort_contains_all_names(
    sorted_names: &Vec<&str>,
    name_lookup: &BTreeMap<String, Vec<NameLookup>>,
) {
    let sorted_len = sorted_names
        .iter()
        .filter(|x| name_lookup.contains_key(&x.to_string()))
        .count();
    let name_lookup_len = name_lookup.len();

    if sorted_len != name_lookup_len {
        panic!(
            "Number of sorted names '{}' != number of names '{}'",
            sorted_len, name_lookup_len
        );
    }
}

fn generate_output<W: Write>(
    appstate: &AppState,
    name_lookup: &BTreeMap<String, Vec<NameLookup>>,
    sorted_names: &Vec<&str>,
    output: W,
) {
    let mut writer = quick_xml::Writer::new_with_indent(output, b' ', 4);

    let mut elem = open_tag(b"Sel4");
    elem.push_attribute(("xml_file_version", OUTPUT_FILE_VERSION.to_string().as_ref()));
    write_open_tag(elem, &mut writer);

    let elem = open_tag(b"Items");
    write_open_tag(elem, &mut writer);

    // Pluck out the things that must be in dependency order from the sorted names list and process
    // those first

    for name in sorted_names {
        if name_lookup.contains_key(&name.to_string()) {
            let vec = &name_lookup[&name.to_string()];
            let count = get_number_of_relevent_items(&vec);

            if count == 0 {
                continue;
            }

            let mut elem = open_tag(b"Group");
            elem.push_attribute(("name", *name));
            elem.push_attribute(("len", count.to_string().as_ref()));
            write_open_tag(elem, &mut writer);

            for x in vec {
                match x {
                    NameLookup::Constant(_) => {
                        let x = x.resolve_constant(appstate);
                        if x.is_error {
                            write_error(*name, "Constant", &x.implied_conditions, &mut writer);
                        } else {
                            let mut elem = open_tag(b"Constant");
                            elem.push_attribute(("name", x.name.as_ref()));
                            add_implied_condition_attribs(&x.implied_conditions, &mut elem);
                            write_open_tag(elem, &mut writer);
                            write_implied_condition_expressions(
                                &x.implied_condition_expressions,
                                &mut writer,
                            );
                            write_condition(b"Condition", &x.condition_expression, &mut writer);
                            write_condition(
                                b"FullCondition",
                                &combined_expression(
                                    &x.implied_condition_expressions,
                                    &x.condition_expression,
                                ),
                                &mut writer,
                            );
                            let elem = open_tag(b"ConstantValue");
                            write_open_tag(elem, &mut writer);
                            write_expression(&x.value, &mut writer);
                            write_close_tag(b"ConstantValue", &mut writer);
                            write_close_tag(b"Constant", &mut writer);
                        }
                    }
                    NameLookup::Enum(_) => {
                        let x = x.resolve_enum(appstate);
                        if x.is_error {
                            write_error(*name, "Enum", &x.implied_conditions, &mut writer);
                        } else {
                            let mut elem = open_tag(b"Enum");
                            elem.push_attribute(("name", x.name.as_ref().unwrap().as_ref()));
                            add_implied_condition_attribs(&x.implied_conditions, &mut elem);
                            write_open_tag(elem, &mut writer);
                            write_implied_condition_expressions(
                                &x.implied_condition_expressions,
                                &mut writer,
                            );
                            write_condition(b"Condition", &x.condition_expression, &mut writer);
                            write_condition(
                                b"FullCondition",
                                &combined_expression(
                                    &x.implied_condition_expressions,
                                    &x.condition_expression,
                                ),
                                &mut writer,
                            );
                            let elem = open_tag(b"EnumConstants");
                            write_open_tag(elem, &mut writer);
                            for x in &x.constants {
                                let mut elem = open_tag(b"EnumConstant");
                                elem.push_attribute(("name", x.name.as_ref()));
                                write_open_tag(elem, &mut writer);
                                write_condition(b"Condition", &x.condition_expression, &mut writer);
                                write_condition(
                                    b"FullCondition",
                                    &x.condition_expression,
                                    &mut writer,
                                );
                                if let Some(value) = &x.value {
                                    let elem = open_tag(b"EnumConstantValue");
                                    write_open_tag(elem, &mut writer);
                                    write_expression(value, &mut writer);
                                    write_close_tag(b"EnumConstantValue", &mut writer);
                                }
                                write_close_tag(b"EnumConstant", &mut writer);
                            }
                            write_close_tag(b"EnumConstants", &mut writer);
                            write_close_tag(b"Enum", &mut writer);
                        }
                    }
                    NameLookup::Type(_) => {
                        let x = x.resolve_type(appstate);
                        if x.is_error {
                            write_error(*name, "Type", &x.implied_conditions, &mut writer);
                        } else {
                            let mut elem = open_tag(b"Type");
                            elem.push_attribute(("name", x.name.as_ref()));
                            elem.push_attribute(("type", x.type_.as_ref()));
                            add_implied_condition_attribs(&x.implied_conditions, &mut elem);
                            write_open_tag(elem, &mut writer);
                            write_implied_condition_expressions(
                                &x.implied_condition_expressions,
                                &mut writer,
                            );
                            write_condition(b"Condition", &x.condition_expression, &mut writer);
                            write_condition(
                                b"FullCondition",
                                &combined_expression(
                                    &x.implied_condition_expressions,
                                    &x.condition_expression,
                                ),
                                &mut writer,
                            );
                            write_close_tag(b"Type", &mut writer);
                        }
                    }
                    NameLookup::Struct(_) => {
                        let x = x.resolve_struct(appstate);
                        if x.is_error {
                            write_error(*name, "Struct", &x.implied_conditions, &mut writer);
                        } else {
                            let mut elem = open_tag(b"Struct");
                            elem.push_attribute(("name", x.name.as_ref().unwrap().as_ref()));
                            elem.push_attribute((
                                "is_packed",
                                if x.is_packed { "true" } else { "false" },
                            ));
                            add_implied_condition_attribs(&x.implied_conditions, &mut elem);
                            write_open_tag(elem, &mut writer);
                            write_implied_condition_expressions(
                                &x.implied_condition_expressions,
                                &mut writer,
                            );
                            write_condition(b"Condition", &x.condition_expression, &mut writer);
                            write_condition(
                                b"FullCondition",
                                &combined_expression(
                                    &x.implied_condition_expressions,
                                    &x.condition_expression,
                                ),
                                &mut writer,
                            );
                            let elem = open_tag(b"StructFields");
                            write_open_tag(elem, &mut writer);
                            for x in &x.fields {
                                let mut elem = open_tag(b"StructField");
                                elem.push_attribute(("name", x.name.as_ref()));
                                write_open_tag(elem, &mut writer);
                                write_condition(b"Condition", &x.condition_expression, &mut writer);
                                write_condition(
                                    b"FullCondition",
                                    &x.condition_expression,
                                    &mut writer,
                                );
                                let elem = open_tag(b"StructFieldType");
                                write_open_tag(elem, &mut writer);
                                match &x.type_ {
                                    StructFieldType::Simple(t) => {
                                        let mut elem = open_tag(b"Simple");
                                        elem.push_attribute(("type", t.as_ref()));
                                        write_open_tag(elem, &mut writer);
                                        write_close_tag(b"Simple", &mut writer);
                                    }
                                    StructFieldType::Pointer(t) => {
                                        let mut elem = open_tag(b"Pointer");
                                        elem.push_attribute(("type", t.as_ref()));
                                        write_open_tag(elem, &mut writer);
                                        write_close_tag(b"Pointer", &mut writer);
                                    }
                                    StructFieldType::Array(t, e) => {
                                        let mut elem = open_tag(b"Array");
                                        elem.push_attribute(("type", t.as_ref()));
                                        write_open_tag(elem, &mut writer);
                                        let elem = open_tag(b"ArrayLength");
                                        write_open_tag(elem, &mut writer);
                                        write_expression(e, &mut writer);
                                        write_close_tag(b"ArrayLength", &mut writer);
                                        write_close_tag(b"Array", &mut writer);
                                    }
                                }
                                write_close_tag(b"StructFieldType", &mut writer);
                                write_close_tag(b"StructField", &mut writer);
                            }
                            write_close_tag(b"StructFields", &mut writer);
                            write_close_tag(b"Struct", &mut writer);
                        }
                    }
                    NameLookup::BitfieldStruct(_) => {
                        let x = x.resolve_bitfield_struct(appstate);
                        if x.is_error {
                            write_error(
                                *name,
                                "BitfieldStruct",
                                &x.implied_conditions,
                                &mut writer,
                            );
                        } else {
                            let mut elem = open_tag(b"BitfieldStruct");
                            elem.push_attribute(("name", x.name.as_ref()));
                            add_implied_condition_attribs(&x.implied_conditions, &mut elem);
                            write_open_tag(elem, &mut writer);
                            write_implied_condition_expressions(
                                &x.implied_condition_expressions,
                                &mut writer,
                            );
                            write_condition(b"Condition", &x.condition_expression, &mut writer);
                            write_condition(
                                b"FullCondition",
                                &combined_expression(
                                    &x.implied_condition_expressions,
                                    &x.condition_expression,
                                ),
                                &mut writer,
                            );
                            let elem = open_tag(b"BitfieldStructFields");
                            write_open_tag(elem, &mut writer);
                            for x in &x.fields {
                                match x {
                                    BitfieldStructField::Padding {
                                        condition_expression,
                                        bits,
                                        ..
                                    } => {
                                        let mut elem = open_tag(b"BitfieldStructField");
                                        elem.push_attribute(("bits", bits.to_string().as_ref()));
                                        write_open_tag(elem, &mut writer);
                                        write_condition(
                                            b"Condition",
                                            &condition_expression,
                                            &mut writer,
                                        );
                                        write_condition(
                                            b"FullCondition",
                                            &condition_expression,
                                            &mut writer,
                                        );
                                        write_close_tag(b"BitfieldStructField", &mut writer);
                                    }
                                    BitfieldStructField::Field {
                                        name,
                                        condition_expression,
                                        bits,
                                        ..
                                    } => {
                                        let mut elem = open_tag(b"BitfieldStructField");
                                        elem.push_attribute(("name", name.as_ref()));
                                        elem.push_attribute(("bits", bits.to_string().as_ref()));
                                        write_open_tag(elem, &mut writer);
                                        write_condition(
                                            b"Condition",
                                            &condition_expression,
                                            &mut writer,
                                        );
                                        write_condition(
                                            b"FullCondition",
                                            &condition_expression,
                                            &mut writer,
                                        );
                                        write_close_tag(b"BitfieldStructField", &mut writer);
                                    }
                                }
                            }
                            write_close_tag(b"BitfieldStructFields", &mut writer);
                            write_close_tag(b"BitfieldStruct", &mut writer);
                        }
                    }
                    NameLookup::TaggedUnion(_) => {
                        let x = x.resolve_tagged_union(appstate);
                        if x.is_error {
                            write_error(*name, "TaggedUnion", &x.implied_conditions, &mut writer);
                        } else {
                            let mut elem = open_tag(b"TaggedUnion");
                            elem.push_attribute(("name", x.name.as_ref()));
                            elem.push_attribute(("tag_name", x.tag_name.as_ref()));
                            add_implied_condition_attribs(&x.implied_conditions, &mut elem);
                            write_open_tag(elem, &mut writer);
                            write_implied_condition_expressions(
                                &x.implied_condition_expressions,
                                &mut writer,
                            );
                            write_condition(b"Condition", &x.condition_expression, &mut writer);
                            write_condition(
                                b"FullCondition",
                                &combined_expression(
                                    &x.implied_condition_expressions,
                                    &x.condition_expression,
                                ),
                                &mut writer,
                            );
                            let elem = open_tag(b"TaggedUnionFields");
                            write_open_tag(elem, &mut writer);
                            for x in &x.fields {
                                let mut elem = open_tag(b"TaggedUnionField");
                                elem.push_attribute((
                                    "tag_constant",
                                    x.tag_constant.to_string().as_ref(),
                                ));
                                elem.push_attribute(("type", x.type_.as_ref()));
                                write_open_tag(elem, &mut writer);
                                write_condition(b"Condition", &x.condition_expression, &mut writer);
                                write_condition(
                                    b"FullCondition",
                                    &x.condition_expression,
                                    &mut writer,
                                );
                                write_close_tag(b"TaggedUnionField", &mut writer);
                            }
                            write_close_tag(b"TaggedUnionFields", &mut writer);
                            write_close_tag(b"TaggedUnion", &mut writer);
                        }
                    }
                    NameLookup::Syscall(_) => {
                        let x = x.resolve_syscall(appstate);
                        if x.is_error {
                            write_error(
                                *name,
                                "Syscall",
                                &ImpliedConditions {
                                    arch: None,
                                    platform: None,
                                    bits: None,
                                },
                                &mut writer,
                            );
                        } else {
                            let mut elem = open_tag(b"Syscall");
                            elem.push_attribute(("name", x.name.as_ref()));
                            elem.push_attribute((
                                "does_send_ipcbuf",
                                if x.does_send_ipcbuf { "true" } else { "false" },
                            ));
                            elem.push_attribute((
                                "does_receive_ipcbuf",
                                if x.does_receive_ipcbuf {
                                    "true"
                                } else {
                                    "false"
                                },
                            ));
                            write_open_tag(elem, &mut writer);
                            write_condition(b"Condition", &x.condition_expression, &mut writer);
                            write_condition(b"FullCondition", &x.condition_expression, &mut writer);
                            let elem = open_tag(b"SyscallNumber");
                            write_open_tag(elem, &mut writer);
                            write_expression(x.syscall_number.as_ref().unwrap(), &mut writer);
                            write_close_tag(b"SyscallNumber", &mut writer);
                            let elem = open_tag(b"SyscallArguments");
                            write_open_tag(elem, &mut writer);
                            for args in &x.arguments {
                                for reg in appstate
                                    .registers
                                    .iter()
                                    .filter(|r| r.symbolic_register == args.symbolic_register)
                                    .filter(|r| !r.machine_register.is_empty())
                                {
                                    let mut elem = open_tag(b"SyscallArgument");
                                    elem.push_attribute(("name", args.name.as_ref()));
                                    elem.push_attribute(("type", args.type_.as_ref()));
                                    if !reg.machine_register.is_empty() {
                                        elem.push_attribute((
                                            "register",
                                            reg.machine_register.as_ref(),
                                        ));
                                    }
                                    if let Some(v) = args.special_value.as_ref() {
                                        elem.push_attribute(("special_value", v.as_ref()));
                                    }
                                    write_open_tag(elem, &mut writer);
                                    write_condition(
                                        b"Condition",
                                        &reg.arch_expression,
                                        &mut writer,
                                    );
                                    write_condition(
                                        b"FullCondition",
                                        &reg.arch_expression,
                                        &mut writer,
                                    );
                                    write_close_tag(b"SyscallArgument", &mut writer);
                                }
                            }
                            write_close_tag(b"SyscallArguments", &mut writer);
                            let elem = open_tag(b"SyscallReturnValues");
                            write_open_tag(elem, &mut writer);
                            for args in &x.return_values {
                                for reg in appstate
                                    .registers
                                    .iter()
                                    .filter(|r| r.symbolic_register == args.symbolic_register)
                                    .filter(|r| !r.machine_register.is_empty())
                                {
                                    let mut elem = open_tag(b"SyscallReturnValue");
                                    elem.push_attribute(("name", args.name.as_ref()));
                                    elem.push_attribute(("type", args.type_.as_ref()));
                                    elem.push_attribute((
                                        "register",
                                        reg.machine_register.as_ref(),
                                    ));
                                    if let Some(v) = args.special_value.as_ref() {
                                        elem.push_attribute(("special_value", v.as_ref()));
                                    }
                                    write_open_tag(elem, &mut writer);
                                    write_condition(
                                        b"Condition",
                                        &reg.arch_expression,
                                        &mut writer,
                                    );
                                    write_condition(
                                        b"FullCondition",
                                        &reg.arch_expression,
                                        &mut writer,
                                    );
                                    write_close_tag(b"SyscallReturnValue", &mut writer);
                                }
                            }
                            write_close_tag(b"SyscallReturnValues", &mut writer);
                            write_close_tag(b"Syscall", &mut writer);
                        }
                    }
                    _ => {}
                }
            }

            write_close_tag(b"Group", &mut writer);
        }
    }

    // Now do the stuff that's left
    for x in &appstate.interfaces {
        for iface in x {
            let mut elem = open_tag(b"Group");
            elem.push_attribute(("name", iface.name.as_ref()));
            elem.push_attribute(("len", "1"));
            write_open_tag(elem, &mut writer);
            let mut elem = open_tag(b"Interface");
            elem.push_attribute(("name", iface.name.as_ref()));
            add_implied_condition_attribs(&iface.implied_conditions, &mut elem);
            write_open_tag(elem, &mut writer);
            write_implied_condition_expressions(&iface.implied_condition_expressions, &mut writer);
            write_condition(
                b"FullCondition",
                &combined_expression(&iface.implied_condition_expressions, &None),
                &mut writer,
            );
            let elem = open_tag(b"Methods");
            write_open_tag(elem, &mut writer);
            for method in &iface.methods {
                if method.is_error {
                    write_error(
                        method.name.as_ref(),
                        "Method",
                        &ImpliedConditions {
                            arch: None,
                            platform: None,
                            bits: None,
                        },
                        &mut writer,
                    );
                } else {
                    let mut elem = open_tag(b"Method");
                    elem.push_attribute(("name", method.name.as_ref()));
                    write_open_tag(elem, &mut writer);
                    write_condition(b"Condition", &method.condition_expression, &mut writer);
                    write_condition(b"FullCondition", &method.condition_expression, &mut writer);
                    let elem = open_tag(b"InvocationLabel");
                    write_open_tag(elem, &mut writer);
                    write_expression(method.invocation_label.as_ref().unwrap(), &mut writer);
                    write_close_tag(b"InvocationLabel", &mut writer);
                    let elem = open_tag(b"MethodArguments");
                    write_open_tag(elem, &mut writer);
                    for arg in &method.arguments {
                        let mut elem = open_tag(b"MethodArgument");
                        elem.push_attribute(("name", arg.name.as_ref()));
                        elem.push_attribute(("type", arg.type_.as_ref()));
                        if arg.is_cap.unwrap() {
                            elem.push_attribute(("is_cap", "true"));
                        } else {
                            elem.push_attribute(("is_cap", "false"));
                        }
                        write_open_tag(elem, &mut writer);
                        write_close_tag(b"MethodArgument", &mut writer);
                    }
                    write_close_tag(b"MethodArguments", &mut writer);
                    let elem = open_tag(b"MethodReturnValues");
                    write_open_tag(elem, &mut writer);
                    for arg in &method.return_values {
                        let mut elem = open_tag(b"MethodReturnValue");
                        elem.push_attribute(("name", arg.name.as_ref()));
                        elem.push_attribute(("type", arg.type_.as_ref()));
                        if arg.is_cap.unwrap() {
                            elem.push_attribute(("is_cap", "true"));
                        } else {
                            elem.push_attribute(("is_cap", "false"));
                        }
                        write_open_tag(elem, &mut writer);
                        write_close_tag(b"MethodReturnValue", &mut writer);
                    }
                    write_close_tag(b"MethodReturnValues", &mut writer);
                    write_close_tag(b"Method", &mut writer);
                }
            }
            write_close_tag(b"Methods", &mut writer);
            write_close_tag(b"Interface", &mut writer);
            write_close_tag(b"Group", &mut writer);
        }
    }

    write_close_tag(b"Items", &mut writer);
    write_close_tag(b"Sel4", &mut writer);
}

fn open_tag(name: &[u8]) -> quick_xml::events::BytesStart {
    quick_xml::events::BytesStart::owned(name.to_vec(), name.len())
}

fn write_open_tag<W: Write>(
    elem: quick_xml::events::BytesStart,
    writer: &mut quick_xml::Writer<W>,
) {
    writer
        .write_event(quick_xml::events::Event::Start(elem))
        .unwrap();
}

fn write_close_tag<W: Write>(tag: &[u8], writer: &mut quick_xml::Writer<W>) {
    writer
        .write_event(quick_xml::events::Event::End(
            quick_xml::events::BytesEnd::borrowed(tag),
        ))
        .unwrap();
}

fn get_number_of_relevent_items(vec: &Vec<NameLookup>) -> usize {
    vec.iter()
        .filter(|x| match x {
            NameLookup::Constant(_) => true,
            NameLookup::Enum(_) => true,
            NameLookup::EnumConstant { .. } => false,
            NameLookup::Type(_) => true,
            NameLookup::Struct(_) => true,
            NameLookup::BitfieldStruct(_) => true,
            NameLookup::TaggedUnion(_) => true,
            NameLookup::InterfaceMethod { .. } => false,
            NameLookup::Syscall(_) => true,
        })
        .count()
}

fn write_error<W: Write>(
    name: &str,
    type_: &str,
    ic: &ImpliedConditions,
    writer: &mut quick_xml::Writer<W>,
) {
    let mut elem = open_tag(b"Error");
    elem.push_attribute(("type", type_));
    elem.push_attribute(("name", name));
    add_implied_condition_attribs(ic, &mut elem);
    write_open_tag(elem, writer);
    write_close_tag(b"Error", writer);
}

fn add_implied_condition_attribs(ic: &ImpliedConditions, elem: &mut quick_xml::events::BytesStart) {
    if let Some(x) = &ic.arch {
        elem.push_attribute(("arch", x.as_ref()));
    }
    if let Some(x) = &ic.platform {
        elem.push_attribute(("platform", x.as_ref()));
    }
    if let Some(x) = ic.bits {
        elem.push_attribute(("arch_word_size", bits_to_string(x)));
    }
}

fn bits_to_string(bits: Bits) -> &'static str {
    match bits {
        Bits::Bits32 => "32",
        Bits::Bits64 => "64",
    }
}

fn write_implied_condition_expressions<W: Write>(
    ic: &ImpliedConditionExpressions,
    writer: &mut quick_xml::Writer<W>,
) {
    if let Some(x) = &ic.arch {
        let elem = open_tag(b"ArchCondition");
        write_open_tag(elem, writer);
        write_expression(x, writer);
        write_close_tag(b"ArchCondition", writer);
    }
    if let Some(x) = &ic.platform {
        let elem = open_tag(b"PlatformCondition");
        write_open_tag(elem, writer);
        write_expression(x, writer);
        write_close_tag(b"PlatformCondition", writer);
    }
    if let Some(x) = &ic.bits {
        let elem = open_tag(b"ArchWordSizeCondition");
        write_open_tag(elem, writer);
        write_expression(x, writer);
        write_close_tag(b"ArchWordSizeCondition", writer);
    }
}

fn write_expression<W: Write>(e: &Expression, writer: &mut quick_xml::Writer<W>) {
    match e {
        Expression::Identifier(x) => {
            //let elem = open_tag(b"Identifier");
            //write_open_tag(elem, writer);
            write_identifier(x, writer);
            //write_close_tag(b"Identifier", writer);
        }
        Expression::StringLiteral(x) => {
            let elem = open_tag(b"StringLiteral");
            write_open_tag(elem, writer);
            write_text(x, writer);
            write_close_tag(b"StringLiteral", writer);
        }
        Expression::NumericLiteral(x) => {
            let elem = open_tag(b"NumericLiteral");
            write_open_tag(elem, writer);
            write_text(&x.to_string(), writer);
            write_close_tag(b"NumericLiteral", writer);
        }
        Expression::UnaryOperator { op, arg1 } => {
            let mut elem = open_tag(b"UnaryOperator");
            elem.push_attribute(("operator", op.to_str()));
            write_open_tag(elem, writer);
            let elem = open_tag(b"Arg1");
            write_open_tag(elem, writer);
            write_expression(arg1, writer);
            write_close_tag(b"Arg1", writer);
            write_close_tag(b"UnaryOperator", writer);
        }
        Expression::BinaryOperator { op, arg1, arg2 } => {
            let mut elem = open_tag(b"BinaryOperator");
            elem.push_attribute(("operator", op.to_str()));
            write_open_tag(elem, writer);
            let elem = open_tag(b"Arg1");
            write_open_tag(elem, writer);
            write_expression(arg1, writer);
            write_close_tag(b"Arg1", writer);
            let elem = open_tag(b"Arg2");
            write_open_tag(elem, writer);
            write_expression(arg2, writer);
            write_close_tag(b"Arg2", writer);
            write_close_tag(b"BinaryOperator", writer);
        }
    }
}

fn write_text<W: Write>(t: &str, writer: &mut quick_xml::Writer<W>) {
    writer
        .write_event(quick_xml::events::Event::Text(
            quick_xml::events::BytesText::from_plain_str(t),
        ))
        .unwrap();
}

fn write_identifier<W: Write>(i: &Identifier, writer: &mut quick_xml::Writer<W>) {
    match i {
        Identifier::Config(x) => {
            let elem = open_tag(b"ConfigIdentifier");
            write_open_tag(elem, writer);
            write_text(x, writer);
            write_close_tag(b"ConfigIdentifier", writer);
        }
        Identifier::Constant(x) => {
            let elem = open_tag(b"ConstantIdentifier");
            write_open_tag(elem, writer);
            write_text(x, writer);
            write_close_tag(b"ConstantIdentifier", writer);
        }
        Identifier::EnumConstant {
            constant_name,
            enum_name,
        } => {
            let mut elem = open_tag(b"EnumConstantIdentifier");
            elem.push_attribute(("enum_name", enum_name.as_ref()));
            write_open_tag(elem, writer);
            write_text(constant_name, writer);
            write_close_tag(b"EnumConstantIdentifier", writer);
        }
        Identifier::Type(x) => {
            let elem = open_tag(b"TypeIdentifier");
            write_open_tag(elem, writer);
            write_text(x, writer);
            write_close_tag(b"TypeIdentifier", writer);
        }
        Identifier::Unresolved(x) => panic!("Unresolved Identifier '{}'", x),
    }
}

fn write_condition<W: Write>(
    elem_name: &[u8],
    e: &Option<Expression>,
    writer: &mut quick_xml::Writer<W>,
) {
    if let Some(x) = e {
        let elem = open_tag(elem_name);
        write_open_tag(elem, writer);
        write_expression(x, writer);
        write_close_tag(elem_name, writer);
    }
}

fn combined_expression(
    ic: &ImpliedConditionExpressions,
    c: &Option<Expression>,
) -> Option<Expression> {
    let mut result: Option<Expression> = None;

    if let Some(x) = &ic.arch {
        match result {
            Some(r) => {
                result = Some(Expression::BinaryOperator {
                    op: Operator::LogicalAnd,
                    arg1: Box::new(r.clone()),
                    arg2: Box::new(x.clone()),
                })
            }
            None => result = Some(x.clone()),
        }
    }
    if let Some(x) = &ic.platform {
        match result {
            Some(r) => {
                result = Some(Expression::BinaryOperator {
                    op: Operator::LogicalAnd,
                    arg1: Box::new(r.clone()),
                    arg2: Box::new(x.clone()),
                })
            }
            None => result = Some(x.clone()),
        }
    }
    if let Some(x) = &ic.bits {
        match result {
            Some(r) => {
                result = Some(Expression::BinaryOperator {
                    op: Operator::LogicalAnd,
                    arg1: Box::new(r.clone()),
                    arg2: Box::new(x.clone()),
                })
            }
            None => result = Some(x.clone()),
        }
    }
    if let Some(x) = c {
        match result {
            Some(r) => {
                result = Some(Expression::BinaryOperator {
                    op: Operator::LogicalAnd,
                    arg1: Box::new(r.clone()),
                    arg2: Box::new(x.clone()),
                })
            }
            None => result = Some(x.clone()),
        }
    }

    result
}
