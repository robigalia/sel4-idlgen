// TODO: Should this be in its own library?

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Expression {
    Identifier(Identifier),
    StringLiteral(String),
    NumericLiteral(u64),
    UnaryOperator {
        op: Operator,
        arg1: Box<Expression>,
    },
    BinaryOperator {
        op: Operator,
        arg1: Box<Expression>,
        arg2: Box<Expression>,
    },
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Identifier {
    Unresolved(String),
    Config(String),
    Constant(String),
    EnumConstant {
        constant_name: String,
        enum_name: String,
    },
    Type(String),
}

#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
pub enum Operator {
    UnaryPlus = 0,
    UnaryMinus,
    LogicalNot,
    BitwiseNot,
    Sizeof,
    Defined,
    Multiply,
    Divide,
    Remainder,
    Add,
    Subtract,
    LeftShift,
    RightShift,
    LessThan,
    LessThanEqual,
    GreaterThan,
    GreaterThanEqual,
    Equal,
    NotEqual,
    BitwiseAnd,
    BitwiseXor,
    BitwiseOr,
    LogicalAnd,
    LogicalOr,
}

impl Operator {
    pub fn to_str(&self) -> &'static str {
        match self {
            Operator::UnaryPlus => "UnaryPlus",
            Operator::UnaryMinus => "UnaryMinus",
            Operator::LogicalNot => "LogicalNot",
            Operator::BitwiseNot => "BitwiseNot",
            Operator::Sizeof => "Sizeof",
            Operator::Defined => "Defined",
            Operator::Multiply => "Multiply",
            Operator::Divide => "Divide",
            Operator::Remainder => "Remainder",
            Operator::Add => "Add",
            Operator::Subtract => "Subtract",
            Operator::LeftShift => "LeftShift",
            Operator::RightShift => "RightShift",
            Operator::LessThan => "LessThan",
            Operator::LessThanEqual => "LessThanEqual",
            Operator::GreaterThan => "GreaterThan",
            Operator::GreaterThanEqual => "GreaterThanEqual",
            Operator::Equal => "Equal",
            Operator::NotEqual => "NotEqual",
            Operator::BitwiseAnd => "BitwiseAnd",
            Operator::BitwiseXor => "BitwiseXor",
            Operator::BitwiseOr => "BitwiseOr",
            Operator::LogicalAnd => "LogicalAnd",
            Operator::LogicalOr => "LogicalOr",
        }
    }
}

impl Expression {
    pub fn resolve_identifiers<F>(&mut self, mut resolver: F)
    where
        F: FnMut(&String) -> Identifier,
    {
        self.resolve_identifiers_1(&mut resolver);
    }

    fn resolve_identifiers_1<F>(&mut self, resolver: &mut F)
    where
        F: FnMut(&String) -> Identifier,
    {
        match self {
            Expression::Identifier(Identifier::Unresolved(x)) => {
                *self = Expression::Identifier(resolver(&x));
            }
            Expression::Identifier(_) => {}
            Expression::StringLiteral(_) => {}
            Expression::NumericLiteral(_) => {}
            Expression::UnaryOperator { arg1, .. } => {
                arg1.resolve_identifiers_1(resolver);
            }
            Expression::BinaryOperator { arg1, arg2, .. } => {
                arg1.resolve_identifiers_1(resolver);
                arg2.resolve_identifiers_1(resolver);
            }
        }
    }

    pub fn visit_identifiers<F>(&self, mut f: F)
    where
        F: FnMut(&Identifier),
    {
        self.visit_identifiers_1(&mut f);
    }

    fn visit_identifiers_1<F>(&self, f: &mut F)
    where
        F: FnMut(&Identifier),
    {
        match self {
            Expression::Identifier(i) => {
                f(i);
            }
            Expression::StringLiteral(_) => {}
            Expression::NumericLiteral(_) => {}
            Expression::UnaryOperator { arg1, .. } => {
                arg1.visit_identifiers_1(f);
            }
            Expression::BinaryOperator { arg1, arg2, .. } => {
                arg1.visit_identifiers_1(f);
                arg2.visit_identifiers_1(f);
            }
        }
    }

    pub fn has_unresolved_references(&self) -> bool {
        match self {
            Expression::Identifier(Identifier::Unresolved(_)) => true,
            Expression::Identifier(_) => false,
            Expression::StringLiteral(_) => false,
            Expression::NumericLiteral(_) => false,
            Expression::UnaryOperator { arg1, .. } => arg1.has_unresolved_references(),
            Expression::BinaryOperator { arg1, arg2, .. } => {
                arg1.has_unresolved_references() || arg2.has_unresolved_references()
            }
        }
    }
}
